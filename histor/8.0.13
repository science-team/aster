

-----------------------------------------------------------------------
--- AUTEUR cibhhlv L.VIVAN   DATE  le 21/03/2005 a 19:49:52

-----------------------------------------------------------------------
CORRECTION AL 2005-097
   NB_JOURS_TRAV  : 0.5
   POUR_LE_COMPTE_DE   : E.GALENNE
   INTERET_UTILISATEUR : OUI
   TITRE : POST_RCCM calcul du PMPB en TRANSITOIRE
   FONCTIONNALITE
     Les crit�res de niveau 0 visent � pr�munir le mat�riel contre les
     dommages de d�formation excessive, d'instabilit� plastique et
     d'instabilit� �lastique et �lastoplastique.
     Le calcul de PM, PB et PMPB doit se faire � partir des contraintes
     primaires. Les contraintes d'origine thermiques ne sont pas
     consid�r�es comme des contraintes primaires (RCCM B3231.2).

                      m�canique
       PM = Max( SIGM_MOY(i)    )
             t           Eq.Tresca

                      m�canique
       PB = Max( SIGM_FLE(i)    )
             t           Eq.Tresca

                        m�canique
       PMPB = Max( SIGM_LIN(i)    )
               t           Eq.Tresca

   RESU_FAUX_VERSION_EXPLOITATION    : OUI   DEPUIS : 7.0.0
   RESU_FAUX_VERSION_DEVELOPPEMENT   : OUI   DEPUIS : 8.0.0
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_R : R7.04.03  POST_RCCM
       EXPL_ :  calcul du PMPB
   VALIDATION
     1 cas test modifi�: rccm04a (PM et PMPB, valeurs de non regression)
     validation dans rccm06a

--------------------------------------------------------------------------
CORRECTION AL 2005-014
   NB_JOURS_TRAV  : 3.5
   POUR_LE_COMPTE_DE   : E.GALENNE
   INTERET_UTILISATEUR : OUI
   TITRE : POST_RCCM calcul du SN* en m�thode UNITAIRE
   FONCTIONNALITE
     Apres concertation avec JMP et EG :
     - calcul du PMPB

                      m�canique          m�canique
       PM = Max( SIGM_MOY(i)   ,    SIGM_MOY(j)   )
                         Eq.Tresca          Eq.Tresca

                      m�canique          m�canique
       PB = Max( SIGM_FLE(i)   ,    SIGM_FLE(j)   )
                         Eq.Tresca          Eq.Tresca

                        m�canique          m�canique
       PMPB = Max( SIGM_LIN(i)   ,    SIGM_LIN(j)   )
                           Eq.Tresca          Eq.Tresca

     - calcul du SN*
     le calcul du SN* est r�alis� si le mot cl� NUME_RESU_THER est
     pr�sent sous le mot cl� facteur SITUATION.
     Les contraintes d'origine thermique sont donn�es derriere le mot cl�
     TABL_RESU_THER sous le mot cl� facteur RESU_THER.
     Exemple issu du rccm01a.com2
           SITUATION=_F( NB_OCCUR=1,
                         NUME_SITU=1,
                         NUME_GROUPE=1,
                         NUME_RESU_THER = 1,
                         ...                 ),
           RESU_THER=_F( NUME_RESU_THER=1,
                         TABL_RESU_THER=TBTHER, ),

     � l'origine:
                             totale         thermique
      SN* = Max( Max( ( SIGM_LIN(t1) + SIGM_FLE(t1) )
             t1   t2
                             totale         thermique
                    - ( SIGM_LIN(t2) + SIGM_FLE(t2) )    )       )
                                                         Eq.Tresca
     � l'extr�mit�:
                             totale         thermique
      SN* = Max( Max( ( SIGM_LIN(t1) - SIGM_FLE(t1) )
             t1   t2
                             totale         thermique
                    - ( SIGM_LIN(t2) - SIGM_FLE(t2) )    )       )
                                                         Eq.Tresca
     - calcul du SN et SP
      Situation P avec ses 2 Etats i et j et son thermique associ� Tp
      Situation Q avec ses 2 Etats k et l et son thermique associ� Tq
      Le calcul des SN et SP pour la situation P se fait de la facon
      suivante :
                       m�canique          thermique
      SN = Max( ( SIGM_LIN(ij)   +   SIGM_LIN(t1) )  )
             t1                                   Eq.Tresca

                     m�canique      thermique
      SP = Max( ( SIGM(ij)   +   SIGM(t1) )  )
             t1                           Eq.Tresca

      Le calcul des SN et SP pour une combinaison de situation se fait
      de la facon suivante :
      on a 4 combinaisons (Pi,Qk), (Pi,Ql), (Pj,Qk), (Pj,Ql)
      pour chaque combinaison (Pm,Qn)
                              m�canique        thermique
      SN = Max( Max( ( +-SIGM_LIN(mn)   +  SIGM_LIN(P) )  )     ,
                  tp                                   Eq.Tresca

                              m�canique         thermique
                Max( ( +-SIGM_LIN(mn)   +  SIGM_LIN(Q) )  )      )
                  tq                                   Eq.Tresca

                            m�canique       thermique
      SP = Max( Max( ( +-SIGM(mn)     +  SIGM(P) )  )     ,
                  tp                             Eq.Tresca

                            m�canique        thermique
                Max( ( +-SIGM(mn)     +   SIGM(Q) )  )      )
                  tq                              Eq.Tresca


   RESU_FAUX_VERSION_EXPLOITATION    : OUI   DEPUIS : 7.3.6
   RESU_FAUX_VERSION_DEVELOPPEMENT   : OUI   DEPUIS : 8.0.0
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_R : R7.04.03  POST_RCCM
       EXPL_ :  calcul du PMPB, SN*
     DOC_V : V1.01.107 RCCM01
       EXPL_ : ajout de la m�thode unitaire
   VALIDATION
     rccm01a.com2


-----------------------------------------------------------------------
--- AUTEUR d6bhhjp J.P.LEFEBVRE   DATE  le 22/03/2005 a 07:35:17

-------------------------------------------------------------------------
CORRECTION AL 2005-089
   NB_JOURS_TRAV  : 1.0
   INTERET_UTILISATEUR : OUI
   TITRE : FIN et EXTR_RESU
   FONCTIONNALITE : Retassage dans FIN
   DETAILS :
   Il s'agit d'une anomalie dans la routine utilis�e pour effectuer le
   retassage JETASS. La lecture et l'ecriture de certains enregistrements
   �taient r�alis�e avec pour longueur d'enregistrement une taille en
   kilo-mots. Ce qui conduisait � tronquer les enregistrements concern�s.
   Cela produit syst�matiquement une erreur, le chainage sur l'enregistrement
   complet ne pouvant plus �tre parcouru.
   On corrige en utilisant la variable LGBL qui contient la taille en octets.
   C'est malheureusement un oubli lors de la r�alisation de l'EL 2004-122.
   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE:  NON
   VALIDATION : 	
   Sur l'�tude associ�e � la fiche de Patrick.
-----------------------------------------------------------------------
REALISATION EL 2005-058
   NB_JOURS_TRAV  : 1.0
   INTERET_UTILISATEUR : OUI
   TITRE : Sauvegarde des bases et temps CPU
   FONCTIONNALITE
     Permettre de r�server une part du temps CPU attribu� au job pour
     terminer proprement l'ex�cution en cas d'arret par manque de temps
     CPU detect� par une commande Aster.
   DETAILS
     Le m�canisme d'arret par manque de temps CPU se base sur le temps
     maximum allou� au job et sur la mesure du temps consomm� par pas de
     charge si on prend l'exemple de STAT_NON_LINE. Il arrive que le temps
     restant soit insuffisant pour fermer et/ou pour compresser la base.
     On ajoute donc un mot-cl� dans DEBUT et POURSUITE pour avoir la
     possibilit� de se r�server une part du temps CPU.

   DEBUT/POURSUITE=MACRO(
     ...
         RESERVE_CPU       =FACT(fr="reserve de temps pour terminer une execution",statut='f',min=1,max=1,
           regles=(EXCLUS('VALE','POURCENTAGE'),),
           VALE            =SIMP(statut='f',typ='I',val_min=0),
           POURCENTAGE     =SIMP(statut='f',typ='R',val_min=0.,val_max=1.0),
#          valeur en pourcentage du temps maximum born�e � 180 secondes
           BORNE           =SIMP(statut='f',typ='I',val_min=0,defaut=180)
         ),

     )

   La valeur entr�e sous le mot cl� facteur RESERVE__CPU permet de modifier la
   valeur du temps CPU maximum pass� en ligne de commande du code et provenant
   directement de la valeur renseign�e au niveau de l'interface astk. On borne
   cette valeur, qui peut �tre fournie en absolu ou en pourcentage par une
   valeur fix�e dans le catalogue mais modifiable.
   Un message d'information apparait est imprim� lorsque le mot cle est activ�.


   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.11.01 DEBUT
       EXPL_ : documenter le mot cle facteur TEMPS_CPU
      DOC_U : U4.11.02 FIN
       EXPL_ : documenter le mot cle facteur TEMPS_CPU
   VALIDATION
   J'ai juste v�rifi� que la valeur utilis�e dans la fonction de mesure
   du temps CPU est bien affect�e par les modification introduites das les
   commandes DEBUT ou POURSUITE.
   Les tests yyyy100a et yyyy100b sont modifi�s pour solliciter les mots cl�s introduits.


-----------------------------------------------------------------------
--- AUTEUR durand C.DURAND   DATE  le 21/03/2005 a 14:16:29

-----------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE  attribut de mot cl� "position" dans catalogues
   FONCTIONNALITE superviseur
   DETAILS
   Restitution pour le compte de Christian Caremoli.
   Modification de N_MCCOMPO.py.
   Le fonctionnement de l'attribut "position='global'" �tait incorrect.
   Cet attribut sert � donner visibilit� dans tout le catalogue de la
   commande courante au mot cl� auquel il est appliqu�.
   Exemple :
   MA_COMMANDE=OPER(...
         NOM       =SIMP(statut='f',typ='TXM' ,into=('ACCE','DEPL'),position='global'),
         RESULTAT  =FACT(statut='o',
                   b_acce  =BLOC( condition = "NOM=='ACCE'",
                                VAL   =SIMP(statut='o',typ='R'), ),)

   Dans cet exemple, le mot cl� NOM aurait �t� jusqu'� pr�sent visible
   au niveau d'une condition de bloc de m�me niveau que le mcsimp
   lui m�me, mais pas deux niveaux plus bas (ici un bloc SOUS un MCFACT)

   Dans le cas pr�sent� ici, l'information est descendante : le mcsimp
   est au niveau le plus haut dans la commande. Les blocs sont � des
   niveaux inf�rieurs. Je n'ai pas test� la situation inverse (globalisation
   d'un mot cl� simple "profond") ... et pour le moment, on la d�conseille
   donc.

   Impact commandes : impr_resu, impr_fonction

   RESU_FAUX_VERSION_EXPLOITATION    :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   VALIDATION : cas tests utilisant impr_resu, en particulier
   IMPACT_DOCUMENTAIRE : OUI
     DOC_D : D5.01.01 introduire une nouvelle commande
       EXPL_ : expliquer l'usage de l'attribut position
-----------------------------------------------------------------------
REALISATION EL 2005-062
   NB_JOURS_TRAV  : 1
   INTERET_UTILISATEUR : NON
   TITRE : catalogue IMPR_RESU
   FONCTIONNALITE impr_resu
   DETAILS
      apr�s correction (cf hors arex meme histor) de la prise en compte
      de l'attribut position dans le catalogue, on peut donc r�tablir les
      blocs commentaris�s de IMPR_RESU donc la condition exploite le
      MCSIMP FORMAT de haut niveau.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON , enfin je ne crois pas
   VALIDATION cas tests  IMPR_RESU
-----------------------------------------------------------------------
CORRECTION AL 2005-098
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : OUI
   TITRE : m�nage m�thode EXTRACT
   FONCTIONNALITE
     mauvais m�nage en version 8.0.12 de la m�thode de r�cup�ration de
     bouts de tables dans un numarray.
     modif des cas tests
     demo002a
     forma03b
     ssnv127a
     ssnv127b
     ssnv127c
     ssnv127d
     ssnv127e
   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
     cas tests concern�s
--------------------------------------------------------------------------
CORRECTION AL 2005-100
   NB_JOURS_TRAV  : 0.
   INTERET_UTILISATEUR : NON
   TITRE : m�nage m�thode IMPR_FONCTION
   FONCTIONNALITE
     suppression concept temporaire dans impr_fonction_ops.py
   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
     ajout impression de nappe dans zzzz140a
--------------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR laverne J.LAVERNE   DATE  le 21/03/2005 a 11:12:04

------------------------------------------------------------------------
REALISATION EL 2005-051
   NB_JOURS_TRAV : 12
   INTERET_UTILISATEUR : OUI
   TITRE : EVOLUTION DES ELEMENTS DE JOINT ET DE LA LOI DE BARENBLATT
   FONCTIONNALITE
   RESU_FAUX_VERSION_EXPLOITATION  :     NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :     NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_R : R3.06.09, R7.02.11
     DOC_U : U4.23.02
       EXPL_: introduction du mot cl� CREA_JOINT
     DOC_V : V6.03.118
   VALIDATION SSNP118A

-----------------------------------------------------------------------
VISUALISATION AUX POINTS DE GAUSS SUR 2 POINTS (ELEMENT DE JOINT)
-----------------------------------------------------------------------

ajout eclatement au pts de gauss dans le cas ou FAPG='FIS2'
MODIF --> ECLA2D.F

-----------------------------------------------------------------------
CREATION D'ELEMENTS DE JOINT
-----------------------------------------------------------------------

CREA_MAILLAGE(
  CREA_JOINT = _F(
    GROUP_NO_1  = 'gno1',
    GROUP_NO_2  = 'gno2',
    PREF_MAILLE = 'MJ',
    NUME_INIT   = 1
    )
  )

Ajout du mot cl� CREA_JOINT dans CREA_MAILLAGE
On tisse des QUAD4 entre les noeuds des deux group_no (il vaut mieux
qu'ils aient ete ordonnes au prealable dans DEFI_GROUP)

Mise en place d'une routine generique (cmcrea) pour creer de nouveaux
maillages (toutes les fonctionnalit�s ne sont pas encore en place).

MODIFS --> CREA_MAILLAGE.CAPY, OP0167.F
AJOUTS --> CMCREA.F, CMJOIN.F


-----------------------------------------------------------------------
INTENDANCE POUR LES ELEMENTS DE JOINT
-----------------------------------------------------------------------

Mise a jour de l'architecture pour pouvoir integrer d'autres lois :

  On decode le nombre de variables internes (au lieu d'affirmer 3).
  MODIFS --> TE0201.F, TE0203.F

  On passe a nmcomp le saut au debut du pas et son increment au lieu
  de sa valeur a la fin. On modifie LCBARE.F et LCJOBA.F de la meme facon.
  MODIFS --> NMCOMP.F, LCBARE.F, LCJOBA.F


Mise a jour de l'architecture pour pouvoir traiter d'autres EF

  Poids des points de Gauss et matrice B sont maintenant calcules
  au meme endroit (dans NMFISA.F).
  MODIFS --> NMFISA.F, NMFI2D.F, NMFIFI.F, PIPEFI.F


-----------------------------------------------------------------------
PILOTAGE PRED_ELAS POUR BARENBLATT
-----------------------------------------------------------------------

Pilotage pour Barenblatt : deplacement relatif
On introduit le deplacement de reference Gc/SigmaC + VIM(1)
pour rendre le critere PRED_ELAS adimensionnel.
Ajout d'une routine sp�cifique � la ldc BARENBLATT, pour permettre
d'autres lois.

AJOUT --> PIPEBA


-----------------------------------------------------------------------
LOI DE BARENBLATT
-----------------------------------------------------------------------

- Penalisation pour la rigidite initiale (SAUT_C change de signification).
  Pour eviter toute confusion, on change aussi le nom du parametre qui
  devient PENA_ADHERENCE (au lieu de SAUT_C).

  Dorenavant, le deplacement critique initial K0 vaut :
  K0 = GC / SIGMAC * PENA_ADHERENCE

Remarque : La VIM(1), variable seuil qui correspond � la plus grande norme
           du saut atteinte, n'est pas nulle � l'instant initial.
           Cette derni�re est initialis� � KO d�fini ci dessus.

- Penalisation de la fermeture (contact).
  On introduit un nouveau coefficient PENA_CONTACT qui vaut par defaut 1 et
  mesure l'intensite de la penalisation de contact.

Remarque : Les modifs pr�c�dentes conduisent � la modif des options
           des mots cl� RUPT_FRAG et RUPT_FRAG_FO dans DEFI_MATERIAU.CAPY

- Traitement des options FULL_MECA_ELAS et RIGI_MECA_ELAS

- Renseignement dans la variable interne 4 s'il y a decollement (1) ou
  contact (2).

 MODIFS --> LCBARE.F,
            GENER_MF2FI2.CATA,
            C_COMP_INCR.CAPY , DEFI_MATERIAU.CAPY ,
            SSNP118A.COMM
 AJOUT  --> PIPEBA.F

------------------------------------------------------------------------
MISE A JOUR CAS TEST AVEC ELEMENT DE JOINT
------------------------------------------------------------------------

- Le cas test SSNP119A (propagation dyna de fissure avec des elements
  de joint) n'a pas le bon nom, on le supprime et le renome : SDNS105A.

- On met � jour ce dernier avec les modifs ci-dessus.

SUPPR --> SSNP119A
AJOUT --> SDNS105A

-------------------------------------------------------------------------
REALISATION EL 2005-038
   POUR_LE_COMPTE_DE : J. PELLET
   INTERET_UTILISATEUR : OUI
   TITRE : CREA_MAILLAGE / QUAD_LINE
   FONCTIONNALITE lin�ariser un maillage quadratique
   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.23.02 (op�rateur CREA_MAILLAGE)
       EXPL_ : ajout de la fonctionnalit� QUAD_LINE
   VALIDATION : cas-test zzzz206a
   NB_JOURS_TRAV  : 7.0
   DETAILS :
     - Le mot-cl� facteur QUAD_LINE de l'op�rateur CREA_MAILLAGE permet la
     transformation de mailles quadratiques en mailles lin�aires. Cette
     fonctionnalit� est la r�ciproque du mot-cl� facteur LINE_QUAD.
     - La possibilit� de lin�ariser qu'une selection de mailles quadratiques est
     possible. Cependant, une alarme est �mise dans le cas-suivant:
     supposons que 2 mailles quadratiques se partagent un noeud milieu, et
     que l'utilisateur ne souhaite lin�ariser qu'une des 2 mailles.
     Alors on conservons le noeud milieu et une alarme est �mise pr�cisant que
     le maillage r�sultant est incompatible.

     Commande:
     MA2=CREA_MAILLAGE(MAILLAGE=MA1,
                       QUAD_LINE=_F( / TOUT = 'OUI',
		                     / GROUP_MA = lgrma,
			             / MAILLE   = lma),)
				
     Liste des mailles qui peuvent etre lin�aris�es:
                     SEG3    --->  SEG2
                     TRIA6   --->  TRIA3
                     QUAD8   --->  QUAD4
                     QUAD9   --->  QUAD4
                     TETRA10 --->  TETRA4
                     PYRAM13 --->  PYRAM5
                     PENTA15 --->  PENTA6
                     HEXA20  --->  HEXA8
                     HEXA27  --->  HEXA8
		
      Le cas-test restitu� permet de v�rifier la lin�arisation des 9 types de
      mailles ci-dessus, ainsi que la lin�arisation d'une partie d'un maillage
      quadratique. La validation du maillage lin�aris� s'est effectu�e en visualisant
      sous GMSH les 9 mailles lin�aris�es et en effectuant un test_fichier.
	
     Sources modifi�es  : op0167.f
     Sources ajout�es   : cmqlql.f, cmqlnm.f, cmqlno.f, cmqlma.f
     Cas-test ajout�    : zzzz206a


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

       C AJOUT utilitai/utclim               d6bhhjp J.P.LEFEBVRE        44     44      0
 CASTEST AJOUT sdns105a                      laverne J.LAVERNE          305    305      0
 CASTEST AJOUT zzzz206a                      laverne J.LAVERNE           54     54      0
 CASTEST MODIF demo002a                       durand C.DURAND           498      5      4
 CASTEST MODIF forma03b                       durand C.DURAND           248      1      1
 CASTEST MODIF rccm01a                       cibhhlv L.VIVAN            199      1      1
 CASTEST MODIF rccm04a                       cibhhlv L.VIVAN            360      5      5
 CASTEST MODIF rccm06a                       cibhhlv L.VIVAN           1432     82      5
 CASTEST MODIF ssnp118a                      laverne J.LAVERNE          231      6      6
 CASTEST MODIF ssnv127a                       durand C.DURAND           462      6      5
 CASTEST MODIF ssnv127b                       durand C.DURAND           461      6      5
 CASTEST MODIF ssnv127c                       durand C.DURAND           469      6      5
 CASTEST MODIF ssnv127d                       durand C.DURAND           468      6      5
 CASTEST MODIF ssnv127e                       durand C.DURAND           464      6      5
 CASTEST MODIF yyyy100a                      d6bhhjp J.P.LEFEBVRE       116      4      2
 CASTEST MODIF yyyy100b                      d6bhhjp J.P.LEFEBVRE        94      4      2
 CASTEST MODIF zzzz140a                       durand C.DURAND           181     22      1
 CASTEST SUPPR ssnp119a.comm                 laverne J.LAVERNE          284      0    284
CATALOPY MODIF commande/crea_maillage        laverne J.LAVERNE          156     21      1
CATALOPY MODIF commande/debut                d6bhhjp J.P.LEFEBVRE        77      8      1
CATALOPY MODIF commande/defi_materiau        laverne J.LAVERNE         2812      5      3
CATALOPY MODIF commande/impr_resu             durand C.DURAND           146     23     23
CATALOPY MODIF commande/mode_iter_simult      durand C.DURAND           140      2      2
CATALOPY MODIF commande/post_rccm            cibhhlv L.VIVAN            198      1      3
CATALOPY MODIF commande/poursuite            d6bhhjp J.P.LEFEBVRE        73      8      1
CATALOPY MODIF commande/proj_champ            durand C.DURAND           101      2      2
CATALOPY MODIF commun/c_comp_incr            laverne J.LAVERNE          305      2      2
 FORTRAN AJOUT algeline/cmcrea               laverne J.LAVERNE          331    331      0
 FORTRAN AJOUT algeline/cmjoin               laverne J.LAVERNE          146    146      0
 FORTRAN AJOUT algorith/pipeba               laverne J.LAVERNE          175    175      0
 FORTRAN AJOUT postrele/rc32s1               cibhhlv L.VIVAN            132    132      0
 FORTRAN AJOUT prepost/cmqlma                laverne J.LAVERNE          151    151      0
 FORTRAN AJOUT prepost/cmqlnm                laverne J.LAVERNE          208    208      0
 FORTRAN AJOUT prepost/cmqlno                laverne J.LAVERNE          176    176      0
 FORTRAN AJOUT prepost/cmqlql                laverne J.LAVERNE          159    159      0
 FORTRAN AJOUT supervis/ibtcpu               d6bhhjp J.P.LEFEBVRE        77     77      0
 FORTRAN MODIF algeline/op0167               laverne J.LAVERNE          880     46      2
 FORTRAN MODIF algorith/lcbare               laverne J.LAVERNE          167    106     83
 FORTRAN MODIF algorith/lcjoba               laverne J.LAVERNE          375     11      5
 FORTRAN MODIF algorith/nmcomp               laverne J.LAVERNE          676      4      3
 FORTRAN MODIF algorith/nmfi2d               laverne J.LAVERNE          199     32     67
 FORTRAN MODIF algorith/nmfifi               laverne J.LAVERNE           65     10     24
 FORTRAN MODIF algorith/nmfisa               laverne J.LAVERNE          102     21      8
 FORTRAN MODIF algorith/pipefi               laverne J.LAVERNE           82     27    134
 FORTRAN MODIF calculel/ecla2d               laverne J.LAVERNE          445     41      1
 FORTRAN MODIF elements/te0201               laverne J.LAVERNE          124      4      3
 FORTRAN MODIF elements/te0203               laverne J.LAVERNE           85     14      9
 FORTRAN MODIF jeveux/jetass                 d6bhhjp J.P.LEFEBVRE       247      3      3
 FORTRAN MODIF postrele/op0165               cibhhlv L.VIVAN            142      2      2
 FORTRAN MODIF postrele/rc3201               cibhhlv L.VIVAN            573     74    121
 FORTRAN MODIF postrele/rc3203               cibhhlv L.VIVAN            313     17     17
 FORTRAN MODIF postrele/rc32ac               cibhhlv L.VIVAN            414     13     10
 FORTRAN MODIF postrele/rc32mu               cibhhlv L.VIVAN            149      3      7
 FORTRAN MODIF postrele/rc32pm               cibhhlv L.VIVAN            166     31     55
 FORTRAN MODIF postrele/rc32sn               cibhhlv L.VIVAN            219     55     13
 FORTRAN MODIF postrele/rc32sp               cibhhlv L.VIVAN            275     43     16
 FORTRAN MODIF postrele/rc32th               cibhhlv L.VIVAN            180     12     39
 FORTRAN MODIF postrele/rcev22               cibhhlv L.VIVAN            315      6      3
 FORTRAN MODIF postrele/rcevo2               cibhhlv L.VIVAN            353      6      3
 FORTRAN MODIF postrele/rcevod               cibhhlv L.VIVAN            478     19      7
 FORTRAN MODIF postrele/rcevom               cibhhlv L.VIVAN            573     16      6
 FORTRAN MODIF postrele/rcevse               cibhhlv L.VIVAN            129      3      3
 FORTRAN MODIF supervis/debut                d6bhhjp J.P.LEFEBVRE        88      5      1
  PYTHON MODIF Macro/impr_fonction_ops        durand C.DURAND           402      2      1
  PYTHON MODIF Noyau/N_MCCOMPO                durand C.DURAND           417     17      1


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :   12        1958      1958             +1958
 MODIF :   52       18324       875     737      +138
 SUPPR :    1         284               284      -284
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   65       20566      2833    1021     +1812 
