# profile for Code_Aster version 11.10

if [ -z "$DEFINE_PROFILE_ASTER_VERSION" ]; then
   export DEFINE_PROFILE_ASTER_VERSION=done
#--- ifndef DEFINE_PROFILE_ASTER_VERSION ---------------------------------------

if [ -z "$ASTER_ROOT" ]; then
    # from first installation
    ASTER_ROOT=/usr/lib/codeaster
    export ASTER_ROOT

    if [ -z "$PATH" ]; then
	export PATH=$ASTER_ROOT/bin:$ASTER_ROOT/outils
    else
	export PATH=$ASTER_ROOT/bin:$ASTER_ROOT/outils:$PATH
    fi
fi


#--- endif DEFINE_PROFILE_ASTER_VERSION ------------------------------------------------
fi

