#!/bin/sh
DESTDIR=/tmp/as_test_${1}_${2}
DESTFILE=${DESTDIR}/astout.export
mkdir -p $DESTDIR
echo P actions astout > ${DESTFILE}
echo P version ${2} >> ${DESTFILE}
echo P debug nodebug >> ${DESTFILE}
echo A rep_mat /usr/lib/codeaster/${2}/materiau >> ${DESTFILE}

echo P mode interactif >> ${DESTFILE}

echo "# parameters" >> ${DESTFILE}
echo P ncpus 1 >> ${DESTFILE}
echo P nbmaxnook 500 >> ${DESTFILE}
echo P cpresok RESOK >> ${DESTFILE}
echo P facmtps 3 >> ${DESTFILE}
echo P tpsjob 300 >> ${DESTFILE}

echo "# list of the test to run" >> ${DESTFILE}
echo F list ${3} D 0 >> ${DESTFILE}

echo "# results destination" >> ${DESTFILE}
echo R resu_test ${DESTDIR} R 0 >> ${DESTFILE}

as_run ${DESTFILE} | tee ${DESTDIR}/test.log

# python aster_test_parse.py ${1} ${2} ${3}
