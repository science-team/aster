!
! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine nmchrm(phase, parmet, method, fonact, sddisc,&
                      sddyna, numins, iterat, defico, metpre,&
                      metcor, reasma)
        character(len=10), intent(in) :: phase
        real(kind=8), intent(in) :: parmet(*)
        character(len=16), intent(in) :: method(*)
        character(len=19), intent(in) :: sddisc
        character(len=19), intent(in) :: sddyna
        integer, intent(in) :: numins
        integer, intent(in) :: iterat
        character(len=24), intent(in) :: defico
        integer, intent(in) :: fonact(*)
        character(len=16), intent(out) :: metcor
        character(len=16), intent(out) :: metpre
        logical, intent(out) :: reasma
    end subroutine nmchrm
end interface
