!
! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine nmdata(result, modele, mate, carele, compor,&
                      lischa, solveu, method, parmet, parcri,&
                      parcon, carcri, sddyna, sdpost, sderro,&
                      sdener, sdcriq, sdimpr)
        character(len=8) :: result
        character(len=24) :: modele
        character(len=24) :: mate
        character(len=24) :: carele
        character(len=24) :: compor
        character(len=19) :: lischa
        character(len=19) :: solveu
        character(len=16) :: method(*)
        real(kind=8) :: parmet(*)
        real(kind=8) :: parcri(*)
        real(kind=8) :: parcon(*)
        character(len=24) :: carcri
        character(len=19) :: sddyna
        character(len=19) :: sdpost
        character(len=24) :: sderro
        character(len=19) :: sdener
        character(len=24) :: sdcriq
        character(len=24) :: sdimpr
    end subroutine nmdata
end interface
