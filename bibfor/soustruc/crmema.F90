subroutine crmema(promes, iampee)
! ======================================================================
! COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
!   1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
! ======================================================================
!
! CALCUL DE LA PSEUDO-MATRICE MASSE POUR LA MODIFICATION STRUCTURALE
!
!   IN  : PROMES : NOM DU CONCEPT PROJ_MESU_MODAL (POUR BASE DE PROJ)
!   IN  : IAMPEE : ADRESSE DE LA MATRICE MACR_ELEM MASSE
!
    implicit none
!
#include "jeveux.h"
!
#include "asterc/getres.h"
#include "asterc/getvid.h"
#include "asterc/r8prem.h"
#include "asterfort/cnocns.h"
#include "asterfort/detrsd.h"
#include "asterfort/dismoi.h"
#include "asterfort/jedema.h"
#include "asterfort/jedetr.h"
#include "asterfort/jeecra.h"
#include "asterfort/jeexin.h"
#include "asterfort/jelira.h"
#include "asterfort/jemarq.h"
#include "asterfort/jenonu.h"
#include "asterfort/jeveuo.h"
#include "asterfort/jexnom.h"
#include "asterfort/jexnum.h"
#include "asterfort/remome.h"
#include "asterfort/rsadpa.h"
#include "asterfort/rsexch.h"
#include "asterfort/rslsvd.h"
#include "asterfort/u2mesi.h"
#include "asterfort/u2mess.h"
#include "asterfort/wkvect.h"
    character(len=8) :: promes
    integer :: iampee
!
!
!
!
    character(len=8) :: basemo, nomres, k8bid, typ, trav, modmes, sol, modlms
    character(len=8) :: nomgd, vals, u, v, wks, modele, ma
    character(len=16) :: nomchp, noresu, maelm, k16bid
    character(len=19) :: chamno, ch1s, nu
    character(len=24) :: vnoeud, vrange, basepr, noeums, baseit, vsu, mesint
    character(len=24) :: modid, vref, refms
!
    integer :: nbmesu, nbvecb, nbord, iadesm, isol, affici(2)
    integer :: lord, lred, lrange, lint, ier, iposi, ipuls
    integer :: imod, jmod, iret, llncmp, iddl, lmesu, jddl, iexist
    integer :: iposj, ino, nddle, nddli, ico, ipos
    integer :: lnoeud, ltrav, lredi, lwks, iadeeq, lrefms, lref
    integer :: jcnsd, jcnsc, jcnsv, jcnsk
    integer :: ibid, nbcmpi, numgd, lmaelm
    integer :: lu, lvals, lv, lvsu
!
    real(kind=8) :: masg, eps
    integer :: iarg
!
! ----------------------------------------------------------------------
!
    call jemarq()
!
    call getres(nomres, k8bid, k8bid)
!
! RECUPERATION DES ELEMENTS RELATIFS A L'EXPANSION
    noeums = promes//'.PROJM    .PJMNO'
    call jelira(noeums, 'LONUTI', nbmesu, k8bid)
!
    refms = promes//'.PROJM    .PJMRF'
    call jeveuo(refms, 'L', lrefms)
    k16bid=zk16(lrefms-1 +1)
    modlms=k16bid(1:8)
    nomchp=zk16(lrefms-1 +2)
    k16bid=zk16(lrefms-1 +3)
    basemo=k16bid(1:8)
!
    basepr = promes//'.PROJM    .PJMBP'
    call jeveuo(basepr, 'L', lred)
    call jelira(basepr, 'LONUTI', nbvecb, k8bid)
    nbvecb = nbvecb/nbmesu
! NBVECB : NOMBRE DE VECTEURS DE BASE
!
! RECUPERATION DES ELEMENTS RELATIFS AU MACRO ELEMENT
    noresu = nomres
    nu = noresu(1:14)//'.NUME'
    call jeveuo(nomres//'.DESM', 'L', iadesm)
    nddle = zi(iadesm-1+4)
    nddli = zi(iadesm-1+5)
!
    baseit = nomres//'.PROJM    .PJMBP'
    vsu = nomres//'.PROJM    .PJMIG'
!
! RECUPERATION DES ELEMENTS RELATIFS AU MODES MESURES
    call getvid('DEFINITION', 'MODE_MESURE', 1, iarg, 1,&
                modmes, ibid)
    vref = nomres//'.PROJM    .PJMRF'
    call jeexin(vref, iexist)
    if (iexist .eq. 0) then
        call wkvect(vref, 'G V K16', 5, lref)
        zk16(lref-1 +1) = modlms
        zk16(lref-1 +2) = nomchp
        zk16(lref-1 +3) = basemo
        zk16(lref-1 +4) = modmes
        zk16(lref-1 +5) = promes
    endif
!
    modid = nomres//'.PROJM    .PJMMM'
    call jeexin(modid, iexist)
    if (iexist .eq. 0) then
        call remome(promes, modmes, nomres)
    endif
    call jeveuo(modid, 'L', lmesu)
    call jelira(modid, 'LONUTI', nbord, k8bid)
    nbord = nbord/nbmesu
! NBORD : NOMBRE DE NUMERO D'ORDRE
!
    trav = '&TRAV'
    wks = '&WKS'
!
! ===============================
!  TEST : CALCUL INVERSE MATRICE DE PASSAGE DEJA REALISE
! ===============================
    call jeexin(vsu, iexist)
    if (iexist .eq. 0) then
!
! CREATION DE LA BASE RESTREINTE AUX DDL EXTERIEUR
        call getvid('DEFINITION', 'MODELE', 1, iarg, 1,&
                    modele, ibid)
        call dismoi('F', 'NOM_MAILLA', modele, 'MODELE', ibid,&
                    ma, iret)
!
        call wkvect(baseit, 'G V R', nddle*nbvecb, lredi)
        call jeveuo(basemo//'           .ORDR', 'L', lord)
        ch1s='&BASEIT.CH1S'
!
        do 220 imod = 1, nbvecb
            call rsexch('F', basemo, nomchp, zi(lord-1+imod), chamno,&
                        iret)
!
! TRANSFORMATION DE CHAMNO EN CHAM_NO_S : CH1S
            call detrsd('CHAM_NO_S', ch1s)
            call cnocns(chamno, 'V', ch1s)
!
! RECUPERATION DU CHAMP AU NOEUD
            call jeveuo(ch1s//'.CNSK', 'L', jcnsk)
            call jeveuo(ch1s//'.CNSD', 'L', jcnsd)
            call jeveuo(ch1s//'.CNSC', 'L', jcnsc)
            call jeveuo(ch1s//'.CNSV', 'L', jcnsv)
!
            nbcmpi = zi(jcnsd-1 + 2)
            nomgd = zk8(jcnsk-1 + 2)
!
            call jenonu(jexnom('&CATA.GD.NOMGD', nomgd), numgd)
            call jeveuo(jexnum('&CATA.GD.NOMCMP', numgd), 'L', llncmp)
!
            if (imod .eq. 1) then
! CREATION DE LA LISTE DES DDL EXTERIEUR
                vnoeud = nomres//'.PROJM    .PJMNO'
                vrange = nomres//'.PROJM    .PJMRG'
                call wkvect(vnoeud, 'G V I', nddle, lnoeud)
                call wkvect(vrange, 'G V K8', nddle, lrange)
            endif
!
            call jeveuo(nu//'.DEEQ', 'L', iadeeq)
!
            do 850 iddl = 1, nddle
                ino=zi(iadeeq-1+nddli*2+(iddl-1)*2+1)
                ico=zi(iadeeq-1+nddli*2+(iddl-1)*2+2)
                if (imod .eq. 1) then
                    typ=zk8(llncmp-1+ico)
                    zi(lnoeud-1+iddl) = ino
                    zk8(lrange-1+iddl) = typ
                endif
                ipos = (imod-1)*nddle + iddl
                zr(lredi-1 +ipos) = zr(jcnsv-1 +(ino-1)*nbcmpi+ico)
850          continue
!
220      continue
!
        call jeecra(vnoeud, 'LONUTI', nddle, k8bid)
        call jeecra(vrange, 'LONUTI', nddle, k8bid)
        call jeecra(baseit, 'LONUTI', nddle*nbvecb, k8bid)
!
! FIN CREATION DE LA BASE RESTREINTE AUX DDL EXTERIEUR (LREDI)
!
! ===============================
! CALCUL MATRICE DE PASSAGE TIT (NOTATION MC)
! ===============================
!
! MESINT : MATRICE DE PASSAGE TIT (CAPTEUR -> INTERFACE)
        mesint = nomres//'.TIT'
        call wkvect(mesint, 'V V R', nddle*nbmesu, lint)
!
! CALCUL DU PSEUDO INVERSE DE LA BASE REDUITE AUX DDL MESURE
!
        if (nbmesu .lt. nbvecb) then
            affici(1) = nbmesu
            affici(2) = nbvecb
            call u2mesi('F', 'SOUSTRUC_82', 2, affici)
        endif
!
        vals = '&VALS'
        u = '&U'
        v = '&V'
!
        call wkvect(vals, 'V V R', nbvecb, lvals)
        call wkvect(u, 'V V R', nbmesu*nbmesu, lu)
        call wkvect(v, 'V V R', nbmesu*nbvecb, lv)
!
! CALCUL PSEUDO INVERSE BASE REDUITE AUX DDL MESURE (LTRAV)
!
        sol = '&SOLUT'
        call wkvect(sol, 'V V R', nbmesu*nbmesu, isol)
!
        call wkvect(wks, 'V V R', nbmesu, lwks)
!
        do 83 iddl = 1, nbmesu
            do 84 jddl = 1, nbmesu
                ipos = (jddl-1)*nbmesu+iddl
                if (iddl .eq. jddl) then
                    zr(isol-1 +ipos) = 1.d0
                else
                    zr(isol-1 +ipos) = 0.d0
                endif
84          continue
83      continue
!
        eps = 1.d2*r8prem()
        call wkvect(trav, 'V V R', nbvecb*nbmesu, ltrav)
        do 85 iddl = 1, nbvecb*nbmesu
            zr(ltrav-1+iddl) = zr(lred-1+iddl)
85      continue
!
        call rslsvd(nbmesu, nbmesu, nbvecb, zr(ltrav), zr(lvals),&
                    zr(lu), zr(lv), nbmesu, zr(isol), eps,&
                    ier, zr(lwks))
!
        call jedetr(trav)
        if (ier .ne. 0) call u2mess('F', 'UTILITAI3_8')
!
        call wkvect(trav, 'V V R', nbvecb*nbmesu, ltrav)
!
        do 630 imod = 1, nbvecb
            do 650 jddl = 1, nbmesu
                ipos = (jddl-1)*nbmesu+imod
                iposj = (jddl-1)*nbvecb+imod
                zr(ltrav-1+iposj) = zr(isol-1+ipos)
650          continue
630      continue
!
        call jedetr(vals)
        call jedetr(u)
        call jedetr(v)
!
        call jedetr(wks)
        call jedetr(sol)
!
! FIN CALCUL PSEUDO INVERSE BASE REDUITE AUX DDL MESURE (LTRAV)
!
        do 250 iddl = 1, nddle
            do 260 jddl = 1, nbmesu
                ipos = (jddl-1)*nddle + iddl
                zr(lint-1+ipos) = 0.d0
                do 270 imod = 1, nbvecb
                    iposi = (imod-1)*nddle+iddl
                    iposj = (jddl-1)*nbvecb+imod
                    zr(lint-1+ipos) = zr(lint-1+ipos) + zr(lredi-1+ iposi)*zr(ltrav-1+iposj)
270              continue
260          continue
250      continue
!
        call jedetr(trav)
!
! ===============================
! FIN CALCUL MATRICE DE PASSAGE TIT (LINT)
! ===============================
!
!
! ===============================
! CALCUL DU PSEUDO INVERSE DE (TIT*PHI) PAR SVD (NOTATION MC)
! MATRICE DE PASSAGE COORDONNEES GENERALISEES -> DDL_INTERFACE
! ICI PHI : MODES PROPRES IDENTIFIES
! ===============================
!
! CALCUL DU PRODUIT TIT*PHIid : LTRAV
        call wkvect(trav, 'V V R', nddle*nbord, ltrav)
!
        do 300 iddl = 1, nddle
            do 310 jmod = 1, nbord
                ipos = (jmod-1)*nddle + iddl
                zr(ltrav-1+ipos) = 0.d0
                do 320 jddl = 1, nbmesu
                    iposi = (jddl-1)*nddle+iddl
                    iposj = (jmod-1)*nbmesu+jddl
                    zr(ltrav-1+ipos) = zr(ltrav-1+ipos) + zr(lint-1+ iposi)*zr(lmesu-1+iposj)
320              continue
310          continue
300      continue
!
        call jedetr(mesint)
!
        if (nddle .lt. nbord) then
            affici(1) = nddle
            affici(2) = nbord
            call u2mesi('F', 'SOUSTRUC_83', 2, affici)
        endif
!
        call wkvect(vals, 'V V R', nbord, lvals)
        call wkvect(u, 'V V R', nddle*nddle, lu)
        call wkvect(v, 'V V R', nddle*nbord, lv)
!
        call wkvect(sol, 'V V R', nddle*nddle, isol)
!
        call wkvect(wks, 'V V R', nddle, lwks)
!
        do 183 iddl = 1, nddle
            do 184 jddl = 1, nddle
                ipos = (jddl-1)*nddle+iddl
                if (iddl .eq. jddl) then
                    zr(isol-1 +ipos) = 1.d0
                else
                    zr(isol-1 +ipos) = 0.d0
                endif
184          continue
183      continue
!
! CALCUL DE L'INVERSE DU PRODUIT TIT*PHIid : LVSU
!
        call wkvect(vsu, 'G V R', nbord*nddle, lvsu)
!
        call rslsvd(nddle, nddle, nbord, zr(ltrav), zr(lvals),&
                    zr(lu), zr(lv), nddle, zr(isol), eps,&
                    ier, zr(lwks))
!
        if (ier .ne. 0) call u2mess('F', 'UTILITAI3_8')
!
        call jedetr(wks)
!
        do 130 imod = 1, nbord
            do 150 jddl = 1, nddle
                ipos = (jddl-1)*nbord+imod
                iposj = (jddl-1)*nddle+imod
                zr(lvsu-1+ipos) = zr(isol-1+iposj)
150          continue
130      continue
!
        call jeecra(vsu, 'LONUTI', nbord*nddle, k8bid)
!
        call jedetr(sol)
!
        call jedetr(trav)
!
        call jedetr(vals)
        call jedetr(u)
        call jedetr(v)
!
    else
        call jeveuo(vsu, 'L', lvsu)
    endif
!
! ===============================
! FIN TEST : CALCUL INVERSE MATRICE DE PASSAGE DEJA REALISE
! ===============================
!
!  CALCUL DE VSUT : LTRAV
    call wkvect(trav, 'V V R', nddle*nbord, ltrav)
    call wkvect(wks, 'V V R', nbord*nddle, lwks)
!
    do 570 imod = 1, nbord
        call rsadpa(modmes, 'L', 1, 'MASS_GENE', imod,&
                    0, ipuls, k8bid)
        masg = zr(ipuls)
        do 560 iddl = 1, nddle
            ipos = (iddl-1)*nbord+imod
            iposi = (imod-1)*nddle+iddl
            zr(ltrav-1+iposi)=zr(lvsu-1+ipos)*sqrt(masg)
            zr(lwks-1+ipos)=zr(lvsu-1+ipos)*sqrt(masg)
560      continue
570  end do
!
! ===============================
! CALCUL DES TERMES DE LA <MATRICE ELEMENTAIRE>
! ===============================
!
    maelm = nomres//'.MAEL_M'
    call wkvect(maelm, 'V V R', nddle*nddle, lmaelm)
!
    do 420 iddl = 1, nddle
        do 430 jddl = 1, nddle
            ipos = (jddl-1)*nddle+iddl
            zr(lmaelm-1+ipos) = 0.d0
            do 450 imod = 1, nbord
                iposi = (imod-1)*nddle+iddl
                iposj = (jddl-1)*nbord+imod
                zr(lmaelm-1+ipos) = zr(lmaelm-1+ipos) + zr(ltrav-1+ iposi)*zr(lwks-1+iposj)
450          continue
430      continue
420  end do
!
! RANGEMENT DES RESULTATS DANS IAMPEE
!
    do 700 iddl = 1, nddle
        do 710 jddl = 1, iddl
            ipos = (iddl-1)*iddl/2 + jddl
            iposi = (jddl-1)*nddle + iddl
            zr(iampee-1+ipos) = zr(lmaelm-1+iposi)
710      continue
700  end do
!
    call jedetr(trav)
    call jedetr(wks)
    call jedetr(maelm)
!
! ===============================
! FIN CALCUL DES TERMES DES <MATRICE ELEMENTAIRE>
! ===============================
!
    call jedema()
!
end subroutine
