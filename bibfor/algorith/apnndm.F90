subroutine apnndm(sdappa, defico, posmai, nnosdm)
!
! ======================================================================
! COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
!   1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
! ======================================================================
! person_in_charge: mickael.abbas at edf.fr
!
    implicit      none
#include "jeveux.h"
#include "asterfort/cfnben.h"
    character(len=19) :: sdappa
    character(len=24) :: defico
    integer :: posmai, nnosdm
!
! ----------------------------------------------------------------------
!
! ROUTINE APPARIEMENT (UTILITAIRE)
!
! NOMBRE DE NOEUDS DE LA MAILLE
!
! ----------------------------------------------------------------------
!
!
! IN  SDAPPA : NOM DE LA SD APPARIEMENT
! IN  DEFICO : SD DEFINITION DU CONTACT
! IN  POSMAI : POSITION DE LA MAILLE
! OUT NNOSDM : NOMBRE DE NOEUDS DE LA MAILLE
!
!
!
!
    integer :: ibid
!
! ----------------------------------------------------------------------
!
! --- INITIALISATIONS
!
    nnosdm = 0
!
! --- NOMBRE D'ENTITES
!
    call cfnben(defico, posmai, 'CONNEX', nnosdm, ibid)
!
end subroutine
