subroutine ccvrch(resuin, numor0)
    implicit none
!     --- ARGUMENTS ---
#include "jeveux.h"
!
#include "asterc/getexm.h"
#include "asterc/getfac.h"
#include "asterfort/rsadpa.h"
#include "asterfort/u2mesk.h"
    character(len=8) :: resuin
    integer :: numor0
! ======================================================================
! COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
!   1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
! ======================================================================
! person_in_charge: nicolas.sellenet at edf.fr
!
    integer :: nchalu, jpara
!
    character(len=8) :: k8b
    character(len=24) :: excisd
!
    nchalu=0
!
    if (getexm('EXCIT','CHARGE') .eq. 1) call getfac('EXCIT', nchalu)
!
    call rsadpa(resuin, 'L', 1, 'EXCIT', numor0,&
                0, jpara, k8b)
    excisd=zk24(jpara)
!
    if (excisd .eq. ' ') then
        if (nchalu .eq. 0) then
            call u2mesk('A', 'UTILITAI4_2', 1, resuin)
        else
            call u2mesk('A', 'UTILITAI4_1', 1, resuin)
        endif
    endif
!
end subroutine
