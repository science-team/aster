subroutine irgmcg(chamsy, partie, ifi, nomcon, ordr,&
                  nbordr, coord, connx, point, nobj,&
                  nbel, nbcmpi, nomcmp, lresu, para,&
                  nomaou, versio)
    implicit none
#include "jeveux.h"
#include "asterfort/celces.h"
#include "asterfort/cesexi.h"
#include "asterfort/codent.h"
#include "asterfort/dismoi.h"
#include "asterfort/irgmg1.h"
#include "asterfort/irgmor.h"
#include "asterfort/irgmpv.h"
#include "asterfort/irgmtb.h"
#include "asterfort/jedema.h"
#include "asterfort/jedetr.h"
#include "asterfort/jelira.h"
#include "asterfort/jemarq.h"
#include "asterfort/jeveuo.h"
#include "asterfort/rsexch.h"
#include "asterfort/u2mesk.h"
#include "asterfort/u2mess.h"
#include "asterfort/wkvect.h"
    character(len=*) :: nomcon, chamsy, nomcmp(*), partie
    character(len=8) :: nomaou
    real(kind=8) :: coord(*), para(*)
    logical :: lresu
    integer :: ifi, nbordr, nbcmpi
    integer :: versio
    integer :: ordr(*), connx(*), point(*)
!     NBRE, NOM D'OBJET POUR CHAQUE TYPE D'ELEMENT
    integer :: neletr
    parameter (neletr =  8)
    integer :: ntyele, maxel, maxno
    parameter (ntyele = 28)
    parameter (maxel  = 48)
    parameter (maxno  =  8)
    integer :: tdec(ntyele, maxel, maxno)
    integer :: typd(ntyele, 3)
    integer :: tord(neletr)
    integer :: nbel(ntyele), nbel2(ntyele), jel(ntyele)
    character(len=24) :: nobj(ntyele)
!     ------------------------------------------------------------------
! ======================================================================
! COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
!   1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
! ======================================================================
!
!        IMPRESSION D'UN CHAM_ELEM DE TYPE "ELGA","ELEM" AU FORMAT GMSH
!
!        CHAMSY : NOM DU CHAM_ELEM A ECRIRE
!        IFI    : NUMERO D'UNITE LOGIQUE DU FICHIER DE SORTIE GMSH
!        NOMCON : NOM DU CONCEPT A IMPRIMER
!        PARTIE : IMPRESSION DE LA PARTIE COMPLEXE OU REELLE DU CHAMP
!        NBORDR : NOMBRE DE NUMEROS D'ORDRE DANS LE TABLEAU ORDR
!        ORDR   : LISTE DES NUMEROS D'ORDRE A IMPRIMER
!        COORD  : VECTEUR COORDONNEES DES NOEUDS DU MAILLAGE
!        CONNX  : VECTEUR CONNECTIVITES DES NOEUDS DU MAILLAGE
!        POINT  : VECTEUR DU NOMBRE DE NOEUDS DES MAILLES DU MAILLAGE
!        NOBJ(i): NOM JEVEUX DEFINISSANT LES ELEMENTS DU MAILLAGE
!        NBEL(i): NOMBRE D'ELEMENTS DU MAILLAGE DE TYPE i
!        NBCMPI : NOMBRE DE COMPOSANTES DEMANDEES A IMPRIMER
!        NOMCMP : NOMS DES COMPOSANTES DEMANDEES A IMPRIMER
!        LRESU  : LOGIQUE INDIQUANT SI NOMCON EST UNE SD RESULTAT
!        PARA   : VALEURS DES VARIABLES D'ACCES (INST, FREQ)
!        NOMAOU : NOM DU MAILLAGE REDECOUPE
!        VERSIO : NUMERO DE LA VERSION GMSH UTILISEE (1 OU 2 )
!
!     ------------------------------------------------------------------
!
    integer :: ior, i, j, k, ine, inoe, ima, listno(8), ix, nbno
    integer :: iq
    integer :: ibid, nbcmp, ipoin, iret, jcesc, jcesl
    integer :: jtabc, jtabd, jtabv, jtabl, jcesk, jcesd, jtype
    integer :: icmp, jncmp, ipt, isp, nbpt, nbsp, jnumol
    integer :: nbma, ncmpu, iad, nbcmpd, nbord2, iadmax, iadmm
    logical :: iwri
    character(len=1) :: tsca
    character(len=8) :: k8b, nomgd, type, nocmp
    character(len=19) :: noch19, champs
    character(len=24) :: numold
!     ------------------------------------------------------------------
!
    call jemarq()
!
! --- TABLEAU DES INFOS DE DECOUPAGE
    call irgmtb(tdec, typd, versio)
!
! --- ORDRE D'IMPRESSION DES VALEURS
    call irgmor(tord, versio)
!
    nbord2 = max(1,nbordr)
    numold = nomaou//'.NUMOLD         '
!
    call wkvect('&&IRGMCG.CESD', 'V V I', nbord2, jtabd)
    call wkvect('&&IRGMCG.CESC', 'V V I', nbord2, jtabc)
    call wkvect('&&IRGMCG.CESV', 'V V I', nbord2, jtabv)
    call wkvect('&&IRGMCG.CESL', 'V V I', nbord2, jtabl)
    call wkvect('&&IRGMCG.TYPE', 'V V K8', nbord2, jtype)
!
    nbcmp = 0
!
    do 60 ior = 1, nbord2
        if (lresu) then
            call rsexch(' ', nomcon, chamsy, ordr(ior), noch19,&
                        iret)
            if (iret .ne. 0) goto 60
        else
            noch19 = nomcon
        endif
        call codent(ior, 'D0', k8b)
        champs = '&&IRGMCG.CH'//k8b
        call celces(noch19, 'V', champs)
        call jeveuo(champs//'.CESK', 'L', jcesk)
        call jeveuo(champs//'.CESD', 'L', zi(jtabd+ior-1))
        call jeveuo(champs//'.CESC', 'L', zi(jtabc+ior-1))
        call jeveuo(champs//'.CESV', 'L', zi(jtabv+ior-1))
        call jeveuo(champs//'.CESL', 'L', zi(jtabl+ior-1))
        call jelira(champs//'.CESV', 'TYPE', ibid, zk8(jtype+ior-1))
!
        nomgd = zk8(jcesk-1+2)
        call dismoi('F', 'TYPE_SCA', nomgd, 'GRANDEUR', ibid,&
                    tsca, ibid)
        if (tsca .ne. 'R') then
            call u2mess('F', 'ALGORITH2_63')
        endif
!
        type = zk8(jcesk-1+3)
        if (type(1:4) .ne. 'ELGA' .and. type(1:4) .ne. 'ELEM') then
            call u2mess('F', 'PREPOST2_56')
        endif
!
        if (ior .eq. 1) then
            jcesc = zi(jtabc+ior-1)
            jcesd = zi(jtabd+ior-1)
            jcesl = zi(jtabl+ior-1)
            nbma = zi(jcesd-1+1)
            nbcmp = zi(jcesd-1+2)
            ncmpu = 0
            call wkvect('&&IRGMCG.NOCMP', 'V V K8', nbcmp, jncmp)
            do 50,icmp = 1,nbcmp
            do 30,ima = 1,nbma
            nbpt = zi(jcesd-1+5+4* (ima-1)+1)
            nbsp = zi(jcesd-1+5+4* (ima-1)+2)
            do 20,ipt = 1,nbpt
            do 10,isp = 1,nbsp
            call cesexi('C', jcesd, jcesl, ima, ipt,&
                        isp, icmp, iad)
            if (iad .gt. 0) goto 40
10          continue
20          continue
30          continue
            goto 50
40          continue
            ncmpu = ncmpu + 1
            zk8(jncmp+ncmpu-1) = zk8(jcesc-1+icmp)
50          continue
        else
            if (zi(zi(jtabd+ior-1)-1+2) .ne. nbcmp) then
                call u2mess('F', 'PREPOST2_53')
            endif
        endif
!
60  end do
!
! --- RECUPERATION DU TABLEAU DE CORRESPONDANCE ENTRE NUMERO DES
!     NOUVELLES MAILLES ET NUMERO DE LA MAILLE INITIALE
!     CREE PAR IRGMMA
!     ***************
    call jeveuo(numold, 'L', jnumol)
    do 101 i = 1, ntyele
        nbel2(i)=0
101  end do
!
! --- BOUCLE SUR LE NOMBRE DE COMPOSANTES DU CHAM_ELEM
!     *************************************************
    if (nbcmpi .eq. 0) then
        nbcmpd = nbcmp
    else
        nbcmpd = nbcmpi
    endif
!
    do 270 k = 1, nbcmpd
!
        if (nbcmpi .ne. 0) then
            do 70 ix = 1, nbcmp
                if (zk8(jncmp+ix-1) .eq. nomcmp(k)) then
                    icmp = ix
                    goto 80
                endif
70          continue
            k8b = nomcmp(k)
            call u2mesk('F', 'PREPOST2_54', 1, k8b)
80          continue
        else
            icmp = k
        endif
        nocmp = zk8(jncmp+icmp-1)
!
! ----- PREMIER PASSAGE POUR DETERMINER SI LE CHAMP A ECRIRE EXISTE
!       SUR LES POI1, SEG2, TRIA3, TETR4...
!       DONC ON  N'ECRIT RIEN
        iwri = .false.
!
! ----- BOUCLE SUR LES ELEMENTS DANS L'ORDRE DONNE PAR IRGMOR
!
        do 120 ine = 1, neletr
!         I=NUM DE L'ELEMENT DANS LE CATALOGUE
            i=tord(ine)
            if (nbel(i) .ne. 0) then
                iadmm = 0
!           NBNO=NBRE DE NOEUDS DE CET ELEMENT
                nbno = typd(i,3)
                call jeveuo(nobj(i), 'L', jel(i))
                do 1201 iq = 1, nbel(i)
                    ima = zi(jel(i)-1+iq)
                    call irgmg1(zi(jnumol), ima, nbord2, zi(jtabd), zi( jtabl),&
                                zi(jtabv), partie, jtype, nbno, icmp,&
                                ifi, iwri, iadmax)
                    iadmm = max(iadmax,iadmm)
1201              continue
                if (iadmm .gt. 0) nbel2(i) = nbel(i)
            endif
120      continue
!
!
! ----- ECRITURE DE L'ENTETE DE View
!       ****************************
!
        call irgmpv(ifi, lresu, nomcon, chamsy, nbord2,&
                    para, nocmp, nbel2, .true., .false.,&
                    .false., versio)
!
        iwri = .true.
!
! ----- BOUCLE SUR LES ELEMENTS DANS L'ORDRE DONNE PAR IRGMOR
!
        do 130 ine = 1, neletr
!         I=NUM DE L'ELEMENT DANS LE CATALOGUE
            i=tord(ine)
            if (nbel2(i) .ne. 0) then
!           NBNO=NBRE DE NOEUDS DE CET ELEMENT
                nbno = typd(i,3)
                call jeveuo(nobj(i), 'L', jel(i))
                do 1301 iq = 1, nbel(i)
                    ima = zi(jel(i)-1+iq)
                    ipoin = point(ima)
                    do 1302 inoe = 1, nbno
                        listno(inoe) = connx(ipoin-1+inoe)
1302                  continue
                    do 1303 j = 1, 3
                        write(ifi,1000) (coord(3*(listno(inoe)-1)+j),&
                        inoe=1,nbno)
1303                  continue
                    call irgmg1(zi(jnumol), ima, nbord2, zi(jtabd), zi( jtabl),&
                                zi(jtabv), partie, jtype, nbno, icmp,&
                                ifi, iwri, iadmax)
1301              continue
            endif
130      continue
!
! ----- FIN D'ECRITURE DE View
!       **********************
!
        write (ifi,1010) '$EndView'
!
270  end do
!
    call jedetr('&&IRGMCG.CESC')
    call jedetr('&&IRGMCG.CESD')
    call jedetr('&&IRGMCG.CESV')
    call jedetr('&&IRGMCG.CESL')
    call jedetr('&&IRGMCG.NOCMP')
    call jedetr('&&IRGMCG.TYPE')
    call jedema()
!
    1000 format (1p,4(e15.8,1x))
    1010 format (a8)
!
end subroutine
