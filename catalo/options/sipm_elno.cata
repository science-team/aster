%& LIBRARY OPTIONS
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% person_in_charge: jean-luc.flejou at edf.fr
SIPM_ELNO
       <<  SIPM_ELNO : CALCUL DES CONTRAINTES MAXI ET MINI AUX NOEUDS DANS LA SECTION
           DE POUTRE A PARTIR DES EFFORTS GENERALISES.
           LICITE EN LINEAIRE SEULEMENT. >>
OPTION__
   IN__
        PGEOMER  GEOM_R 'MAIL!.COORDO'
       <<  PGEOMER : COORDONNEES DES NOEUDS >>
        PDEPLAR  DEPL_R  'RESU!DEPL!N'
       <<  PDEPLAR : DEPLACEMENTS INSTANT ACTUEL >>
        PMATERC  ADRSJEVE 'CHMA!.MATE_CODE'
       <<  PMATERC : CHAMP DE MATERIAU >>
        PTEMPSR  INST_R 'VOLA!&&CCPARA.CH_INST_R'
       <<  PTEMPSR :  INSTANT ACTUEL >>
        PVARCPR  VARI_R 'VOLA!&&CCPARA.VARI_INT_N'
       <<  PVARCPR : VARIABLES DE COMMANDE  >>
        PVARCRR  VARI_R 'VOLA!&&CCPARA.VARI_INT_REF'
       << PVARCRR : VARIABLES DE COMMANDES  DE REFERENCE >>
        PCAORIE  CAORIE 'CARA!.CARORIEN'
       <<  PCAORIE : ORIENTATION LOCALE D'UN ELEMENT DE POUTRE OU DE TUYAU,
           ISSUE DE AFFE_CARA_ELEM MOT CLE ORIENTATION >>
        PCAGEPO  CAGEPO 'CARA!.CARGEOPO'
       <<  PCAGEPO : CARACTERISTIQUE POUTRES, SECTION RECTANGLE OU CERCLE,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PCAGNPO  CAGNPO 'CARA!.CARGENPO'
       <<  PCAGNPO : CARACTERISTIQUES GEOMETRIQUES D'UNE SECTION DE POUTRE,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PCAARPO  CAARPO 'CARA!.CARARCPO'
       <<  PCAARPO : CARACTERISTIQUE DE POUTRE COURBE POU_C_T,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PPESANR  PESA_R
       <<  PPESANR : CHARGE PESANTEUR POUR UNE MODELISATION POUTRE >>
        PFF1D1D  FORC_F
       <<  PFF1D1D : CHARGE REPARTIE DE TYPE FONCTION POUR UNE MODELISATION POUTRE >>
        PFR1D1D  FORC_R
       <<  PFR1D1D : CHARGE REPARTIE DE TYPE REEL POUR UNE MODELISATION POUTRE >>
        PCHDYNR  DEPL_R
       <<  PCHDYNR : CHAMP D'ACCELERATIONS  >>
        PSUROPT  NEUT_K24
       <<  PSUROPT : OPTION DE CALCUL DE LA MASSE POUR UNE MODELISATION POUTRE  >>
        PCOEFFR  IMPE_R
       <<  PCOEFFR : COEF_MULT DE LA CHARGE,
           PRESENCE DE CHARGE REPARTIE POUR UNE MODELISATION POUTRE  >>
        PCOEFFC  IMPE_C
       <<  PCOEFFC : COEF_MULT_C DE LA CHARGE,
           PRESENCE DE CHARGE REPARTIE POUR UNE MODELISATION POUTRE  >>
        
   OUT__
PCONTRR SIEF_R ELNO__
PCONTRC SIEF_C ELNO__
