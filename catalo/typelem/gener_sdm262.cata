%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% ======================================================================
GENER_SDM262
TYPE_GENE__

ENTETE__ ELEMENT__ DHH2Q9_SUDM       MAILLE__ QUAD9
   ELREFE__  QU9    GAUSS__   RIGI=NOEU_S   FPGVARI=FPG5   FPGCONT=FPG5 FPG1=FPG1  FPG_LISTE__  MATER = (FPG1)
   ELREFE__  QU4    GAUSS__   RIGI=NOEU_S
   ENS_NOEUD__  EN3     =     5  6  7  8   9
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4

ENTETE__ ELEMENT__ DHH2T7_SUDM       MAILLE__ TRIA7
   ELREFE__  TR7    GAUSS__   RIGI=NOEU_S   FPGVARI=FPG4   FPGCONT=FPG4 FPG1=FPG1  FPG_LISTE__  MATER = (FPG1)
   ELREFE__  TR3    GAUSS__   RIGI=NOEU_S
   ENS_NOEUD__  EN3     =     4  5  6  7
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3

MODE_LOCAL__
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR ALPHA)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     NVI_C    NVI_T
                                        NVI_H    NVI_M    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN3   (PRE1     PRE2     )
    CFLUXF   = FTHM_F   ELEM__         (PFLU1    PFLU2   )
    EFLUXE   = FTHM_R   ELGA__ RIGI   (PFLU1    PFLU2  )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[8]     )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[8]     )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    ECONTPG  = SIEF_R   ELGA__ FPGCONT (M11      FH11     M12      FH12
                                        M21      FH21     M22      FH22   )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (M11      FH11     M12      FH12
                                        M21      FH21     M22      FH22   )
    EREFCO   = PREC     ELEM__         (SIGM     FHYDR1   FHYDR2   FTHERM     )
    CTEREFE  = TEMP_R   ELEM__         (TEMP     )
    ZVARI1   = VARI_R   ELEM__         (VARI     )
    ZVARIPG  = VARI_R   ELGA__ FPGVARI (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )

    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM            581   IN__   ECONTPG  PEPCON1  ECONTPG  PEPCON2
                              OUT__  ECONTPG  PEPCON3
    FORC_NODA           520   IN__   ECONTPG  PCONTMR
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           520   IN__   ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARIPG  PVARIMR  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
                                     MMATUNS  PMATUNS
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    RAPH_MECA           520   IN__   ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARIPG  PVARIMR  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    RIGI_MECA_TANG      520   IN__   ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARIPG  PVARIMR  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                              OUT__  MMATUNS  PMATUNS
    TOU_INI_ELGA        99    IN__
                              OUT__  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO        99    IN__
                              OUT__  ENGEOM_R PGEOM_R  ENINST_R PINST_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R
