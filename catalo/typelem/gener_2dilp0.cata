%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% person_in_charge: romeo.fernandes at edf.fr
GENER_2DILP0
TYPE_GENE__

ENTETE__ ELEMENT__ TR7_DP_2D          MAILLE__ TRIA7
   ELREFE__  TR7       GAUSS__  RIGI=FPG3   FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TR6       GAUSS__  RIGI=FPG3
   ELREFE__  TR3       GAUSS__  RIGI=FPG3
   ELREFE__  SE3       GAUSS__  RIGI=FPG4
   ENS_NOEUD__  EN3     =     7
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3

ENTETE__ ELEMENT__ QU9_DP_2D         MAILLE__ QUAD9
   ELREFE__  QU9       GAUSS__  RIGI=FPG4  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  QU8       GAUSS__  RIGI=FPG4
   ELREFE__  QU4       GAUSS__  RIGI=FPG4
   ELREFE__  SE3       GAUSS__  RIGI=FPG4
   ENS_NOEUD__  EN3     =     9
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4

MODE_LOCAL__
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     NVI_C    NVI_T
                                        NVI_H    NVI_M    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       GONF     )
                                 EN2   (DX       DY       )
                                 EN3   (PRES     )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (DEPV     DGONFX1  DGONFX2  PRES     )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (DEPV     DGONFX1  DGONFX2  PRES     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (DEPV     DGONFX1  DGONFX2  PRES     )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGINDLO  = INDL_R   ELGA__ RIGI    (INDICE   DIR[4]   )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    E1NEUTK  = NEUT_K24 ELEM__         (Z1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    E1GNEUT  = NEUT_R   ELGA__ RIGI    (X1       )
    E1NNEUT  = NEUT_R   ELNO__ IDEN__  (X1       )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (PRES     SIG1     SIG2     DEPV     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (PRES     SIG1     SIG2     DEPV     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (PRES     SIG1     SIG2     DEPV     )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM            581   IN__   ECONTPG  PEPCON1  ECONTPG  PEPCON2
                              OUT__  ECONTPG  PEPCON3
    CARA_GEOM           -1    IN__     OUT__  XXXXXX   XXXXXX
    CHAR_LIMITE         -1    IN__     OUT__  XXXXXX   XXXXXX
    COOR_ELGA           479   IN__   NGEOMER  PGEOMER OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__     OUT__  XXXXXX   XXXXXX
    EPOT_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPEQ_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPEQ_ELNO           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPSI_ELGA           5     IN__   DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                              OUT__  EDEFOPG  PDEFOPG
    EPSI_ELNO           4     IN__   EDEFOPG  PDEFOPG
                              OUT__  EDEFONO  PDEFONO
    FORC_NODA           5     IN__   ECONTPG  PCONTMR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER  CCOMPOR  PCOMPOR
                                     CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           5     IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    INDL_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_ENER          -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__     OUT__  XXXXXX   XXXXXX
    INIT_VARC           99    IN__  
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER           -1    IN__     OUT__  XXXXXX   XXXXXX
    NORME_L2            -1    IN__   XXXXXX   XXXXXX  
                              OUT__  XXXXXX   XXXXXX
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    RAPH_MECA           5     IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    RICE_TRACEY         -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA_TANG      5     IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARIPG  PVARIMR
                              OUT__  MMATUNS  PMATUNS
    RIGI_MECA_ELAS      5     IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARIPG  PVARIMR
                              OUT__  MMATUNS  PMATUNS
    SIEF_ELNO           4     IN__   ECONTPG  PCONTRR
                              OUT__  ECONTNO  PSIEFNOR ECONTNC  PSIEFNOC
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOU_INI_ELGA        99    IN__  
                              OUT__  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO        99    IN__  
                              OUT__  ENGEOM_R PGEOM_R  ENINST_R PINST_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R
    VAEX_ELGA           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARIPG  PVARIGR
                              OUT__  E1GNEUT  PVARIGS
    VAEX_ELNO           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARINO  PVARINR
                              OUT__  E1NNEUT  PVARINS
    VARI_ELNO           4     IN__   ZVARIPG  PVARIGR
                              OUT__  ZVARINO  PVARINR
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__     OUT__  XXXXXX   XXXXXX
