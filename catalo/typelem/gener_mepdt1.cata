%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_MEPDT1
TYPE_GENE__

ENTETE__ ELEMENT__ MECA_POU_D_T     MAILLE__ SEG2
   ELREFE__  SE2    GAUSS__  RIGI=FPG3  NOEU=NOEU  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI NOEU FPG1)

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CABSCUR  = ABSC_R   ELEM__         (ABSC1    ABSC2    )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAGRPO  = CAGEPO   ELEM__         (HY1      HZ1      EPY1     EPZ1     HY2      HZ2
                                        EPY2     EPZ2     R1       EP1      R2       EP2
                                        TSEC     )
    CCAGEPO  = CAGEPO   ELEM__         (R1       EP1      )
    CCAGNPO  = CAGNPO   ELEM__         (A1       IY1      IZ1      AY1      AZ1      EY1
                                        EZ1      JX1      RY1      RZ1      RT1      A2
                                        IY2      IZ2      AY2      AZ2      EY2      EZ2
                                        JX2      RY2      RZ2      RT2      TVAR     )
    CCAORIE  = CAORIE   ELEM__         (ALPHA    BETA     GAMMA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    NDEPLAC  = DEPL_C   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ      )
    DDL_MECA = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ      )
    EDEPLRPG = DEPL_R   ELGA__ RIGI    (DX       DY       DZ       DRX      DRY      DRZ      )
    EDEPLCPG = DEPL_C   ELGA__ RIGI    (DX       DY       DZ       DRX      DRY      DRZ      )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    NVITER   = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       )
    EENERR   = ENER_R   ELEM__         (TOTALE   TRAC_COM TORSION  FLEX_Y   FLEX_Z   )
    EENECNO  = ENER_R   ELEM__         (TOTALE   DX       DY       DZ       DRX      DRY      DRZ      )
    CEPSINR  = EPSI_R   ELEM__         (EPX      KY       KZ       )
    EDEFGNO  = EPSI_R   ELNO__ IDEN__  (EPXX     GAXY     GAXZ     GAT      KY       KZ       )
    EDEFGPG  = EPSI_R   ELGA__ RIGI    (EPXX     GAXY     GAXZ     GAT      KY       KZ       )
    CFRELEC  = FELECR   ELEM__         (X1       Y1       Z1       X2       Y2       Z2
                                        CODE     )
    CFLAPLA  = FLAPLA   ELEM__         (NOMAIL   NOGEOM   )
    CFORCEC  = FORC_C   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCER  = FORC_R   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    COMEG2R  = OME2_R   ELEM__         (OMEG2    )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    CCOEFC   = IMPE_C   ELEM__         (IMPE     )
    CCOEFR   = IMPE_R   ELEM__         (IMPE     )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CLISTMA  = LISTMA   ELEM__         (LISTMA   TRANS    )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    CSUROPT  = NEUT_K24 ELEM__         (Z1       )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CROTATR  = ROTA_R   ELEM__         (OME    AR     BR     CR     X      Y     Z  )
    CMASDIA  = POSI     ELEM__         (POS      )
    EEFGEGA  = SIEF_R   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    EEFGEGC  = SIEF_C   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    EEFFONO  = SIEF_R   ELNO__ IDEN__  (FX       FY       FZ       MX       MY       MZ       )
    EEFGENO  = SIEF_R   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    EEFGENC  = SIEF_C   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPO  = SIEF_R   ELNO__ IDEN__  (SN       SVY      SVZ      SMT      SMFY     SMFZ     )
    ECONTPC  = SIEF_C   ELNO__ IDEN__  (SN       SVY      SVZ      SMT      SMFY     SMFZ     )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    CVENTCX  = VENTCX_F ELEM__         (FCXP     )
    EREFCO   = PREC     ELEM__         (EFFORT    MOMENT)
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    EEINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EENEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    EENEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )
    ENONLIN  = NEUT_I   ELEM__         (X1)
    ESTRAUX  = STRX_R   ELGA__ RIGI    (ALPHA    BETA     GAMMA)

VECTEUR__
    MVECTUC = VDEP_C NDEPLAC
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUC = MDEP_C NDEPLAC NDEPLAC
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM            581   IN__   EEFGEGA  PEPCON1  EEFGEGA  PEPCON2
                              OUT__  EEFGEGA  PEPCON3
    AMOR_MECA           50    IN__   NGEOMER  PGEOMER  MMATUUR  PMASSEL  CMATERC  PMATERC  MMATUUR  PRIGIEL
                                     ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    CARA_GEOM           -1    IN__     OUT__  XXXXXX   XXXXXX
    CHAR_LIMITE         -1    IN__     OUT__  XXXXXX   XXXXXX
    CHAR_MECA_EPSI_R    39    IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CEPSINR  PEPSINR  NGEOMER  PGEOMER
                                     CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FC1D1D    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCEC  PFC1D1D  NGEOMER  PGEOMER

                              OUT__  MVECTUC  PVECTUC
    CHAR_MECA_FF1D1D    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCEF  PFF1D1D  NGEOMER  PGEOMER
                                     CTEMPSR  PTEMPSR
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FR1D1D    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCER  PFR1D1D  NGEOMER  PGEOMER

                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FRELEC    145   IN__   CFRELEC  PFRELEC  NGEOMER  PGEOMER
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FRLAPL    148   IN__   CFLAPLA  PFLAPLA  NGEOMER  PGEOMER  CLISTMA  PLISTMA
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PESA_R    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CPESANR  PPESANR
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_ROTA_R    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CROTATR  PROTATR
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SF1D1D    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     CFORCEF  PFF1D1D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SR1D1D    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                     NGEOMER  PGEOMER  CVENTCX  PVENTCX  NVITER   PVITER

                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_TEMP_R    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_HYDR_R    150   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  CTEMPSR  PTEMPSR

                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SECH_R    150   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  CTEMPSR  PTEMPSR
                                     CCOMPOR  PCOMPOR
                              OUT__  MVECTUR  PVECTUR
    COOR_ELGA           478   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    DEGE_ELNO           158   IN__   CCAGEPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                              OUT__  EDEFGNO  PDEFOGR
    DEGE_ELGA           158   IN__   DDL_MECA PDEPLAR
                                     NGEOMER  PGEOMER  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                              OUT__  EDEFGPG  PDEFOPG
    ECIN_ELEM           151   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  COMEG2R  POMEGA2
                                     NGEOMER  PGEOMER  CMASDIA  PMASDIA  CMATERC  PMATERC  ZVARCPG  PVARCPR
                                     DDL_MECA PVITESR
                              OUT__  EENECNO  PENERCR
    EFGE_ELGA           546   IN__   EEFGEGA  PSIEFR
                              OUT__  EEFGEGA  PEFGER    EEFGEGC  PEFGEC
    EFGE_ELNO           185   IN__   CCAGEPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                     EEFGEGA  PCONTRR  ENONLIN  PNONLIN  CCOMPOR  PCOMPOR
                                     DDL_MECA PCHDYNR  CCOEFC   PCOEFFC  CCOEFR   PCOEFFR  DDL_MECA PDEPLAR
                                     CFORCEF  PFF1D1D  CFORCER  PFR1D1D  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CPESANR  PPESANR  CSUROPT  PSUROPT  ZVARCPG  PVARCPR
                                     CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR
                              OUT__  EEFGENO  PEFFORR  EEFGENC  PEFFORC
    ENEL_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__     OUT__  XXXXXX   XXXXXX
    EPOT_ELEM           151   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                              OUT__  EENERR   PENERDR
    FORC_NODA           347   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  EEFGEGA  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  ESTRAUX  PSTRXMR
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           247   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                     EEFGEGA  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  ESTRAUX  PSTRXMR
                              OUT__  ECODRET  PCODRET  EEFGEGA  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR  ESTRAUX  PSTRXPR
    FULL_MECA_ELAS      -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_ENER          -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__     OUT__  XXXXXX   XXXXXX
    INI_STRX            23    IN__   CCAORIE  PCAORIE
                              OUT__  ESTRAUX  PSTRX_R
    INIT_VARC           99    IN__
                              OUT__  ZVARCPG  PVARCPR
    MASS_FLUI_STRU      141   IN__   CABSCUR  PABSCUR  CCAGEPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                     NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    MASS_INER           38    IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  EMASSINE PMASSINE
    MASS_MECA           141   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    MASS_MECA_DIAG      141   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    MASS_MECA_EXPLI     141   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    MECA_GYRO           259   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  MMATUNS  PMATUNS
    M_GAMMA             141   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PACCELR  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  ZVARCPG  PVARCPR
                              OUT__  MVECTUR  PVECTUR
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    PAS_COURANT         404   IN__   CMATERC  PMATERC  NGEOMER  PGEOMER
                              OUT__  ECOURAN  PCOURAN
    RAPH_MECA           247   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                     EEFGEGA  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  ESTRAUX  PSTRXMR
                              OUT__  ECODRET  PCODRET  EEFGEGA  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
                                     ESTRAUX  PSTRXPR
    REFE_FORC_NODA      347   IN__   EREFCO   PREFCO
                              OUT__  MVECTUR  PVECTUR
    REPERE_LOCAL        135   IN__   CCAORIE  PCAORIE
                              OUT__  CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    RICE_TRACEY         -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_FLUI_STRU      140   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    RIGI_GYRO           262   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  MMATUNS  PMATUNS
    RIGI_MECA           140   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_ELAS      -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA_GE        143   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  EEFGEGA  PEFFORR  NGEOMER  PGEOMER
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_HYST      50    IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  MMATUUR  PRIGIEL  ZVARCPG  PVARCPR
                              OUT__  MMATUUC  PMATUUC
    RIGI_MECA_RO        -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA_TANG      247   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                     EEFGEGA  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER
                                     CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  ESTRAUX  PSTRXMR
                              OUT__  MMATUUR  PMATUUR
    SIEF_ELGA           144   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR
                                     NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR
                              OUT__  EEFGEGA  PCONTRR
                                     EEFGEGC  PCONTRC
    SIEF_ELNO           347   IN__   CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  EEFGEGA  PCONTRR
                                     DDL_MECA PDEPPLU  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR
                              OUT__  EEFGENO  PSIEFNOR  EEFGENC  PSIEFNOC
    SIPM_ELNO           149   IN__   CCAGRPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                     DDL_MECA PCHDYNR  CCOEFC   PCOEFFC  CCOEFR   PCOEFFR  DDL_MECA PDEPLAR
                                     CFORCEF  PFF1D1D  CFORCER  PFR1D1D  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CPESANR  PPESANR  CSUROPT  PSUROPT  ZVARCPG  PVARCPR
                                     CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR
                              OUT__  ECONTNO  PCONTRR  ECONTNC  PCONTRC
    SIPO_ELNO           149   IN__   CABSCUR  PABSCUR  CCAGRPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                     DDL_MECA PCHDYNR  CCOEFC   PCOEFFC  CCOEFR   PCOEFFR  DDL_MECA PDEPLAR
                                     CFORCEF  PFF1D1D  CFORCER  PFR1D1D  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CPESANR  PPESANR  CSUROPT  PSUROPT  ZVARCPG  PVARCPR
                                     CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR
                              OUT__  ECONTPO  PCONTPO  ECONTPC  PCONTPC
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOU_INI_ELEM        99    IN__
                              OUT__  CGEOMER  PGEOM_R
    TOU_INI_ELGA        99    IN__
                              OUT__  EEFGEGA  PSIEF_R  ZVARIPG  PVARI_R  EGGEOM_R PGEOM_R  EGINST_R PINST_R
                                     EGNEUT_R PNEUT_R  EGNEUT_F PNEUT_F  EDEPLRPG PDEPL_R  EDEPLCPG PDEPL_C
    TOU_INI_ELNO        99    IN__
                              OUT__  EEFGENO  PSIEF_R  ZVARINO  PVARI_R  NGEOMER PGEOM_R  EEINST_R PINST_R
                                     EENEUT_F PNEUT_F  EENEUT_R PNEUT_R
    VARI_ELNO           347   IN__   ZVARIPG  PVARIGR  CCOMPOR  PCOMPOR
                              OUT__  ZVARINO  PVARINR
    WEIBULL             -1    IN__     OUT__  XXXXXX   XXXXXX
