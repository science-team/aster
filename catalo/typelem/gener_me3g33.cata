%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME3G33
TYPE_GENE__

ENTETE__ ELEMENT__ THV_HEXA20D     MAILLE__ HEXA20
   ELREFE__  H20       GAUSS__  RIGI=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  HE8       GAUSS__  RIGI=NOEU_S
   ELREFE__  QU8       GAUSS__  RIGI=FPG9
   ENS_NOEUD__  EN2     =     9  10  11  12  13  14  15  16  17  18  19  20
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6  7  8
ENTETE__ ELEMENT__ THV_PENTA15D    MAILLE__ PENTA15
   ELREFE__  P15       GAUSS__  RIGI=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  PE6       GAUSS__  RIGI=NOEU_S
   ELREFE__  QU8       GAUSS__  RIGI=FPG9
   ELREFE__  TR6       GAUSS__  RIGI=FPG6
   ENS_NOEUD__  EN2     =     7  8  9  10  11  12  13  14  15
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6
ENTETE__ ELEMENT__ THV_TETRA10D    MAILLE__ TETRA10
   ELREFE__  T10       GAUSS__  RIGI=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TE4       GAUSS__  RIGI=NOEU_S
   ELREFE__  TR6       GAUSS__  RIGI=FPG6
   ENS_NOEUD__  EN2     =     5  6  7  8  9  10
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ THV_HEXA20S     MAILLE__ HEXA20
   ELREFE__  H20       GAUSS__  RIGI=FPG8NOS  MASS=FPG8  NOEU_S=NOEU_S  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  HE8       GAUSS__  RIGI=FPG8NOS  MASS=FPG8  NOEU_S=NOEU_S
   ELREFE__  QU8       GAUSS__  RIGI=FPG4     MASS=FPG4  NOEU_S=NOEU_S
   ENS_NOEUD__  EN2     =     9  10  11  12  13  14  15  16  17  18  19  20
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6  7  8
ENTETE__ ELEMENT__ THV_PENTA15S    MAILLE__ PENTA15
   ELREFE__  P15       GAUSS__  RIGI=FPG6NOS  MASS=FPG6  NOEU_S=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  PE6       GAUSS__  RIGI=FPG6NOS  MASS=FPG6  NOEU_S=NOEU_S
   ELREFE__  QU8       GAUSS__  RIGI=FPG9     MASS=FPG4  NOEU_S=NOEU_S
   ELREFE__  TR6       GAUSS__  RIGI=FPG3     MASS=FPG3  NOEU_S=NOEU_S
   ENS_NOEUD__  EN2     =     7  8  9  10  11  12  13  14  15
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6
ENTETE__ ELEMENT__ THV_TETRA10S    MAILLE__ TETRA10
   ELREFE__  T10       GAUSS__  RIGI=FPG4NOS  MASS=FPG4  NOEU_S=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TE4       GAUSS__  RIGI=FPG4NOS  MASS=FPG4  NOEU_S=NOEU_S
   ELREFE__  TR6       GAUSS__  RIGI=FPG3     MASS=FPG3  NOEU_S=NOEU_S
   ENS_NOEUD__  EN2     =     5  6  7  8  9  10
   ENS_NOEUD__  EN1     =     1  2  3  4

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     NVI_C    NVI_T
                                        NVI_H    NVI_M    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (PRE1     TEMP     )
                                 EN2   (         )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]     )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    E1NEUTK  = NEUT_K24 ELEM__         (Z1       )
    E1GNEUT  = NEUT_R   ELGA__ RIGI    (X1       )
    E1NNEUT  = NEUT_R   ELNO__ IDEN__  (X1       )
    EREFCO   = PREC     ELEM__         (SIGM     FHYDR1   FHYDR2   FTHERM   )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (M11      FH11X    FH11Y    FH11Z    ENT11    M12
                                        FH12X    FH12Y    FH12Z    ENT12    QPRIM    FHTX
                                        FHTY     FHTZ     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (M11      FH11X    FH11Y    FH11Z    ENT11    M12
                                        FH12X    FH12Y    FH12Z    ENT12    QPRIM    FHTX
                                        FHTY     FHTZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (M11      FH11X    FH11Y    FH11Z    ENT11    M12
                                        FH12X    FH12Y    FH12Z    ENT12    QPRIM    FHTX
                                        FHTY     FHTZ     )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30])
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30])
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    CARA_GEOM           -1    IN__     OUT__  XXXXXX   XXXXXX
    CHAR_LIMITE         -1    IN__     OUT__  XXXXXX   XXXXXX
    COOR_ELGA           488   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__     OUT__  XXXXXX   XXXXXX
    EPOT_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    FORC_NODA           600   IN__   ECONTPG  PCONTMR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    INDIC_ENER          -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__     OUT__  XXXXXX   XXXXXX
    INIT_VARC           99    IN__  
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER           -1    IN__     OUT__  XXXXXX   XXXXXX
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    NORME_L2            -1    IN__     OUT__  XXXXXX   XXXXXX
    RAPH_MECA           600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      600   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  EREFCO   PREFCO
                              OUT__  MVECTUR  PVECTUR
    RICE_TRACEY         -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA           -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA_TANG      600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  MMATUNS  PMATUNS
    SIEF_ELNO           600   IN__   ECONTPG  PCONTRR
                              OUT__  ECONTNO  PSIEFNOR
                                     ECONTNC  PSIEFNOC
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOU_INI_ELGA        99    IN__  
                              OUT__  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R  EGNEUT_R PNEUT_R EGNEUT_F PNEUT_F
    VAEX_ELGA           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARIPG  PVARIGR
                              OUT__  E1GNEUT  PVARIGS
    VAEX_ELNO           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARINO  PVARINR
                              OUT__  E1NNEUT  PVARINS
    VARI_ELNO           600   IN__   ZVARIPG  PVARIGR  CCOMPOR  PCOMPOR
                              OUT__  ZVARINO  PVARINR
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__     OUT__  XXXXXX   XXXXXX
