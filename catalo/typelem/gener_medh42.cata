%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% person_in_charge: sylvie.granet at edf.fr
GENER_MEDH42
TYPE_GENE__

ENTETE__ ELEMENT__ THHM_DPQ8D       MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=NOEU_S   MASS=NOEU_S  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  QU4       GAUSS__  RIGI=NOEU_S
   ELREFE__  SE3       GAUSS__  RIGI=FPG4
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ THHM_DPTR6D      MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=NOEU_S   MASS=NOEU_S  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=NOEU_S
   ELREFE__  SE3       GAUSS__  RIGI=FPG4
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3
ENTETE__ ELEMENT__ THHM_DPQ8S         MAILLE__ QUAD8
   ELREFE__  QU8    GAUSS__   RIGI=FPG4NOS   MASS=FPG4  NOEU_S=NOEU_S   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  QU4    GAUSS__   RIGI=FPG4NOS   MASS=FPG4  NOEU_S=NOEU_S
   ELREFE__  SE3    GAUSS__   RIGI=FPG4
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ THHM_DPTR6S        MAILLE__ TRIA6
   ELREFE__  TR6    GAUSS__   RIGI=FPG3NOS   MASS=FPG3  NOEU_S=NOEU_S  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TR3    GAUSS__   RIGI=FPG3NOS   MASS=FPG3  NOEU_S=NOEU_S
   ELREFE__  SE3    GAUSS__   RIGI=FPG4
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3

MODE_LOCAL__
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    NMATERC  = ADRSJEVE ELNO__  IDEN__ (I1       )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT[9]     NVI_C    NVI_T
                                        NVI_H    NVI_M    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       PRE1     PRE2     TEMP     )
                                 EN2   (DX       DY       )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDFEQPG  = EPSI_R   ELGA__ RIGI    (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDFEQNO  = EPSI_R   ELNO__ IDEN__  (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGINDLO  = INDL_R   ELGA__ RIGI    (INDICE   DIR[4]   )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    ECARAGE  = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      IXR2     IYR2     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30])
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    E1NEUTK  = NEUT_K24 ELEM__         (Z1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30])
    E1GNEUT  = NEUT_R   ELGA__ RIGI    (X1       )
    E1NNEUT  = NEUT_R   ELNO__ IDEN__  (X1       )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    EREFCO   = PREC     ELEM__         (SIGM     FHYDR1   FHYDR2   FTHERM   )
    ESIGMPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ)
    ESIGMPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ)
    ESIGMNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ)
    ESIGMNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ)
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ
                                        SIP      M11      FH11X    FH11Y    ENT11    M12
                                        FH12X    FH12Y    ENT12    M21      FH21X    FH21Y
                                        ENT21    QPRIM    FHTX     FHTY     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ
                                        SIP      M11      FH11X    FH11Y    ENT11    M12
                                        FH12X    FH12Y    ENT12    M21      FH21X    FH21Y
                                        ENT21    QPRIM    FHTX     FHTY     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ
                                        SIP      M11      FH11X    FH11Y    ENT11    M12
                                        FH12X    FH12Y    ENT12    M21      FH21X    FH21Y
                                        ENT21    QPRIM    FHTX     FHTY     )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )

    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM            581   IN__   ECONTPG  PEPCON1  ECONTPG  PEPCON2
                              OUT__  ECONTPG  PEPCON3
    CARA_GEOM           285   IN__   NGEOMER  PGEOMER
                              OUT__  ECARAGE  PCARAGE
    CHAR_LIMITE         -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_FR2D2D    600   IN__   NFORCER  PFR2D2D  NGEOMER  PGEOMER
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PESA_R    600   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                              OUT__  MVECTUR  PVECTUR
    COOR_ELGA           479   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__    OUT__    XXXXXX   XXXXXX
    EPEQ_ELGA           335   IN__   EDEFOPG  PDEFORR
                              OUT__  EDFEQPG  PDEFOEQ
    EPEQ_ELNO           335   IN__   EDEFONO  PDEFORR
                              OUT__  EDFEQNO  PDEFOEQ
    EPOT_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    EPSI_ELGA           600   IN__   DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                              OUT__  EDEFOPG  PDEFOPG  EDEFOPC  PDEFOPC
    EPSI_ELNO           4     IN__   EDEFOPG  PDEFOPG
                              OUT__  EDEFONO  PDEFONO  EDEFONC  PDEFONC
    FORC_NODA           600   IN__   ECONTPG  PCONTMR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    FULL_MECA_ELAS      600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    INDIC_ENER          -1    IN__    OUT__    XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__    OUT__    XXXXXX   XXXXXX
    INDL_ELGA           30    IN__   CCOMPOR  PCOMPOR  ESIGMPG  PCONTPR  CMATERC  PMATERC  ZVARIPG  PVARIPR
                              OUT__  EGINDLO  PINDLOC
    INIT_VARC           99    IN__  
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER           285   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  EMASSINE PMASSINE
    MASS_MECA           82    IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  MMATUUR  PMATUUR
    M_GAMMA             82    IN__   DDL_MECA PACCELR  NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  MVECTUR  PVECTUR
    NORME_L2            -1    IN__    OUT__    XXXXXX   XXXXXX
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    PAS_COURANT         404   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  ECOURAN  PCOURAN
    RAPH_MECA           600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      600   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  EREFCO   PREFCO
                              OUT__  MVECTUR  PVECTUR
    RICE_TRACEY         -1    IN__    OUT__    XXXXXX   XXXXXX
    RIGI_MECA           -1    IN__    OUT__    XXXXXX   XXXXXX
    RIGI_MECA_ELAS      600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  MMATUNS  PMATUNS
    RIGI_MECA_TANG      600   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR
                              OUT__  MMATUNS  PMATUNS
    SIEF_ELNO           600   IN__   ECONTPG  PCONTRR
                              OUT__  ECONTNO  PSIEFNOR
                                     ECONTNC  PSIEFNOC
    SIEQ_ELGA           335   IN__   ESIGMPG  PCONTRR
                              OUT__  ECOEQPG  PCONTEQ
    SIEQ_ELNO           335   IN__   ESIGMNO  PCONTRR
                              OUT__  ECOEQNO  PCONTEQ
    SIGM_ELGA           546   IN__   ESIGMPG  PSIEFR
                              OUT__  ESIGMPG  PSIGMR   ESIGMPC  PSIGMC
    SIGM_ELNO           4     IN__   ESIGMPG  PCONTRR
                              OUT__  ESIGMNO  PSIEFNOR ESIGMNC  PSIEFNOC
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOU_INI_ELGA        99    IN__  
                              OUT__  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO        99    IN__  
                              OUT__  ENGEOM_R PGEOM_R  ENINST_R PINST_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R
    VAEX_ELGA           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARIPG  PVARIGR
                              OUT__  E1GNEUT  PVARIGS
    VAEX_ELNO           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARINO  PVARINR
                              OUT__  E1NNEUT  PVARINS
    VARI_ELNO           600   IN__   ZVARIPG  PVARIGR  CCOMPOR  PCOMPOR
                              OUT__  ZVARINO  PVARINR
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__    OUT__    XXXXXX   XXXXXX
