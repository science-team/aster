%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_MECPL2_XT
TYPE_GENE__

%  CATALOGUES DES ELEMENTS 2D_CP X-FEM CRACKTIP SANS CONTACT (QUE LINEAIRES ET QUADRATIQUES)

ENTETE__ ELEMENT__ MECPTR3_XT      MAILLE__ TRIA3
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3  XINT=FPG12  NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM36  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  SE2       GAUSS__  RIGI=FPG2  MASS=FPG3
   ATTRIBUT__  XFEM=XT
   ENS_NOEUD__  EN1     =     1   2   3
ENTETE__ ELEMENT__ MECPQU4_XT      MAILLE__ QUAD4
   ELREFE__  QU4       GAUSS__  RIGI=FPG4  MASS=FPG4   NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM72  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3  XINT=FPG12  NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM36
   ELREFE__  SE2       GAUSS__  RIGI=FPG2  MASS=FPG3
   ATTRIBUT__  XFEM=XT
   ENS_NOEUD__  EN1     =     1   2   3   4
ENTETE__ ELEMENT__ MECPTR6_XT      MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=FPG3  MASS=FPG3   NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM72   FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=FPG6  MASS=FPG6  XINT=FPG12
   ATTRIBUT__  XFEM=XT
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6
ENTETE__ ELEMENT__ MECPQU8_XT      MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=FPG9  MASS=FPG9  NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM144  FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR6       GAUSS__  RIGI=FPG3  MASS=FPG6  XINT=FPG12
   ELREFE__  TR3       GAUSS__  RIGI=FPG6  MASS=FPG6 XINT=FPG12
   ATTRIBUT__  XFEM=XT
  ENS_NOEUD__  EN1     =     1   2   3   4   5   6   7   8
MODE_LOCAL__
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]     )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CONTX_R  = XCONTAC  ELEM__         (RHON     MU       RHOTK    INTEG   COECH    COSTCO   COSTFR
                                        COPECO   COPEFR)
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CPRESSF  = PRES_F   ELEM__         (PRES     CISA    )
    CREFERI  = NEUT_I   ELEM__         (X[12]    )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CFREQR   = FREQ_R   ELEM__         (FREQ     )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1    (DX     DY      E1X     E1Y     E2X     E2Y
                                        E3X      E3Y      E4X     E4Y     )
    DDL_MECC = DEPL_R   ELNO__ IDEN__  (DX      DY      )
    DDL_NOZ1 = SIZZ_R   ELNO__ IDEN__  (SIZZ     )
    E2NEUTI  = NEUT_I   ELEM__         (X1       X2      )
    E3NEUTI  = NEUT_I   ELEM__         (X[3]     )
    E6NEUTI  = NEUT_I   ELEM__         (X[6]     )
    E9NEUTI  = NEUT_I   ELEM__         (X[9]     )
    E18NEUI  = NEUT_I   ELEM__         (X[18]    )
    E36NEUI  = NEUT_I   ELEM__         (X[36]    )

    E1NEUTR  = NEUT_R   ELEM__         (X1       )
    E4NEUTR  = NEUT_R   ELEM__         (X[4]     )
    E6NEUTR  = NEUT_R   ELEM__         (X[6]     )
    E8NEUTR  = NEUT_R   ELEM__         (X[8]     )
    E10NEUTR = NEUT_R   ELEM__         (X[10]    )
    E12NEUTR = NEUT_R   ELEM__         (X[12]    )
    E15NEUTR = NEUT_R   ELEM__         (X[15]    )
    E20NEUTR = NEUT_R   ELEM__         (X[20]    )
    E22NEUTR = NEUT_R   ELEM__         (X[22]    )

    EVOISIN  = VOISIN   ELEM__         (V0       V1       V2       V3       V4       V5
                                        V6       T0       T1       T2       T3       T4
                                        T5       T6       )
    EERREUR  = ERRE_R   ELEM__         (ERREST   NUEST    SIGCAL   TERMRE   TERMR2   TERMNO
                                        TERMN2   TERMSA   TERMS2   TAILLE   )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    ECONTPG  = SIEF_R   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTPC  = SIEF_C   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTSE  = NEUT_R   ELEM__         (X[72]    )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     )
    EFORCER  = FORC_R   ELGA__ XFEM    (FX       FY       )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EGGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EGNEUT_F = NEUT_F   ELGA__ XFEM    (X[30]    )
    EGNEUT_R = NEUT_R   ELGA__ XFEM    (X[30]    )
    EGTHETA  = G        ELEM__         (GTHETA   )
    EKTHETA  = G        ELEM__         (GTHETA   FIC1     FIC2     K1       K2       )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     CISA     )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    N2NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       X2       )
    N6NEUT_R = NEUT_R   ELNO__ IDEN__  (X[6]     )
    NDEPLAC  = DEPL_C   ELNO__ IDEN__  (DX       DY       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       )
    STANO_I  = NEUT_I   ELNO__ IDEN__  (X1       )
    XFGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        )
    ZVARIPG  = VARI_R   ELGA__ XFEM    (VARI     )
    FISNO_I  = NEUT_I   ELNO__ IDEN__  (X1       )

VECTEUR__
    MVECTUC = VDEP_C NDEPLAC
    MVECTUR = VDEP_R DDL_MECA
    MVECZZR = VSIZ_R DDL_NOZ1

MATRICE__
    MMATUUC = MDEP_C NDEPLAC NDEPLAC
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATZZR = MSIZ_R DDL_NOZ1 DDL_NOZ1

OPTION__
    REPERE_LOCAL        133   IN__   CCAMASS  PCAMASS  NGEOMER  PGEOMER
                              OUT__  CGEOMER  PREPLO1  CGEOMER  PREPLO2
    CALC_G              288   IN__   DDL_MECA PDEPLAR  CCOMPOR  PCOMPOR  NFORCER  PFRVOLU  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  CPESANR  PPESANR  CROTATR  PROTATR  ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR  DDL_MECC PTHETAR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     EPRESNO  PPRESSR  E6NEUTR  PPINTER  E12NEUTR PBASECO
                                     E15NEUTR PAINTER  E3NEUTI  PCFACE   E3NEUTI  PLONGCO  E22NEUTR PPMILTO
                              OUT__  EGTHETA  PGTHETA
    CALC_G_F            288   IN__   DDL_MECA PDEPLAR  CFORCEF  PFFVOLU  DDL_MECC PTHETAR  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  CPESANR  PPESANR  CROTATR  PROTATR  ZVARCPG  PVARCPR
                                     CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     CPRESSF  PPRESSF  CCOMPOR  PCOMPOR  E12NEUTR PBASECO
                                     E6NEUTR  PPINTER  E15NEUTR PAINTER  E3NEUTI  PCFACE   E3NEUTI  PLONGCO
                                     CTEMPSR  PTEMPSR  E22NEUTR PPMILTO
                              OUT__  EGTHETA  PGTHETA
    CALC_K_G            297   IN__   DDL_MECA PDEPLAR  CCOMPOR  PCOMPOR  NFORCER  PFRVOLU  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  CPESANR  PPESANR  CROTATR  PROTATR  ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR  DDL_MECC PTHETAR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     EPRESNO  PPRESSR  E6NEUTR  PPINTER  E12NEUTR PBASECO
                                     E15NEUTR PAINTER  E3NEUTI  PCFACE   E3NEUTI  PLONGCO  E22NEUTR PPMILTO
                                     CFREQR   PPULPRO
                              OUT__  EKTHETA  PGTHETA
    CALC_K_G_F          297   IN__   DDL_MECA PDEPLAR  CFORCEF  PFFVOLU  DDL_MECC PTHETAR  NGEOMER  PGEOMER
                                     CMATERC  PMATERC  CPESANR  PPESANR  CROTATR  PROTATR  ZVARCPG  PVARCPR
                                     CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     CPRESSF  PPRESSF  CCOMPOR  PCOMPOR  E12NEUTR PBASECO
                                     E6NEUTR  PPINTER  E15NEUTR PAINTER  E3NEUTI  PCFACE   E3NEUTI  PLONGCO
                                     CTEMPSR  PTEMPSR  E22NEUTR PPMILTO  CFREQR   PPULPRO
                              OUT__  EKTHETA  PGTHETA
    CARA_GEOM           -1    IN__   XXXXXX   XXXXXX
                              OUT__  XXXXXX   XXXXXX
    CFL_XFEM            118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PLONCAR
    CHAR_LIMITE         -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_EPSA_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_FF2D2D    440   IN__   CFORCEF  PFF2D2D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR


                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FR2D2D    440   IN__   NFORCER  PFR2D2D  NGEOMER  PGEOMER  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA
                                     N1NEUT_R PLSN     N1NEUT_R PLST     E22NEUTR PPMILTO
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_HYDR_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_META_Z    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_PESA_R    441   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA
                                     N1NEUT_R PLSN     N1NEUT_R PLST     E22NEUTR PPMILTO
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PRES_F    37    IN__   NGEOMER  PGEOMER  CPRESSF  PPRESSF  CTEMPSR  PTEMPSR  E6NEUTR  PPINTER
                                     E15NEUTR PAINTER  E3NEUTI  PCFACE   E3NEUTI  PLONGCO  N1NEUT_R PLST
                                     STANO_I  PSTANO   E12NEUTR PBASECO
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PRES_R    37    IN__   NGEOMER  PGEOMER  EPRESNO  PPRESSR  E6NEUTR  PPINTER  E15NEUTR PAINTER
                                     E3NEUTI  PCFACE   E3NEUTI  PLONGCO  N1NEUT_R PLST     STANO_I  PSTANO
                                     E12NEUTR PBASECO
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PTOT_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_ROTA_R    441   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  CROTATR  PROTATR  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA
                                     N1NEUT_R PLSN     N1NEUT_R PLST     E22NEUTR PPMILTO
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SECH_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_TEMP_R    541   IN__   CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR  E8NEUTR  PPINTTO
                                     E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR
                                     N1NEUT_R PLSN     N1NEUT_R PLST     STANO_I  PSTANO   E22NEUTR PPMILTO
                              OUT__  MVECTUR  PVECTUR  ECONTPG  PCONTRT
    CHVOIS_XFEM         400   IN__   E36NEUI  PCNSETO E9NEUTI  PLONCHA
                              OUT__  E18NEUI  PCVOISX
    COOR_ELGA           479   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__    OUT__    XXXXXX   XXXXXX
    EPOT_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ERME_ELEM           382   IN__   NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  EVOISIN  PVOISIN  CMATERC  PMATERC
                                     CREFERI  PFORCE   CREFERI  PPRESS   CPESANR  PPESANR  CROTATR  PROTATR
                                     EFORCER  PFRVOLU  CFORCEF  PFFVOLU  ECONTNO  PCONTNO  E8NEUTR  PPINTTO
                                     E36NEUI  PCNSETO  E9NEUTI  PLONCHA  E18NEUI  PCVOISX  ECONTSE  PCONTSER
                              OUT__  EERREUR  PERREUR
    FORC_NODA           542   IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI PHEAVTO   E9NEUTI  PLONCHA
                                     N6NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST     STANO_I  PSTANO
                                     E22NEUTR PPMILTO
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  STANO_I  PSTANO
                                     ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     E22NEUTR PPMILTO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR
                                     ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    GRAD_NEUT_R         24    IN__   NGEOMER  PGEOMER  N1NEUT_R PNEUTER
                              OUT__  N2NEUT_R PGNEUTR
    INDIC_ENER          -1    IN__    OUT__    XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__    OUT__    XXXXXX   XXXXXX
    INIT_MAIL_VOIS      99    IN__
                              OUT__  EVOISIN  PVOISIN
    INIT_VARC           99    IN__
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER           285   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  EMASSINE PMASSINE
    MASS_MECA           538   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO
                                     E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST
                              OUT__  MMATUUR  PMATUUR
    MOY_NOEU_S          118   IN__   N1NEUT_R PNEUTR
                              OUT__  E1NEUTR  PMOYEL
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    NORME_L2            -1    IN__    OUT__    XXXXXX   XXXXXX
    RAPH_MECA           539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                     ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO
                                     E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     STANO_I  PSTANO
                                     E22NEUTR PPMILTO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      -1    IN__    OUT__    XXXXXX   XXXXXX
    RICE_TRACEY         -1    IN__    OUT__    XXXXXX   XXXXXX
    RIGI_MECA           539   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO
                                     E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST
                                     E22NEUTR PPMILTO
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_TANG      539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC
                                     ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  STANO_I  PSTANO
                                     ZVARIPG  PVARIMR  E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO
                                     E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST
                                     E22NEUTR PPMILTO
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_GE        536   IN__   NGEOMER  PGEOMER  ECONTPG  PCONTRR  STANO_I  PSTANO
                                     E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO
                                     E9NEUTI  PLONCHA  N6NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST
                                     E22NEUTR PPMILTO
                              OUT__  MMATUUR  PMATUUR
    SIEF_ELGA           261   IN__   CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  E36NEUI  PCNSETO
                                     DDL_MECA PDEPLAR  NGEOMER  PGEOMER  STANO_I  PSTANO
                                     CMATERC  PMATERC  E8NEUTR  PPINTTO  E6NEUTI  PHEAVTO
                                     ZVARCPG  PVARCRR  ZVARCPG  PVARCPR  N6NEUT_R PBASLOR
                                     E9NEUTI  PLONCHA  N1NEUT_R PLSN
                                     N1NEUT_R PLST     E22NEUTR PPMILTO  FISNO_I  PFISNO
                              OUT__  ECONTPG  PCONTRR
    SIEF_ELNO           289   IN__   ECONTPG  PCONTRR  E36NEUI  PCNSETO  E9NEUTI  PLONCHA
                              OUT__  ECONTNO  PSIEFNOR ECONTNC  PSIEFNOC
    SIEQ_ELGA           -1    IN__    OUT__    XXXXXX   XXXXXX
    SIEQ_ELNO           -1    IN__    OUT__    XXXXXX   XXXXXX
    SIGM_ELGA           546   IN__   ECONTPG  PSIEFR
                              OUT__  ECONTPG  PSIGMR   ECONTPC  PSIGMC
    SIGM_ELNO           289   IN__   ECONTPG  PCONTRR  E36NEUI  PCNSETO  E9NEUTI  PLONCHA
                              OUT__  ECONTNO  PSIEFNOR ECONTNC  PSIEFNOC
    SISE_ELNO           289   IN__   ECONTPG  PCONTRR  E9NEUTI  PLONCHA
                              OUT__  ECONTSE  PCONTSER
    TOPOFA              510   IN__   NGEOMER  PGEOMER  N1NEUT_R PLSN     N1NEUT_R PLST     N2NEUT_R PGRADLT
                                     N2NEUT_R PGRADLN
                              OUT__  E6NEUTR  PPINTER  E15NEUTR PAINTER  E3NEUTI  PCFACE   E9NEUTI  PLONCHA
                                     E12NEUTR PBASECO  E6NEUTR  PGESCLA  E6NEUTR  PGMAITR  E6NEUTR  PGESCLO
    TOPOSE              514   IN__   NGEOMER  PGEOMER  N1NEUT_R PLEVSET  N2NEUT_R PGRADLN
                              OUT__  E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA
                                     E15NEUTR PAINTER  E22NEUTR PPMILTO
    TOU_INI_ELGA        99    IN__
                              OUT__  EDOMGGA  PDOMMAG  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__    OUT__    XXXXXX   XXXXXX
    XFEM_SMPLX_INIT     118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PMEAST   N2NEUT_R PNIELNO
    XFEM_SMPLX_CALC     118   IN__   N1NEUT_R PLSNO    N2NEUT_R PGRLS    E1NEUTR  PGRANDF  N2NEUT_R PNIELNO
                              OUT__  E1NEUTR  PDPHI    N1NEUT_R PALPHA
    XFEM_XPG            46    IN__   NGEOMER  PGEOMER  E8NEUTR  PPINTTO  E36NEUI  PCNSETO  E6NEUTI  PHEAVTO
                                     E9NEUTI  PLONCHA  E22NEUTR PPMILTO
                              OUT__  XFGEOM_R PXFGEOM
