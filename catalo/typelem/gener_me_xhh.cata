%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME_XHH
TYPE_GENE__

%  CATALOGUES DES ELEMENTS 3D X-FEM MULTI HEAVISIDE SANS CONTACT

ENTETE__ ELEMENT__ MECA_XH1_HEXA8       MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8    NOEU=NOEU   XFEM=XFEM480 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH1
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ MECA_XH2_HEXA8       MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8    NOEU=NOEU   XFEM=XFEM960  FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH2
   ENS_NOEUD__  EN2     =     1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ MECA_XH3_HEXA8       MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8    NOEU=NOEU   XFEM=XFEM960 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH3
   ENS_NOEUD__  EN3     =     1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ MECA_XH4_HEXA8       MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8    NOEU=NOEU   XFEM=XFEM960 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH4
   ENS_NOEUD__  EN4     =     1   2   3   4   5   6   7   8

ENTETE__ ELEMENT__ MECA_XH1_PENTA6      MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6    NOEU=NOEU   XFEM=XFEM240 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH1
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6
ENTETE__ ELEMENT__ MECA_XH2_PENTA6      MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6    NOEU=NOEU   XFEM=XFEM480 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH2
   ENS_NOEUD__  EN2     =     1   2   3   4   5   6
ENTETE__ ELEMENT__ MECA_XH3_PENTA6      MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6    NOEU=NOEU   XFEM=XFEM480 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH3
   ENS_NOEUD__  EN3     =     1   2   3   4   5   6
ENTETE__ ELEMENT__ MECA_XH4_PENTA6      MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6    NOEU=NOEU   XFEM=XFEM480 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH4
   ENS_NOEUD__  EN4     =     1   2   3   4   5   6

ENTETE__ ELEMENT__ MECA_XH1_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM180 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH1
   ENS_NOEUD__  EN1     =     1   2   3   4   5
ENTETE__ ELEMENT__ MECA_XH2_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM360 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH2
   ENS_NOEUD__  EN2     =     1   2   3   4   5
ENTETE__ ELEMENT__ MECA_XH3_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM360 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH3
   ENS_NOEUD__  EN3     =     1   2   3   4   5
ENTETE__ ELEMENT__ MECA_XH4_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM360 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG5    NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12
   ATTRIBUT__  XFEM=XH4
   ENS_NOEUD__  EN4     =     1   2   3   4   5

ENTETE__ ELEMENT__ MECA_XH1_TETRA4      MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG1    NOEU=NOEU   XINT=FPG5  XFEM=XFEM90 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12
   ATTRIBUT__  XFEM=XH1
   ENS_NOEUD__  EN1     =     1   2   3   4
ENTETE__ ELEMENT__ MECA_XH2_TETRA4      MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG1    NOEU=NOEU   XINT=FPG5  XFEM=XFEM180  FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12
   ATTRIBUT__  XFEM=XH2
   ENS_NOEUD__  EN2     =     1   2   3   4
ENTETE__ ELEMENT__ MECA_XH3_TETRA4      MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG1    NOEU=NOEU   XINT=FPG5  XFEM=XFEM180 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12
   ATTRIBUT__  XFEM=XH3
   ENS_NOEUD__  EN3     =     1   2   3   4
ENTETE__ ELEMENT__ MECA_XH4_TETRA4      MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG1    NOEU=NOEU   XINT=FPG5  XFEM=XFEM180 FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12
   ATTRIBUT__  XFEM=XH4
   ENS_NOEUD__  EN4     =     1   2   3   4


MODE_LOCAL__
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]     )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y
                                        Z        )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     )
    CEPSINR  = EPSI_R   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CONTX_R  = XCONTAC  ELEM__         (RHON     MU       RHOTK    INTEG   COECH    COSTCO   COSTFR
                                        COPECO   COPEFR)
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CPRESSF  = PRES_F   ELEM__         (PRES     )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX   DY   DZ   H1X  H1Y  H1Z  )
                                 EN2   (DX   DY   DZ   H1X  H1Y  H1Z  H2X  H2Y  H2Z  )
                                 EN3   (DX   DY   DZ   H1X  H1Y  H1Z  H2X  H2Y  H2Z  H3X  H3Y  H3Z  )
                                 EN4   (DX   DY   DZ   H1X  H1Y  H1Z  H2X  H2Y  H2Z  H3X  H3Y  H3Z  H4X  H4Y  H4Z  )
    DDL_MECC = DEPL_R   ELNO__ IDEN__  (DX   DY   DZ   )
    E1NEUTR  = NEUT_R   ELEM__         (X1       )
    E1NEUTI  = NEUT_I   ELEM__         (X1       )
    E3NEUTI  = NEUT_I   ELEM__         (X[3]     )
    E10NEUTI = NEUT_I   ELEM__         (X[10]    )
    E15NEUTI = NEUT_I   ELEM__         (X[15]    )
    E18NEUTR = NEUT_R   ELEM__         (X[18]    )
    E22NEUTR = NEUT_R   ELEM__         (X[22]    )
    E30NEUTR = NEUT_R   ELEM__         (X[30]    )
    E198NEUR = NEUT_R   ELEM__         (X[90]    )
    E54NEUTR = NEUT_R   ELEM__         (X[54]    )
    E90NEUTR = NEUT_R   ELEM__         (X[90]    )
    E192NEUI = NEUT_I   ELEM__         (X[90]    )
    E768NEUI = NEUT_I   ELEM__         (X[360]   )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPG  = SIEF_R   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPC  = SIEF_C   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EFACY_R  = FACY_R   ELGA__ RIGI    (DTAUM1   VNM1X    VNM1Y    VNM1Z    SINMAX1  SINMOY1
                                        EPNMAX1  EPNMOY1  SIGEQ1   NBRUP1   ENDO1    DTAUM2
                                        VNM2X    VNM2Y    VNM2Z    SINMAX2  SINMOY2  EPNMAX2
                                        EPNMOY2  SIGEQ2   NBRUP2   ENDO2    )
    EGGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        Z        )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EGNEUT_F = NEUT_F   ELGA__ XFEM    (X[30]    )
    EGNEUT_R = NEUT_R   ELGA__ XFEM    (X[30]    )
    EKTHETA  = G        ELEM__         (GTHETA   FIC1      FIC2      FIC3
                                        K1       K2        K3        BETA   )
    EGTHETA  = G        ELEM__         (GTHETA   )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     )
    G27NEUTR = NEUT_R   ELGA__ RIGI    (X[27]    )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    N3NEUT_R = NEUT_R   ELNO__ IDEN__  (X[3]     )
    N9NEUT_R = NEUT_R   ELNO__ IDEN__  (X[9]     )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       FZ       )
    EFORCER  = FORC_R   ELGA__ XFEM    (FX       FY       FZ       )
    STANO_I  = NEUT_I   ELNO__ IDEN__  (X1       )
    XFGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        Z        )
    ZVARIPG  = VARI_R   ELGA__ XFEM    (VARI     )
    FISNO_I  = NEUT_I   ELNO__ IDEN__  (X1       )
    FISCO_I  = NEUT_I   ELEM__         (X[2]     )
    CFREQR   = FREQ_R   ELEM__         (FREQ     )

VECTEUR__

    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA

OPTION__
    REPERE_LOCAL        133   IN__   CCAMASS  PCAMASS  NGEOMER  PGEOMER
                              OUT__  CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    CALC_G              288   IN__   NGEOMER  PGEOMER  DDL_MECA PDEPLAR  DDL_MECC PTHETAR  CMATERC  PMATERC
                                     CCOMPOR  PCOMPOR  E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  EPRESNO  PPRESSR
                                     E18NEUTR PPINTER  E30NEUTR PAINTER  E15NEUTI PCFACE   E3NEUTI  PLONGCO
                                     E54NEUTR PBASECO  NFORCER  PFRVOLU  CPESANR  PPESANR  CROTATR  PROTATR
                              OUT__  EGTHETA  PGTHETA
    CALC_G_F            288   IN__   NGEOMER  PGEOMER  DDL_MECA PDEPLAR  DDL_MECC PTHETAR  CMATERC  PMATERC
                                     CCOMPOR  PCOMPOR  G27NEUTR PCOURB   E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  CPRESSF  PPRESSF
                                     E18NEUTR PPINTER  E30NEUTR PAINTER  E15NEUTI PCFACE   E3NEUTI  PLONGCO
                                     CTEMPSR  PTEMPSR  E54NEUTR PBASECO  CFORCEF  PFFVOLU  CPESANR  PPESANR
                                     CROTATR  PROTATR
                              OUT__  EGTHETA  PGTHETA
    CALC_K_G            297   IN__   NGEOMER  PGEOMER  DDL_MECA PDEPLAR  DDL_MECC PTHETAR  CMATERC  PMATERC
                                     CCOMPOR  PCOMPOR  G27NEUTR PCOURB   E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  EPRESNO  PPRESSR
                                     E18NEUTR PPINTER  E30NEUTR PAINTER  E15NEUTI PCFACE   E3NEUTI  PLONGCO
                                     NFORCER  PFRVOLU  CPESANR  PPESANR  CROTATR  PROTATR  E54NEUTR PBASECO
                                     CFREQR   PPULPRO
                              OUT__  EKTHETA  PGTHETA
    CALC_K_G_F          297   IN__   NGEOMER  PGEOMER  DDL_MECA PDEPLAR  DDL_MECC PTHETAR  CMATERC  PMATERC
                                     CCOMPOR  PCOMPOR  G27NEUTR PCOURB   E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  CPRESSF  PPRESSF
                                     E18NEUTR PPINTER  E30NEUTR PAINTER  E15NEUTI PCFACE   E3NEUTI  PLONGCO
                                     CTEMPSR  PTEMPSR  CFORCEF  PFFVOLU  CPESANR  PPESANR  CROTATR  PROTATR
                                     E54NEUTR PBASECO  CFREQR   PPULPRO
                              OUT__  EKTHETA  PGTHETA
    CARA_GEOM           -1    IN__    OUT__    XXXXXX   XXXXXX
    CFL_XFEM            118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PLONCAR
    CHAR_LIMITE         -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_EPSA_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_HYDR_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_META_Z    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_PESA_R    441   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  STANO_I  PSTANO
                                     N1NEUT_R PLSN     N1NEUT_R PLST     E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI  PLONCHA
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PTOT_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_ROTA_R    441   IN__   DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     CROTATR  PROTATR  STANO_I  PSTANO   N1NEUT_R PLSN     N1NEUT_R PLST
                                     E198NEUR PPINTTO  E768NEUI PCNSETO  E192NEUI PHEAVTO  E10NEUTI  PLONCHA
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SECH_R    -1    IN__    OUT__    XXXXXX   XXXXXX
    CHAR_MECA_TEMP_R    541   IN__   CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  CMATERC  PMATERC
                                     ZVARCPG  PVARCPR  CTEMPSR  PTEMPSR  ZVARCPG  PVARCRR  E198NEUR PPINTTO
                                     E768NEUI PCNSETO  E192NEUI PHEAVTO  E10NEUTI PLONCHA  N9NEUT_R PBASLOR
                                     N1NEUT_R PLSN     N1NEUT_R PLST     STANO_I  PSTANO   FISNO_I  PFISNO
                              OUT__  MVECTUR  PVECTUR  ECONTPG  PCONTRT
    COOR_ELGA           488   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__    OUT__    XXXXXX   XXXXXX
    EPOT_ELEM           -1    IN__    OUT__    XXXXXX   XXXXXX
    FORC_NODA           542   IN__   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER
                                     E198NEUR PPINTTO  E768NEUI PCNSETO  E192NEUI PHEAVTO  E10NEUTI  PLONCHA
                                     N9NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST     STANO_I  PSTANO
                                     FISNO_I  PFISNO
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR
                                     ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI  PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     STANO_I  PSTANO   FISNO_I  PFISNO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    GRAD_NEUT_R         24    IN__   NGEOMER  PGEOMER  N1NEUT_R PNEUTER
                              OUT__  N3NEUT_R PGNEUTR
    GRAD_NEUT9_R        398   IN__   NGEOMER  PGEOMER  N9NEUT_R PNEUTER
                              OUT__  G27NEUTR PGNEUTR
    INDIC_ENER          -1    IN__    OUT__    XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__    OUT__    XXXXXX   XXXXXX
    INIT_VARC           99    IN__
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER           -1    IN__    OUT__    XXXXXX   XXXXXX
    MOY_NOEU_S          118   IN__   N1NEUT_R PNEUTR
                              OUT__  E1NEUTR  PMOYEL
    NORME_L2            -1    IN__    OUT__    XXXXXX   XXXXXX
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    RAPH_MECA           539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR
                                     ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  E198NEUR PPINTTO  E768NEUI PCNSETO
                                     E192NEUI PHEAVTO  E10NEUTI  PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN
                                     N1NEUT_R PLST     STANO_I  PSTANO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      -1    IN__    OUT__    XXXXXX   XXXXXX
    RICE_TRACEY         -1    IN__    OUT__    XXXXXX   XXXXXX
    RIGI_MECA           539   IN__   NGEOMER  PGEOMER  CMATERC   PMATERC  STANO_I   PSTANO
                                     E198NEUR PPINTTO  E768NEUI  PCNSETO  E192NEUI  PHEAVTO
                                     E10NEUTI  PLONCHA  N9NEUT_R  PBASLOR  N1NEUT_R  PLSN     N1NEUT_R PLST
                                     FISNO_I  PFISNO
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_TANG      539   IN__   CCAMASS  PCAMASS  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                     DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR
                                     CTEMPSR  PINSTPR  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARCPG  PVARCRR  STANO_I  PSTANO
                                     ZVARIPG  PVARIMR  E198NEUR PPINTTO  E768NEUI PCNSETO  E192NEUI PHEAVTO
                                     E10NEUTI  PLONCHA  N9NEUT_R PBASLOR  N1NEUT_R PLSN     N1NEUT_R PLST
                                     FISNO_I  PFISNO
                              OUT__  MMATUUR  PMATUUR
    SIEF_ELGA           261   IN__   CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  E768NEUI  PCNSETO
                                     DDL_MECA PDEPLAR  NGEOMER  PGEOMER  STANO_I  PSTANO
                                     CMATERC  PMATERC  E198NEUR PPINTTO  E192NEUI  PHEAVTO
                                     ZVARCPG  PVARCRR  ZVARCPG  PVARCPR  N9NEUT_R PBASLOR
                                     E10NEUTI  PLONCHA  N1NEUT_R PLSN
                                     N1NEUT_R PLST     E22NEUTR PPMILTO  FISNO_I  PFISNO
                              OUT__  ECONTPG  PCONTRR
    SIEQ_ELGA           -1    IN__    OUT__    XXXXXX   XXXXXX
    SIEQ_ELNO           -1    IN__    OUT__    XXXXXX   XXXXXX
    SIGM_ELGA           546   IN__   ECONTPG  PSIEFR
                              OUT__  ECONTPG  PSIGMR   ECONTPC  PSIGMC
    SIGM_ELNO           -1    IN__    OUT__    XXXXXX   XXXXXX
    TOPOFA              510   IN__   NGEOMER  PGEOMER  N1NEUT_R PLSN     N1NEUT_R PLST     N3NEUT_R PGRADLT
                                     N3NEUT_R PGRADLN  N3NEUT_R PGRADLT
                              OUT__  E18NEUTR PPINTER  E30NEUTR PAINTER  E15NEUTI PCFACE   E3NEUTI  PLONCHA
                                     E54NEUTR PBASECO  E18NEUTR PGESCLA  E18NEUTR PGMAITR  E18NEUTR PGESCLO
    TOPOSE              514   IN__   NGEOMER  PGEOMER  N1NEUT_R PLEVSET  FISCO_I PFISCO
                              OUT__  E198NEUR PPINTTO  E768NEUI PCNSETO  E192NEUI PHEAVTO  E10NEUTI  PLONCHA
                                     E30NEUTR PAINTER
    TOU_INI_ELGA        99    IN__
                              OUT__  EDOMGGA  PDOMMAG  EFACY_R  PFACY_R  EGGEOM_R PGEOM_R  EGINST_R PINST_R
                                     EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__    OUT__    XXXXXX   XXXXXX
    XFEM_SMPLX_INIT     118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PMEAST   N3NEUT_R PNIELNO
    XFEM_SMPLX_CALC     118   IN__   N1NEUT_R PLSNO    N3NEUT_R PGRLS    E1NEUTR  PGRANDF  N3NEUT_R PNIELNO
                              OUT__  E1NEUTR  PDPHI    N1NEUT_R PALPHA
    XFEM_XPG            46    IN__   NGEOMER  PGEOMER  E198NEUR PPINTTO  E768NEUI PCNSETO  E192NEUI PHEAVTO
                                     E10NEUTI PLONCHA
                              OUT__  XFGEOM_R PXFGEOM
