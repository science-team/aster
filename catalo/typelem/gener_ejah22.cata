%& LIBRARY TYPELEM
%            CONFIGURATION MANAGEMENT OF EDF VERSION
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_EJAH22
TYPE_GENE__

ENTETE__ ELEMENT__ HM_J_AXQ8S         MAILLE__ QUAD8
   ELREFE__  SE3    GAUSS__   RIGI=FPG3NOS   MASS=FPG3  NOEU_S=NOEU_S FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  SE2    GAUSS__   RIGI=FPG3NOS   MASS=FPG3  NOEU_S=NOEU_S
   ENS_NOEUD__  EN1     =     1  2
   ENS_NOEUD__  EN2     =     3  4
   ENS_NOEUD__  EN3     =     6  8
   ENS_NOEUD__  EN4     =     5  7

MODE_LOCAL__

    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y        Z)
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL  TSAMPL   TSRETOUR )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT[9]   NVI_C    NVI_T    NVI_H    NVI_M    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       PRE1     )
                                 EN2   (DX       DY       PRE1     )
                                 EN3   (PRE1     )
                                 EN4   (DX       DY       LH1      )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    ( )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    ( )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  ( )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  ( )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       )
    EFORCER  = FORC_R   ELGA__ RIGI    (FX       FY       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGINDLO  = INDL_R   ELGA__ RIGI    (INDICE   DIR[4]   )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CPDTTH_R = INST_R   ELEM__         (INST     DELTAT   THETA    )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    ECARAGE  = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      IXR2     IYR2     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    E1NEUTK  = NEUT_K24 ELEM__         (Z1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    CREFERI  = NEUT_I   ELEM__         (X[12]    )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    E1GNEUT  = NEUT_R   ELGA__ RIGI    (X1       )
    E1NNEUT  = NEUT_R   ELNO__ IDEN__  (X1       )
    E2NEUTR  = NEUT_R   ELEM__         (X1       X2       )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    EREFCO   = PREC     ELEM__         (SIGM     FHYDR1   FHYDR2   FTHERM   )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIGN     SITX     SIP      M11      FH11X
                                        LH1P     LH1M     DPRE1P   DPRE1M   )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIGN     SITX     SIP      M11      FH11X
                                        LH1P     LH1M     DPRE1P   DPRE1M   )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG   TRSIG    )
    ECONTNC  = SIEF_C   ELNO__ DIFF__
                                 EN1   (         )
                                 EN2   (         )
                                 EN3   (SIGN     SITX     SIP      M11      FH11X
                                        LH1P     LH1M     DPRE1P   DPRE1M   )
                                 EN4   (         )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    REPERE_LOCAL        133   IN__   CCAMASS  PCAMASS  NGEOMER  PGEOMER
                              OUT__  CGEOMER  PREPLO1  CGEOMER  PREPLO2
    ADD_SIGM            -1    IN__     OUT__  XXXXXX   XXXXXX
    CARA_GEOM           285   IN__   NGEOMER  PGEOMER
                              OUT__  ECARAGE  PCARAGE
    CHAR_LIMITE         -1    IN__     OUT__  XXXXXX   XXXXXX
    CHAR_MECA_FR2D2D    -1    IN__     OUT__  XXXXXX   XXXXXX
    COOR_ELGA           362   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENEL_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    ENER_TOTALE         -1    IN__     OUT__  XXXXXX   XXXXXX
    EPOT_ELEM           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPEQ_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPEQ_ELNO           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPSI_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    EPSI_ELNO           -1    IN__     OUT__  XXXXXX   XXXXXX
    FORC_NODA           313   IN__   ECONTPG  PCONTMR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  CCAMASS  PCAMASS
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA           313   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  CCAMASS  PCAMASS
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    FULL_MECA_ELAS      313   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  CCAMASS  PCAMASS
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    INDIC_ENER          -1    IN__     OUT__  XXXXXX   XXXXXX
    INDIC_SEUIL         -1    IN__     OUT__  XXXXXX   XXXXXX
    INDL_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    INIT_VARC           -1    IN__     OUT__  XXXXXX   XXXXXX
    MASS_INER           285   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  EMASSINE PMASSINE
    MASS_MECA           -1    IN__     OUT__  XXXXXX   XXXXXX
    M_GAMMA             -1    IN__     OUT__  XXXXXX   XXXXXX
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    NORME_L2            -1    IN__     OUT__  XXXXXX   XXXXXX
    PAS_COURANT         404   IN__   NGEOMER  PGEOMER  CMATERC  PMATERC
                              OUT__  ECOURAN  PCOURAN
    RAPH_MECA           313   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR   ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  CCAMASS  PCAMASS
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      -1    IN__     OUT__  XXXXXX   XXXXXX
    RICE_TRACEY         -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA           -1    IN__     OUT__  XXXXXX   XXXXXX
    RIGI_MECA_ELAS      313   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  CCAMASS  PCAMASS
                              OUT__  MMATUNS  PMATUNS
    RIGI_MECA_TANG      313   IN__   CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                     DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                     CMATERC  PMATERC  ZVARCPG  PVARCRR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                                     ZVARIPG  PVARIMR  CCAMASS  PCAMASS
                              OUT__  MMATUNS  PMATUNS
    SIEF_ELNO           313   IN__   ECONTPG  PCONTRR
                              OUT__  ECONTNO  PSIEFNOR
                                     ECONTNC  PSIEFNOC
    SIEQ_ELGA           -1    IN__     OUT__  XXXXXX   XXXXXX
    SIEQ_ELNO           -1    IN__     OUT__  XXXXXX   XXXXXX
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOU_INI_ELGA        99    IN__
                              OUT__  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO        99    IN__
                              OUT__  ENGEOM_R PGEOM_R  ENINST_R PINST_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R
    VAEX_ELGA           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARIPG  PVARIGR
                              OUT__  E1GNEUT  PVARIGS
    VAEX_ELNO           549   IN__   CCOMPOR  PCOMPOR  E1NEUTK  PNOVARI  ZVARINO  PVARINR
                              OUT__  E1NNEUT  PVARINS
    VARI_ELNO           313   IN__   ZVARIPG  PVARIGR  CCOMPOR  PCOMPOR
                              OUT__  ZVARINO  PVARINR
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL             -1    IN__     OUT__  XXXXXX   XXXXXX
