# TITRE INTERFACE EN CONTACT FROTTANT AVEC X-FEM
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#       CAS_TEST SSNV209A: 2D, ELEMENTS Q4, METHODE CONTINUE

DEBUT(CODE=_F(NOM='SSNV209A',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

#......................................................
#  CALCUL TEST ELEMENTS FINIS DE CONTACT 2D QUAD4
#  ECRASEMENT D UN LOPIN FISSURE AVEC FROTTEMENT
#......................................................

MA=LIRE_MAILLAGE(FORMAT='MED',VERI_MAIL=_F(VERIF='OUI'),)

MA=DEFI_GROUP(reuse =MA,
                    MAILLAGE=MA,
                    CREA_GROUP_NO=(_F(GROUP_MA='LCONTA',
                                      OPTION='NOEUD_ORDO',
                                      NOM='RELEVE',
                                      GROUP_NO_ORIG='PPA',
                                      GROUP_NO_EXTR='PPS',),
                                   _F(NOM='NFISSU',
                                      OPTION='PLAN',
                                      POINT=(0.,0.),
                                      VECT_NORMALE=(0.,1.),
                                      PRECISION=1.E-6,),),);

MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=_F(TOUT = 'OUI',
                       PHENOMENE = 'MECANIQUE',
                       MODELISATION = 'D_PLAN',)
              )

MA=MODI_MAILLAGE(reuse=MA,
                 MAILLAGE=MA,
                 ORIE_PEAU_2D=(_F(GROUP_MA = 'LPRESV',),
                               _F(GROUP_MA = 'LPRESH',),
                               _F(GROUP_MA = 'LBLOCX',),
                               _F(GROUP_MA = 'LCONTA',),
                               _F(GROUP_MA = 'LBATI',),
                               )
                )

MATPLA=DEFI_MATERIAU(ELAS=_F(E = 1.3E11,
                             NU = 0.2,)
                    )
MATBAT=DEFI_MATERIAU(ELAS=_F(E = 1.3E16,
                             NU = 0.0,)
                    )
CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=(_F(GROUP_MA ='SPLAQ',
                             MATER = MATPLA,),
                        _F(GROUP_MA ='SBATI',
                             MATER = MATBAT,),),
                    )
CHA1=AFFE_CHAR_MECA(MODELE=MO,
                    DDL_IMPO=(_F(GROUP_MA = 'LENCAV',  DX = 0.0, DY = 0.0,),
                                    ),
                    PRES_REP=(_F(GROUP_MA = 'LPRESV', PRES = 5.E07,),
                              )
                   )

def pr(y) :
   if y< 1.e-15 : return 0.E07
   if y> 1.e-15 : return  15.E07
   if y==1.e-15 : return  0.
FCTPRES = FORMULE(VALE='pr(Y)',
                  NOM_PARA=('X','Y'),);

CHAF=AFFE_CHAR_MECA_F(MODELE=MO,
                     PRES_REP=_F(GROUP_MA = ('LPRESH','LBLOCX',),
                              PRES=FCTPRES,),);

# ==================================================

CHA2 = DEFI_CONTACT(MODELE         = MO,
                    FORMULATION    = 'CONTINUE',
                    FROTTEMENT     = 'COULOMB',
                    ALGO_RESO_CONT = 'POINT_FIXE',
                    ALGO_RESO_GEOM = 'POINT_FIXE',
                    ALGO_RESO_FROT = 'NEWTON',                    
                    ITER_GEOM_MAXI = 3,
                    ITER_CONT_MAXI = 4,
                    ZONE =(
                           _F(
                             GROUP_MA_MAIT = 'LBATI',
                             GROUP_MA_ESCL = 'LCONTA',
                             APPARIEMENT   = 'MAIT_ESCL',
                             CONTACT_INIT  = 'INTERPENETRE',
                             SEUIL_INIT    = 1.,
                             COULOMB       = 1.0,
                             ),
                          ),
                   );


RAMPE=DEFI_FONCTION(NOM_PARA='INST',
                    PROL_GAUCHE='LINEAIRE',
                    PROL_DROITE='LINEAIRE',
                    VALE=(0.0,0.0,1.0,1.0,)
                    )

L_INST=DEFI_LIST_REEL(DEBUT=0.,
                      INTERVALLE=_F(JUSQU_A = 1.0, NOMBRE = 1,)
                      )

#-----------------------------------------------------------
#                   RESOLUTION
#-----------------------------------------------------------

U2M=STAT_NON_LINE(MODELE    = MO,
                  CHAM_MATER = CHMAT,
                  EXCIT      =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               _F(CHARGE = CHAF,
                                  FONC_MULT = RAMPE,),
                               ),
                  CONTACT    = CHA2,
                  COMP_ELAS  = _F(RELATION = 'ELAS',),
                  NEWTON=_F(REAC_ITER=1),
                  INCREMENT  = _F(LIST_INST = L_INST,),
                  CONVERGENCE= _F(ARRET = 'OUI',
                                 ITER_GLOB_MAXI = 50,
                                 RESI_GLOB_RELA = 1.0E-6,),
                  SOLVEUR=_F(METHODE='MUMPS',
                         ),
               )

#----------------------------------------------
#               TESTS NON- REGRESSION
#----------------------------------------------

TEST_RESU(RESU=(_F(GROUP_NO='PPR',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='VALE_CONT',
                   NOM_CMP='JEU',
                   VALE_CALC= 0.000000000000E+00,
                   VALE_REFE=0.0,
                   TOLE_MACHINE=9.9999999999999998E-13,
                   CRITERE='ABSOLU',
                   PRECISION=9.9999999999999998E-13,
                   ),
                _F(GROUP_NO='PPR',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='VALE_CONT',
                   NOM_CMP='CONT',
                   VALE_CALC=1.000000000,
                   VALE_REFE=1.0,
                   CRITERE='RELATIF',
                   PRECISION=1.0000000000000001E-05,
                   ),
                _F(GROUP_NO='PPR',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='VALE_CONT',
                   NOM_CMP='RN',
                   VALE_CALC= 1.04863317E+05,
                   VALE_REFE=1.048637688477E5,
                   CRITERE='RELATIF',
                   PRECISION=1.0000000000000001E-05,
                   ),
                _F(GROUP_NO='PPA',
                   INST=1.0,
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=2.8459459150097002E-05,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=2.7079257212128999E-05,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=2.2740276195227001E-05,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=1.9727074680514999E-05,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=1.5364146641690999E-05,
                   TOLE_MACHINE=1.E-3,
                   ),
                ),
          )

FIN()
