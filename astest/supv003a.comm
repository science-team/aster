# TITRE TEST DE LA METHODE D'INTERROGATION DES OPTIONS get_option_dependency
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
# person_in_charge: mathieu.courtois at edf.fr

DEBUT(CODE=_F(NOM='SUPV003A', NIV_PUB_WEB='INTERNET'),
      PAR_LOT='NON', IGNORE_ALARM='SUPERVIS_1',
      LANG='EN')

import aster
lopt = aster.get_option_dependency('SIGM_ELGA')
assert len(lopt) == 3, lopt
assert lopt == ('DEPL', 'SIEF_ELGA', 'SIGM_ELGA'), lopt

lopt = aster.get_option_dependency('ACCE')
assert len(lopt) == 0, lopt

lopt = aster.get_option_dependency('SING_ELEM')
assert len(lopt) == 0, lopt

lopt = aster.get_option_dependency('SING_NOEU')
assert len(lopt) == 2, lopt
assert lopt == ('SING_ELNO', 'SING_NOEU'), lopt

tab = CREA_TABLE(LISTE=(_F(PARA='NBVAL',
                           LISTE_I=len(lopt),),),)
TEST_TABLE(CRITERE='ABSOLU',
           REFERENCE='ANALYTIQUE',
           VALE_CALC_I=2,
           VALE_REFE_I=2,
           TOLE_MACHINE=0.0,
           PRECISION=0,
           NOM_PARA='NBVAL',
           TABLE=tab,)

help(aster.get_option_dependency)

# test que le dictionnaire anglais est disponible
#: Noyau/N_ASSD.py:225
translation = _("On attend un objet concept.")
test = translation == "A concept object is expected."

tab2 = CREA_TABLE(LISTE=(_F(PARA='BOOL',
                            LISTE_I=int(test),),),)
TEST_TABLE(CRITERE='ABSOLU',
           REFERENCE='ANALYTIQUE',
           VALE_CALC_I=1,
           VALE_REFE_I=1,
           TOLE_MACHINE=0.0,
           PRECISION=0,
           NOM_PARA='BOOL',
           TABLE=tab2,)

FIN()
