# person_in_charge: mickael.abbas at edf.fr
# AUTEUR : M. ZARROUG
# DATE   : 18/12/2000
# TITRE CONTACT DE DEUX SPHERES
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS_TEST__:SSNV104E
# MODELISATION 2D AXI ELEMENTS LINEAIRES : TRIA3 ET QUAD4
#
######

DEBUT(CODE=_F(NOM='SSNV104E',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(FORMAT='MED',);

MA=DEFI_GROUP(reuse =MA,
              MAILLAGE=MA,
              CREA_GROUP_NO=(_F(GROUP_MA='CON1',),
                             _F(GROUP_MA='CON2')));

MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=(_F(TOUT='OUI',
                        PHENOMENE='MECANIQUE',
                        MODELISATION='AXIS')));

MAT=DEFI_MATERIAU(ELAS=_F(E=2.E4,
                          NU=0.29999999999999999,
                          ALPHA=1.E-2));

CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=_F(TOUT='OUI',
                            MATER=MAT));

CHA1 = DEFI_CONTACT(MODELE         = MO,
                    FORMULATION    = 'CONTINUE',
                    ALGO_RESO_CONT = 'POINT_FIXE',
                    ALGO_RESO_GEOM = 'POINT_FIXE',
                    ITER_CONT_MAXI = 20,
                    ZONE =(
                           _F(
                             GROUP_MA_ESCL = 'CON1',
                             GROUP_MA_MAIT = 'CON2',
                             ALGO_CONT     = 'STANDARD',
                             GLISSIERE     = 'OUI',
                             COEF_CONT     = 10000.,
                             ),
                          ),
                   );

CHA2=AFFE_CHAR_MECA(MODELE=MO,
                    DDL_IMPO=(_F(GROUP_NO='SPHSUP',
                                 DY=-2.0,),
                              _F(GROUP_NO='SPHINF',
                                 DY=2.0,),
                              _F(GROUP_NO='AXE',
                                 DX=0.0)),
                    INFO=1);

RAMPE=DEFI_FONCTION(NOM_PARA='INST',
                    VALE=(0.0,0.0,1.0,1.0,2.,0.),
                    PROL_DROITE='LINEAIRE',
                    PROL_GAUCHE='LINEAIRE',);

L_INST=DEFI_LIST_REEL(DEBUT=0.0,
                      INTERVALLE=(_F(JUSQU_A=1.0,
                                     NOMBRE=10),
                                  _F(JUSQU_A=2.0,
                                     NOMBRE=10)));

RESU0=STAT_NON_LINE(MODELE=MO,
                   CHAM_MATER=CHMAT,
                   EXCIT=(
                          _F(CHARGE=CHA2,
                             FONC_MULT=RAMPE)),
                   CONTACT  = CHA1,
                   COMP_ELAS=_F(RELATION='ELAS',),
                   INCREMENT=_F(LIST_INST=L_INST,),

                   AFFICHAGE=(_F(INFO_RESIDU='OUI',
                                 INFO_TEMPS='OUI',
                                 UNITE=37,),),
                   NEWTON=_F(MATRICE='TANGENTE',
                             REAC_ITER=1),
                   CONVERGENCE=_F(RESI_GLOB_MAXI=1.));

RESU=STAT_NON_LINE(MODELE=MO,
                   CHAM_MATER=CHMAT,
                   EXCIT=(
                          _F(CHARGE=CHA2,
                             FONC_MULT=RAMPE)),
                   CONTACT  = CHA1,
                   COMP_ELAS=_F(RELATION='ELAS',),
                   INCREMENT=_F(LIST_INST=L_INST,),
                   OBSERVATION=(_F(NOM_CHAM='DEPL',
                                   NOM_CMP='DY',
                                   PAS_OBSE=1,
                                   NOEUD='N81',),
                                _F(NOM_CHAM='SIEF_ELGA',
                                   POINT = 1,
                                   NOM_CMP='SIYY',
                                   MAILLE='M1',),),

                   SUIVI_DDL=(_F(
                                   NOM_CHAM   = 'FORC_NODA',
                                   NOM_CMP    = 'DX',
                                   TOUT = 'OUI',
                                   EVAL_CHAM='MAX',),
                                _F(
                                   NOM_CHAM   = 'SIEF_ELGA',
                                   NOM_CMP    = 'SIXX',
                                   MAILLE     = ('M1','M2'),
                                   POINT      = 1,),
                                _F(
                                   NOM_CHAM   = 'SIEF_ELGA',
                                   NOM_CMP='SIXX',
                                   TOUT = 'OUI',
                                   EVAL_CHAM='MAX',
                                   EVAL_ELGA='MAX',
                                   )),
                   AFFICHAGE=(_F(INFO_RESIDU='NON',
                                 UNITE=37,),),
                   NEWTON=_F(MATRICE='TANGENTE',
                             REAC_ITER=1),
                   CONVERGENCE=_F(RESI_GLOB_MAXI=1.,
                                  ITER_GLOB_MAXI=30 ));



RESU=CALC_CHAMP(reuse=RESU,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=RESU)


TAB_OBS = RECU_TABLE (CO = RESU,
                      NOM_TABLE = 'OBSERVATION');

IMPR_TABLE(TABLE=TAB_OBS);

SIELGA=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                  OPERATION='EXTR',
                  RESULTAT=RESU,
                  NOM_CHAM='SIEF_ELGA',
                  NUME_ORDRE=10,);

SIELNO=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                  OPERATION='EXTR',
                  RESULTAT=RESU,
                  NOM_CHAM='SIGM_ELNO',
                  NUME_ORDRE=10,);

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       PRECISION=0.070000000000000007,
                       MAILLE='M31',
                       CHAM_GD=SIELNO,
                       VALE_CALC=-2.97129287E+03,
                       VALE_REFE=-2798.3000000000002,
                       REFERENCE='NON_DEFINI',),
          )

# TEST DE LA CONFORMITE PAR RAPPORT AUX RESULTATS ASTER
# PRECEDENTS

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       PRECISION=1.E-2,
                       MAILLE='M31',
                       CHAM_GD=SIELNO,
                       VALE_CALC=-2.97129287E+03,
                       VALE_REFE=-2971.3699999999999,
                       REFERENCE='NON_DEFINI',),
          RESU=(_F(NUME_ORDRE=10,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N291',
                   NOM_CMP='DX',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DX',
                   VALE_CALC=-0.112429686,
                   VALE_REFE=-0.110211,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.029999999999999999,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DY',
                   VALE_CALC=-0.161447638,
                   VALE_REFE=-0.162911,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DX',
                   VALE_CALC=-0.171068850,
                   VALE_REFE=-0.16594600000000001,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.040000000000000001,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DY',
                   VALE_CALC=-0.627911085,
                   VALE_REFE=-0.62966599999999995,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=5.0000000000000001E-3,),
                ),
          )

# Impression au format GMSH
# IMPR_RESU(FORMAT='GMSH',
#           UNITE=37,
#           RESU=_F(MAILLAGE=MA,
#                   RESULTAT=RESU,
#                   NOM_CHAM=('DEPL','VALE_CONT')));

# TEST DE LA FONCTIONNALITE GLISSIERE:
# On teste que les noeuds restent bien colles apres contact
# Noeuds concernes : N72, N80, N81

TAB_CONT=POST_RELEVE_T(ACTION=_F(INTITULE='CONTACT',
                                 GROUP_NO='CON1',
                                 RESULTAT=RESU,
                                 NOM_CHAM='VALE_CONT',
                                 LIST_INST=L_INST,
                                 TOUT_CMP='OUI',
                                 OPERATION='EXTRACTION'));

IMPR_TABLE(TABLE=TAB_CONT);

# -- A T=2sec
TEST_RESU(RESU=_F(NUME_ORDRE=2,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='CONT',
                  VALE_CALC= 0.00000000E+00,
                  VALE_REFE=0,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=2,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='CONT',
                  VALE_CALC= 0.00000000E+00,
                  VALE_REFE=0,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=2,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='CONT',
                  VALE_CALC= 0.00000000E+00,
                  VALE_REFE=0,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

# -- A T=5sec
TEST_RESU(RESU=_F(NUME_ORDRE=5,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='CONT',
                  VALE_CALC= 0.00000000E+00,
                  VALE_REFE=0,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=5,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='CONT',
                  VALE_CALC= 0.00000000E+00,
                  VALE_REFE=0,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=5,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

# -- A T=10sec
TEST_RESU(RESU=_F(NUME_ORDRE=10,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=10,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=10,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

# -- A T=15sec
TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='JEU',
                  VALE_CALC=-7.46040217E-19,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=1.E-08,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='JEU',
                  VALE_CALC= 1.24127253E-17,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=1.E-08,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=15,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='JEU',
                  VALE_CALC=-2.09333120E-17,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=1.E-08,),
          )

# -- A T=20sec
TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='CONT',
                  VALE_CALC=2.000000000,
                  VALE_REFE=2,
                  CRITERE='ABSOLU',
                  PRECISION=0.10000000000000001,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N72',
                  NOM_CMP='JEU',
                  VALE_CALC= 3.12613103E-21,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=4.9999999999999998E-08,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N80',
                  NOM_CMP='JEU',
                  VALE_CALC=-4.07663837E-22,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=1.E-08,),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=20,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RESU,
                  NOM_CHAM='VALE_CONT',
                  NOEUD='N81',
                  NOM_CMP='JEU',
                  VALE_CALC=-2.28352073E-22,
                  VALE_REFE=0.0,
                  CRITERE='ABSOLU',
                  PRECISION=1.E-08,),
          )

FIN();
