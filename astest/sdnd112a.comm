# TITRE TEST EFFET LAME FLUIDE ENTRE DEUX STRUCTURES MOBILES
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# -----------------------------------------------------------------
# SYSTEME MASSE-RESSORT A DEUX NOEUDS
# LE PREMIER NOEUD ECARTE DE SA POSITION INITIALE VIENT TAPER
# SUR LE DEUXIEME NOEUD IMMOBILE INITIALEMENT
# -----------------------------------------------------------------
# REF: PAS DE REF BIBLIO, COMPARAISON CALCUL MATLAB
# -----------------------------------------------------------------

DEBUT(CODE=_F(NOM='SDND112A',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='NON'))
# SDVERI='NON' car la verification est trop couteuse en CPU

MAILLA2=LIRE_MAILLAGE()

BICHOC=AFFE_MODELE(  MAILLAGE=MAILLA2,AFFE=(
              _F(  PHENOMENE = 'MECANIQUE', MODELISATION = 'DIS_T',
                     GROUP_MA = 'RESSORTS'),
                   _F(  PHENOMENE = 'MECANIQUE', MODELISATION = 'DIS_T',
                     GROUP_NO = ( 'MASSES1', 'MASSES2',)))
                    )

#
CARA_BIC=AFFE_CARA_ELEM(  MODELE=BICHOC,DISCRET=(
           _F(  CARA = 'K_T_D_L',  REPERE = 'GLOBAL', GROUP_MA = 'RESSORTS',
                     VALE = (98696., 0., 0., )),
           _F(  CARA = 'M_T_L',  REPERE = 'GLOBAL', GROUP_MA = 'RESSORTS',
                     VALE = (0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
                        0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0)),
                   _F(  CARA = 'M_T_D_N',  GROUP_NO = 'MASSES1', VALE = 25.),
                   _F(  CARA = 'M_T_D_N',  GROUP_NO = 'MASSES2', VALE = 25.))
                         )

#
CL_BICHO=AFFE_CHAR_MECA(  MODELE=BICHOC,DDL_IMPO=(
              _F(  GROUP_NO = 'ENCBICHO',    DX = 0.,  DY = 0.,  DZ = 0.),
              _F(  GROUP_NO = ( 'MASSES1', 'MASSES2',), DY = 0.,  DZ = 0.),),
                       )
#
FORCE=AFFE_CHAR_MECA(  MODELE=BICHOC,
                     FORCE_NODALE=_F( GROUP_NO = 'MASSES2',  FX = 1.)
                       )
#
ASSEMBLAGE(  MODELE=BICHOC,   CHARGE=CL_BICHO,   CARA_ELEM=CARA_BIC,
                   NUME_DDL=CO("NUMDDLC"),MATR_ASSE=(
                 _F(  MATRICE = CO("RIGI_BIC"),  OPTION = 'RIGI_MECA'),
                           _F(  MATRICE = CO("MASS_BIC"), OPTION = 'MASS_MECA'),),
             VECT_ASSE=(_F(VECTEUR=CO("VECTASS"),OPTION='CHAR_MECA',CHARGE=FORCE),),)

#
# CALCUL DES MODES AVEC BLOCAGE DES ANCRAGES:
#---------------------------------------------
MODE_BIC=MODE_ITER_SIMULT(  MATR_RIGI=RIGI_BIC,   MATR_MASS=MASS_BIC,
                  METHODE='JACOBI',
                   OPTION='SANS',
                 CALC_FREQ=_F(  OPTION = 'BANDE',  FREQ = (1., 10., ))
                            )

#
DEPLPHY1=CREA_CHAMP( OPERATION='AFFE', TYPE_CHAM='NOEU_DEPL_R',
                     MAILLAGE=MAILLA2,
                     AFFE=(
                         _F( GROUP_NO = 'MASSES1',
                             NOM_CMP = ('DX','DY','DZ',), VALE = (0.E0, 0., 0.,)),
                         _F( GROUP_NO = 'MASSES2',
                             NOM_CMP = ('DX','DY','DZ',), VALE = (0.001,  0., 0., ))),
                     CHAM_NO=VECTASS , PROL_ZERO='OUI')


#
PROJ_BASE(  BASE=MODE_BIC,
                  STOCKAGE='DIAG',
                  MATR_ASSE_GENE=(
                   _F(
                                   MATRICE = CO("MASSEGEN"),
                                   MATR_ASSE = MASS_BIC),
                   _F(
                                   MATRICE = CO("RIGIDGEN"),
                                   MATR_ASSE = RIGI_BIC)),
                   VECT_ASSE_GENE=_F(
                                   VECTEUR = CO("DEPLINI1"),
                                   VECT_ASSE = DEPLPHY1,
                                   TYPE_VECT = 'DEPL')
)

#
# DESCRIPTION DES PAS DE CALCUL
#------------------------------

L_INST=DEFI_LIST_REEL(  DEBUT=0.,
                            INTERVALLE=_F(  JUSQU_A = 1., PAS = 0.0001) )

#
# DEFINITION DE L'OBSTACLE ENTRE DEUX POINTS
# LIMITE LES DEPLACEMENTS EN X ABSOLU (Z LOCAL)
#-----------------------------------------------
GRILLE=DEFI_OBSTACLE(   TYPE='BI_PLAN_Z' )

#
TRAN_GE1=DYNA_VIBRA( TYPE_CALCUL='TRAN',
                     BASE_CALCUL='GENE',
                     MATR_MASS=MASSEGEN,   MATR_RIGI=RIGIDGEN,
                       SCHEMA_TEMPS=_F(SCHEMA='ADAPT_ORDRE2',),
                       AMOR_MODAL=_F(AMOR_REDUIT=( 0.0, 0.0,),),
                       ETAT_INIT=_F( DEPL= DEPLINI1),
                 PARA_LAME_FLUI = _F(NMAX_ITER=100,
                                      LAMBDA=10.,
                                      RESI_RELA=1.E-5,),
# CONDITION DE CHOC ENTRE LE NOEUD 2 ET LE NOEUD 12
# TEST SUR LA DIFFERENCE DES DEPLACEMENTS ABSOLUS
                CHOC=_F(  GROUP_NO_1 = 'MASSES1',
                       GROUP_NO_2 = 'MASSES2',
                       OBSTACLE = GRILLE,
                       INTITULE = 'NO2/NO12',
                       NORM_OBST = (0., 0., 1.,),
                       DIST_1 = 0.4495,
                       DIST_2 = 0.4495,
#                      RIGI_NOR: 2.88E7
# NE REGARDE QUE L'EFFET DE LA LAME FLUIDE
                       RIGI_NOR = 2.88E10,
                       AMOR_NOR = 0.,
                       RIGI_TAN = 0.,
                       FROTTEMENT='COULOMB',
                       COULOMB = 0.,
                       LAME_FLUIDE = 'OUI',
                       ALPHA = -0.08325,
                       BETA = 0.07493,
                       CHI = -0.9996E-6,
                       DELTA = -0.1665),
                 INCREMENT=_F( INST_INIT = 0.,  INST_FIN = 1.,
                            PAS = 0.00001),
                ARCHIVAGE=_F(  PAS_ARCH = 100)
                )

#
# RESTITUTION DES DEPLACEMENTS SUR LA BASE PHYSIQUE
# -------------------------------------------------
DYNATRA1=REST_GENE_PHYS(   RESU_GENE=TRAN_GE1,
                              NOM_CHAM='DEPL',
                              INTERPOL='LIN',
                         MODE_MECA=MODE_BIC,
                          LIST_INST=L_INST
                         )

TEST_RESU(RESU=(_F(INST=0.050000000000000003,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-6.7500000000000004E-4,
                   TOLE_MACHINE=(1.E-3, 0.000005,),
                   CRITERE=('RELATIF', 'ABSOLU'),
                   ),
                _F(INST=0.10000000000000001,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC= 5.46564969E-04,
                   VALE_REFE=5.44E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=6.0000000000000001E-3, TOLE_MACHINE=(6.0000000000000001E-3, 5.0000000000000004E-06),),
                _F(INST=0.45000000000000001,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-4.87733842E-04,
                   VALE_REFE=-4.73E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.040000000000000001, TOLE_MACHINE=(0.040000000000000001, 2.0000000000000002E-05),),
                _F(INST=0.94999999999999996,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-4.99616796E-04,
                   VALE_REFE=-4.6799999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.070000000000000007, TOLE_MACHINE=(0.070000000000000007, 4.0000000000000003E-05),),
                _F(INST=0.050000000000000003,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-3.24215104E-04,
                   VALE_REFE=-3.2200000000000002E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=8.0000000000000002E-3, TOLE_MACHINE=(8.0000000000000002E-3, 5.0000000000000004E-06),),
                _F(INST=0.10000000000000001,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC= 4.52626108E-04,
                   VALE_REFE=4.4999999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=7.0000000000000001E-3, TOLE_MACHINE=(7.0000000000000001E-3, 5.0000000000000004E-06),),
                _F(INST=0.45000000000000001,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-5.11680886E-04,
                   VALE_REFE=-4.9700000000000005E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.040000000000000001, TOLE_MACHINE=(0.040000000000000001, 3.0000000000000001E-05),),
                _F(INST=0.94999999999999996,
                   RESULTAT=DYNATRA1,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-4.99750036E-04,
                   VALE_REFE=-4.6799999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.070000000000000007, TOLE_MACHINE=(0.070000000000000007, 4.0000000000000003E-05),),
                ),
          )

#
TRAN_GE2=DYNA_VIBRA( TYPE_CALCUL='TRAN',
                     BASE_CALCUL='GENE',
                     MATR_MASS=MASSEGEN,   MATR_RIGI=RIGIDGEN,
                       SCHEMA_TEMPS=_F(SCHEMA='EULER',),
                       AMOR_MODAL=_F(AMOR_REDUIT=( 0.0, 0.0,),),
                       ETAT_INIT=_F( DEPL = DEPLINI1),
                 PARA_LAME_FLUI=_F(NMAX_ITER=100,
                                    LAMBDA=10.,
                                   RESI_RELA=1.E-5),
# CONDITION DE CHOC ENTRE LE NOEUD 2 ET LE NOEUD 12
# TEST SUR LA DIFFERENCE DES DEPLACEMENTS ABSOLUS
                CHOC=_F(  GROUP_NO_1 = 'MASSES1',
                       GROUP_NO_2 = 'MASSES2',
                       OBSTACLE = GRILLE,
                       INTITULE = 'NO2/NO12',
                       NORM_OBST = (0., 0., 1.,),
                       DIST_1 = 0.4495,
                       DIST_2 = 0.4495,
#                      RIGI_NOR: 2.88E7
# NE REGARDE QUE L'EFFET DE LA LAME FLUIDE
                       RIGI_NOR = 2.88E10,
                       AMOR_NOR = 0.,
                       RIGI_TAN = 0.,
                       FROTTEMENT='COULOMB',
                       COULOMB = 0.,
                       LAME_FLUIDE = 'OUI',
                       ALPHA = -0.08325,
                       BETA = 0.07493,
                       CHI = -0.9996E-6,
                       DELTA = -0.1665),
                 INCREMENT=_F( INST_INIT = 0.,  INST_FIN = 1.,
                            VERI_PAS = 'NON',
                            PAS = 0.00001),
                ARCHIVAGE=_F(  PAS_ARCH = 100)
                )

#
# RESTITUTION DES DEPLACEMENTS SUR LA BASE PHYSIQUE
# -------------------------------------------------
DYNATRA2=REST_GENE_PHYS(   RESU_GENE=TRAN_GE2,
                              NOM_CHAM='DEPL',
                              INTERPOL='LIN',
                         MODE_MECA=MODE_BIC,
                          LIST_INST=L_INST
                         )

TEST_RESU(RESU=(_F(INST=0.050000000000000003,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-6.75680412E-04,
                   VALE_REFE=-6.7500000000000004E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=2.E-3, TOLE_MACHINE=(2.E-3, 5.0000000000000004E-06),),
                _F(INST=0.10000000000000001,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC= 5.46889168E-04,
                   VALE_REFE=5.44E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=6.0000000000000001E-3, TOLE_MACHINE=(6.0000000000000001E-3, 5.0000000000000004E-06),),
                _F(INST=0.45000000000000001,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-4.88034918E-04,
                   VALE_REFE=-4.73E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.040000000000000001, TOLE_MACHINE=(0.040000000000000001, 1.0000000000000001E-05),),
                _F(INST=0.94999999999999996,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO2',
                   NOM_CMP='DX',
                   VALE_CALC=-4.99936569E-04,
                   VALE_REFE=-4.6799999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.070000000000000007, TOLE_MACHINE=(0.070000000000000007, 4.0000000000000003E-05),),
                _F(INST=0.050000000000000003,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-3.24319588E-04,
                   VALE_REFE=-3.2200000000000002E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=8.0000000000000002E-3, TOLE_MACHINE=(8.0000000000000002E-3, 5.0000000000000004E-06),),
                _F(INST=0.10000000000000001,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC= 4.53110832E-04,
                   VALE_REFE=4.4999999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=7.0000000000000001E-3, TOLE_MACHINE=(7.0000000000000001E-3, 5.0000000000000004E-06),),
                _F(INST=0.45000000000000001,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-5.11965084E-04,
                   VALE_REFE=-4.9700000000000005E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.040000000000000001, TOLE_MACHINE=(0.040000000000000001, 3.0000000000000001E-05),),
                _F(INST=0.94999999999999996,
                   RESULTAT=DYNATRA2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO12',
                   NOM_CMP='DX',
                   VALE_CALC=-5.00063435E-04,
                   VALE_REFE=-4.6799999999999999E-4,
                   REFERENCE='NON_DEFINI',
                   CRITERE=('RELATIF', 'ABSOLU'),
                   PRECISION=0.070000000000000007, TOLE_MACHINE=(0.070000000000000007, 4.0000000000000003E-05),),
                ),
          )

FIN()
#
