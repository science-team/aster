# TITRE CALCUL D'UNE LIGNE D'ARBRE AVEC 3 DISQUES (CADYRO : DLRR08)
# REFERENCE : M. LALANNE, G. FERRARIS -        RORORDYNAMIC, PREDICTION IN
# ENGINEERING - pp 68-71
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
DEBUT(CODE=_F(NOM='SDLL140A',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'),
      IGNORE_ALARM=('ALGELINE4_87'))
# On desactive une alarme sur le comptage des valeurs propres complexes
# dans la methode de calcul des modes QZ.
# On verifie que, malgre le message, l'on a toutes les valeurs propres voulues.

MAIL=LIRE_MAILLAGE(FORMAT='MED',)

MODELE=AFFE_MODELE(MAILLAGE=MAIL,
                   AFFE=(_F(GROUP_MA='ROTOR',
                           PHENOMENE='MECANIQUE',
                           MODELISATION='POU_D_T',),
                         _F(GROUP_MA=('PALIER_A','PALIER_E','DISQUE1','DISQUE2','DISQUE3'),
                           PHENOMENE='MECANIQUE',
                           MODELISATION='DIS_TR',),))

ACIER=DEFI_MATERIAU(ELAS=_F(E=2.0E11,
                            NU=0.3,
                            RHO=7800.0,),);


CHMAT=AFFE_MATERIAU(MAILLAGE=MAIL,
                    AFFE=(_F(GROUP_MA='ROTOR',
                            MATER=ACIER,),))

CARELEM=AFFE_CARA_ELEM(MODELE=MODELE,
                       POUTRE=(_F(GROUP_MA='ROTOR',
                                 SECTION='CERCLE',
                                 CARA='R',
                                 VALE=(.05,),),),
                        DISCRET=(
                                 _F(GROUP_MA='PALIER_A',
                                    CARA='K_TR_D_N',
                                    VALE=(0.,.5E8,.7E8,0.,0.,0.)
                                  ),
                                 _F(GROUP_MA='PALIER_A',
                                    CARA='M_TR_D_N',
                                    VALE=(0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,)
                                  ),
                                 _F(GROUP_MA='PALIER_E',
                                    CARA='K_TR_D_N',
                                    VALE=(0.,.5E8,.7E8,0.,0.,0.)
                                  ),
                                 _F(GROUP_MA='PALIER_E',
                                    CARA='M_TR_D_N',
                                    VALE=(0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,)
                                  ),
                                 _F(GROUP_MA='PALIER_A',
                                    CARA='A_TR_D_N',
                                    VALE=(0.,.5E3,.7E3,0.,0.,0.)
                                  ),
                                 _F(GROUP_MA='PALIER_E',
                                    CARA='A_TR_D_N',
                                    VALE=(0.,.5E3,.7E3,0.,0.,0.)
                                  ),
                                 _F(GROUP_MA='DISQUE1',
                                    CARA='M_TR_D_N',
                                    VALE=(14.580130,0.1232021,0.06463858,0.06463858,0,0,0,0,0,0),
                                  ),
                                 _F(GROUP_MA='DISQUE2',
                                    CARA='M_TR_D_N',
                                    VALE=(45.945793,0.97634809,0.4977460,0.4977460,0,0,0,0,0,0),
                                  ),
                                 _F(GROUP_MA='DISQUE3',
                                    CARA='M_TR_D_N',
                                    VALE=(55.134951,1.1716177,0.6023493,0.6023493,0,0,0,0,0,0),
                                  ),
                        ))
                                 
                                 

BLOQUAGE=AFFE_CHAR_MECA(MODELE=MODELE,
                        DDL_IMPO=(_F(GROUP_NO=('PALIER_A',),
                                     DX=0., DRX=0.
                                     ),
                                   ))
                                    

ASSEMBLAGE(MODELE=MODELE,
                CHAM_MATER=CHMAT,
                CARA_ELEM=CARELEM,
                CHARGE=BLOQUAGE,
                NUME_DDL=CO('NUMEDDL'),
                MATR_ASSE=(_F(MATRICE=CO('RIGIDITE'),
                              OPTION='RIGI_MECA',),
                           _F(MATRICE=CO('MASSE'),
                              OPTION='MASS_MECA',),
                           _F(MATRICE=CO('AMOR'),
                              OPTION='AMOR_MECA',),),);

GYELEM=CALC_MATR_ELEM(OPTION='MECA_GYRO',
                      MODELE=MODELE,
                      CHAM_MATER=CHMAT,
                      CARA_ELEM=CARELEM,);


GYASS=ASSE_MATRICE(MATR_ELEM=GYELEM,
                   NUME_DDL=NUMEDDL,);


                    
MODES=MODE_ITER_SIMULT(MATR_RIGI=RIGIDITE,
                       MATR_MASS=MASSE,
                       CALC_FREQ=_F(OPTION='BANDE',
                                    FREQ=(3.,3000.)));

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=60.606430550894,
                   VALE_REFE=60.618000000000002,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=63.016081329435,
                   VALE_REFE=63.029000000000003,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=169.44005603643,
                   VALE_REFE=169.51300000000001,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=185.48699170614,
                   VALE_REFE=185.584,),
                _F(NUME_ORDRE=7,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=329.48177561895,
                   VALE_REFE=329.613,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=361.91136420893,
                   VALE_REFE=362.089,),
                _F(NUME_ORDRE=9,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=528.86325488957,
                   VALE_REFE=529.29100000000005,),
                _F(NUME_ORDRE=10,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=557.05890203817,
                   VALE_REFE=557.54899999999998,),
                _F(NUME_ORDRE=12,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=830.48999536497,
                   VALE_REFE=831.11099999999999,),
                _F(NUME_ORDRE=13,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODES,
                   VALE_CALC=845.3138314008,
                   VALE_REFE=846.01300000000003,),
                ),
          )

OM=25000.*pi/30.

GYOM=COMB_MATR_ASSE(COMB_R=(_F(MATR_ASSE=GYASS, COEF_R=OM,),
                            _F(MATR_ASSE=AMOR, COEF_R=1.,),))



MODEG=MODE_ITER_SIMULT(MATR_RIGI=RIGIDITE,
                       MATR_MASS=MASSE,
                       MATR_AMOR=GYOM,
                       METHODE='SORENSEN',
                       VERI_MODE=_F(SEUIL=1.E-3),
                       CALC_FREQ=_F(OPTION='CENTRE',
                                    FREQ=1.,
                                    NMAX_FREQ=16))


MODEZ=MODE_ITER_SIMULT(MATR_RIGI=RIGIDITE,
                       MATR_MASS=MASSE,
                       MATR_AMOR=GYOM,INFO=1,
                       VERI_MODE=_F(SEUIL=1.E-3),
                       METHODE='QZ',
                       CALC_FREQ=_F(OPTION='PLUS_PETITE',
                                    NMAX_FREQ=12 ))



TEST_RESU(RESU=(
               _F(NUME_ORDRE=1,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=55.405182342234,
                   VALE_REFE=55.408000000000001,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=67.184414707958,
                   VALE_REFE=67.209000000000003,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=112.1061888258,
                   VALE_REFE=112.106,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=157.85192427669,
                   VALE_REFE=157.904,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=193.54765473224,
                   VALE_REFE=193.70599999999999,),
                _F(NUME_ORDRE=6,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=249.79952434503,
                   VALE_REFE=249.898,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=407.13788808901,
                   VALE_REFE=407.61900000000003,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=9,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=446.23375186581,
                   VALE_REFE=446.62200000000001,),
                _F(NUME_ORDRE=11,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=622.01880015503,
                   VALE_REFE=622.654,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=12,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEG,
                   VALE_CALC=713.93620378516,
                   VALE_REFE=715.02599999999995,
                   PRECISION=2.E-3,),
                ),
          )

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=55.405182266345,
                   VALE_REFE=55.408000000000001,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=67.184414918293,
                   VALE_REFE=67.209000000000003,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=112.10649804259,
                   TOLE_MACHINE=1.E-5,      # ajustement pour calibre7
                   VALE_REFE=112.106,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=157.85192448454,
                   VALE_REFE=157.904,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=193.54765435822,
                   VALE_REFE=193.70599999999999,),
                _F(NUME_ORDRE=6,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=249.79952176862,
                   VALE_REFE=249.898,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=407.13788313021,
                   VALE_REFE=407.61900000000003,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=9,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=446.23376180924,
                   VALE_REFE=446.62200000000001,),
                _F(NUME_ORDRE=11,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=622.01878612716,
                   VALE_REFE=622.654,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=12,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MODEZ,
                   VALE_CALC=713.93623720862,
                   VALE_REFE=715.02599999999995,
                   PRECISION=2.E-3,),
                ),
          )

PROJ_BASE(BASE=MODES,
          STOCKAGE='PLEIN',
          MATR_ASSE_GENE=(_F(MATRICE=CO('MAGE'),
                             MATR_ASSE=MASSE,),
                          _F(MATRICE=CO('RIGE'),
                             MATR_ASSE=RIGIDITE,),
                          _F(MATRICE=CO('OMGE'),
                             MATR_ASSE=GYOM,),),);


MOD2ETAG=MODE_ITER_SIMULT(MATR_RIGI=RIGE,
                          MATR_MASS=MAGE,
                          MATR_AMOR=OMGE,
                          METHODE='QZ',
                          CALC_FREQ=_F(OPTION='TOUT'),)

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=55.407535354874,
                   VALE_REFE=55.408000000000001,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=67.187161166996,
                   VALE_REFE=67.209000000000003,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=157.86259591403,
                   VALE_REFE=157.904,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=193.55635789501,
                   VALE_REFE=193.70599999999999,),
                _F(NUME_ORDRE=6,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=249.85162074883,
                   VALE_REFE=249.898,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=407.37831950119,
                   VALE_REFE=407.61900000000003,),
                _F(NUME_ORDRE=9,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=446.50796066819,
                   VALE_REFE=446.62200000000001,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=11,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=622.50516147094,
                   VALE_REFE=622.654,
                   PRECISION=2.E-3,),
                _F(NUME_ORDRE=12,
                   PARA='FREQ',
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=MOD2ETAG,
                   VALE_CALC=714.29896173011,
                   VALE_REFE=715.02599999999995,
                   PRECISION=2.E-3,),
                ),
          )

FIN()
