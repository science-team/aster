# TITRE CONTACT AVEC FROTTEMENT D'UNE PLAQUE SUR UN PLAN RIGIDE
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
#
#
# ======================================================================
# CAS_TEST__:SSNV128H
# TEST DU GRECO CALCUL 3D
# MODELE
# MAILLAGE : ELEMENT 3D PENTAEDRE A 6 NOEUDS
# UNITES   : NEWTON , METRE , SECONDE (N,M,S)
#

DEBUT( CODE=_F(  NOM = 'SSNV128H',NIV_PUB_WEB='INTERNET'),
IGNORE_ALARM=('CALCULEL2_63'),DEBUG=_F(SDVERI='OUI'))

#......................................................................
#  CALCUL TEST ELEMENTS FINIS DE CONTACT 3D PENTA6
#  PLAQUE AVEC CONTACT ET FROTTEMENT SUR UN PLAN RIGIDE
#......................................................................
#
MA=LIRE_MAILLAGE(FORMAT='MED',)
#
MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=_F(TOUT = 'OUI',
                       PHENOMENE = 'MECANIQUE',
                       MODELISATION = '3D',)
              )
#
MA=MODI_MAILLAGE(reuse=MA,
                 MAILLAGE=MA,
                 ORIE_PEAU_3D=(_F(GROUP_MA = 'SPRESV',),
                               _F(GROUP_MA = 'SPRESH',))
                )
#
MATPLA=DEFI_MATERIAU(ELAS=_F(E = 1.3E11,
                             NU = 0.2,)
                    )
#
CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=_F(TOUT = 'OUI',
                             MATER = MATPLA,)
                    )
#
CHA1=AFFE_CHAR_MECA(MODELE=MO,
                    DDL_IMPO=(_F(GROUP_MA = 'SBATI',  DX = 0.0, DY = 0.0, DZ=0.0,),
                              _F(GROUP_MA = 'SBLOCX', DX = 0.0,),
                              _F(GROUP_MA = 'LBLOCY', DY = 0.0,),
                              _F(GROUP_MA = 'VPLAQ',  DZ = 0.0,)),
                    PRES_REP=(_F(GROUP_MA = 'SPRESV', PRES = 5.E07,),
                              _F(GROUP_MA = 'SPRESH', PRES = 15.E07,))
                   )
#
CHA2=DEFI_CONTACT(MODELE         = MO,
                  FORMULATION    = 'DISCRETE',
                  FROTTEMENT     = 'COULOMB',
                  REAC_GEOM      = 'CONTROLE',
                  NB_ITER_GEOM   = 2,
                  ZONE=_F(
                          GROUP_MA_MAIT  = 'SBATI',
                          GROUP_MA_ESCL  = 'SCONTA',
                          ALGO_CONT      = 'LAGRANGIEN',
                          ALGO_FROT      = 'LAGRANGIEN',
                          COEF_MATR_FROT = 0.8,
                          COULOMB        = 1.0,
                        )
                  )
#
CHA3=DEFI_CONTACT(MODELE         = MO,
                  FORMULATION    = 'DISCRETE',
                  FROTTEMENT     = 'COULOMB',
                  ZONE=_F(
                          GROUP_MA_MAIT  = 'SBATI',
                          GROUP_MA_ESCL  = 'SCONTA',
                          ALGO_CONT      = 'LAGRANGIEN',
                          ALGO_FROT      = 'PENALISATION',
                          E_T            = 1.0E11,
                          COULOMB        = 1.0,
                          COEF_MATR_FROT = 0.4,
                        )
                  )

#
RAMPE=DEFI_FONCTION(NOM_PARA='INST',
                    PROL_GAUCHE='LINEAIRE',
                    PROL_DROITE='LINEAIRE',
                    VALE=(0.0,0.0,1.0,1.0,)
                    )
#
L_INST=DEFI_LIST_REEL(DEBUT=0.,
                      INTERVALLE=_F(JUSQU_A = 1.0, NOMBRE = 1,)
                      )
#
#-----------------------------------------------------------
#
U2M=STAT_NON_LINE(SOLVEUR=_F(SYME='OUI',),
                  MODELE    = MO,
                CHAM_MATER = CHMAT,
                EXCIT      =(_F(CHARGE = CHA1,
                                FONC_MULT = RAMPE,),
                             ),
                CONTACT    = CHA2,
                COMP_ELAS  = _F(RELATION = 'ELAS',),
                NEWTON     = _F(REAC_ITER = 1,),
                INCREMENT  = _F(LIST_INST = L_INST,),
                CONVERGENCE= _F(ARRET = 'OUI',
                               ITER_GLOB_MAXI = 200,
                               RESI_GLOB_RELA = 1.0E-5,),
#                INFO=2,
               )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85085005E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.71619144E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28362823E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.97837007E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54016744E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#-----------------------------------------------------------
#
U2L=STAT_NON_LINE(MODELE    = MO,
                CHAM_MATER = CHMAT,
                EXCIT      =(_F(CHARGE = CHA1,
                                FONC_MULT = RAMPE,),
                             ),
                 CONTACT    = CHA2,
                COMP_ELAS  = _F(RELATION = 'ELAS',),
                SOLVEUR    = _F(SYME='OUI',
                                METHODE = 'LDLT',),
                NEWTON     = _F(REAC_ITER = 1,),
                INCREMENT  = _F(LIST_INST = L_INST,),
                CONVERGENCE= _F(ARRET = 'OUI',
                               ITER_GLOB_MAXI = 200,
                               RESI_GLOB_RELA = 1.0E-5,),
#                INFO=2,
               )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85085005E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.71619144E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28362823E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.97837007E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54016744E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#-----------------------------------------------------------
#
U3M=STAT_NON_LINE(SOLVEUR=_F(SYME='OUI',),
                  MODELE      = MO,
                 CHAM_MATER  = CHMAT,
                 EXCIT       =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                                               CONTACT    = CHA3,
                 COMP_ELAS   = _F(RELATION = 'ELAS',),
                 NEWTON      = _F(REAC_ITER = 1,),
                 INCREMENT   = _F(LIST_INST = L_INST,),
                 CONVERGENCE = _F(ARRET = 'OUI',
                                  ITER_GLOB_MAXI = 200,
                                  RESI_GLOB_RELA = 1.0E-5,),
#                 INFO=2,
               )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85085428E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.71619570E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28363224E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.97837426E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54017215E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#-----------------------------------------------------------
#
U3L=STAT_NON_LINE(MODELE      = MO,
                 CHAM_MATER  = CHMAT,
                 EXCIT       =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                 CONTACT    = CHA3,
                 COMP_ELAS   = _F(RELATION = 'ELAS',),
                 SOLVEUR     = _F(SYME='OUI',
                                  METHODE = 'LDLT',),
                 NEWTON      = _F(REAC_ITER = 1,),
                 INCREMENT   = _F(LIST_INST = L_INST,),
                 CONVERGENCE = _F(ARRET = 'OUI',
                                  ITER_GLOB_MAXI = 200,
                                  RESI_GLOB_RELA = 1.0E-5,),
#                 INFO=2,
               )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85085428E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.71619570E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28363224E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.97837426E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54017215E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#
FIN()
#
