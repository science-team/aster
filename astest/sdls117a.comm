# TITRE PLAQUE EXCENTREE ORTHOTROPE ET AMORTIE SOUMISE A DES VIBRATIONS
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#       (CALCUL MODALE ET CALCUL HARMONIQUE)

# SDLS117A
# PLAQUE  RECTANGULAIRE EXCENTREE
# LE MODELE COMPORTE 66 NOEUDS ET 100 ELEMENTS DKT
# LE COTE A1A4 (L14) EST ENCASTRE
# ON IMPOSE UNE FORCE LINEIQUE SELON X SUR LE COTE A2A3 (L12)
#----------------------------------------------------------------------
#POINTS:  A4                          A3
#           -------------------------
#           -                       -
#           -                       -
#  Y        -                       -
# I         -                       -
# I         -------------------------
# I       A1                          A2
# --> X
# ______________________________________________________________________
#

DEBUT(CODE=_F(NOM='SDLS117A',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(FORMAT='MED',INFO=2 )

MA=DEFI_GROUP( reuse=MA,   MAILLAGE=MA,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

MA=DEFI_GROUP( reuse=MA,  MAILLAGE=MA,
                     CREA_GROUP_NO=_F( GROUP_MA = 'PLAQ1',
                                    NOM = 'PLAQ1')
                 )

MO=AFFE_MODELE(  MAILLAGE=MA,
                    AFFE=_F( TOUT = 'OUI', PHENOMENE = 'MECANIQUE',
                    MODELISATION = 'DKT') )

EP=AFFE_CARA_ELEM(  MODELE=MO,COQUE=(

#   POUR AFFECTER DES CARACTERISTIQUES AUX MAILLES DE BORD
#   DONC EVITER LES ALARMES
                       _F(  GROUP_MA = 'TOUT',  EPAIS = 0.2),
                       _F( GROUP_MA = 'PLAQ1', EPAIS = 0.2,
                       ANGL_REP = (0.,0.,),  EXCENTREMENT = 0.1,
                       INER_ROTA = 'OUI'))
                     )

## AJOUT : amortissement hysteretique defini par eta

eta = 0.1
alpha = 0.1
beta  = 0.000001

MAT1A=DEFI_MATERIAU(  ELAS_ORTH=_F( E_L = 20000., E_T = 20000.,
                                    E_N = 20000., NU_LT = 0., NU_LN = 0., NU_TN = 0.,
                                    G_LT = 2000.,  G_LN = 0., G_TN = 0.,
                                    RHO = 1000.,
                                    AMOR_HYST  = eta,
                                    AMOR_ALPHA = alpha,
                                    AMOR_BETA  = beta) )

MU=DEFI_COMPOSITE(
               COUCHE=_F( EPAIS = 0.2, MATER = MAT1A, ORIENTATION = 0.)
                      )

CHMUL=AFFE_MATERIAU(  MAILLAGE=MA,
                      AFFE=_F( TOUT = 'OUI',MATER = MU) )

CHME=AFFE_CHAR_MECA(  MODELE=MO,
                     DDL_IMPO=_F(
                        GROUP_MA = 'L14',
                        DX = 0., DY = 0., DZ = 0.,  DRX = 0., DRY = 0., DRZ = 0.),)

## AJOUT : force dynamique elementaire

CHEXT=AFFE_CHAR_MECA(MODELE=MO,FORCE_ARETE=_F(GROUP_MA = 'L12',  FX = 1000., MY=100.))

# ______________________________________________________________________
#
#                         RESOLUTION
# ______________________________________________________________________
#
# CALCUL AVEC LA PLAQUE EXCENTREE
#################################

MEL=CALC_MATR_ELEM(  MODELE=MO, CHARGE=(CHME,),
                     CHAM_MATER=CHMUL,
                     CARA_ELEM=EP, OPTION='RIGI_MECA' )

MASEL=CALC_MATR_ELEM(  MODELE=MO, CHARGE=(CHME,),
                       CHAM_MATER=CHMUL,
                       CARA_ELEM=EP, OPTION='MASS_MECA' )

## AJOUT : calcul d'une matrice de rigidite geometrique

RIHYEL=CALC_MATR_ELEM( MODELE=MO, CHARGE=(CHME,),
                        CHAM_MATER = CHMUL,
                        RIGI_MECA  = MEL,
                        CARA_ELEM=EP, OPTION='RIGI_MECA_HYST')

## AJOUT : matrice d'amortissement de Rayleigh

AMOEL=CALC_MATR_ELEM( MODELE=MO, CHARGE=(CHME,),
                      CHAM_MATER=CHMUL,
                      RIGI_MECA=MEL,
                      MASS_MECA=MASEL,
                      CARA_ELEM=EP, OPTION='AMOR_MECA')

## AJOUT : vecteur de force elementaire

VEC = CALC_VECT_ELEM(OPTION='CHAR_MECA',
                        CHARGE=CHEXT)

NU=NUME_DDL(  MATR_RIGI=RIHYEL )


# Assemblage

VECASS = ASSE_VECTEUR(VECT_ELEM=VEC,   NUME_DDL=NU )
MATASS = ASSE_MATRICE(MATR_ELEM=MEL,   NUME_DDL=NU )
MATAMS = ASSE_MATRICE(MATR_ELEM=AMOEL, NUME_DDL=NU )
MATASM = ASSE_MATRICE(MATR_ELEM=MASEL, NUME_DDL=NU )
RIHYAS = ASSE_MATRICE(MATR_ELEM=RIHYEL,NUME_DDL=NU )

# fonction H(f) multiplicatrice pour le chargement

FONC=DEFI_FONCTION(NOM_PARA='FREQ',
                    VALE=(   0.,1.,
                             1.,1.,
                           399.,1.,
                           400.,0.,
                          1000.,0.),
                    PROL_DROITE='LINEAIRE',);

lis_freq=DEFI_LIST_REEL(  DEBUT=0,
                          INTERVALLE=_F(  JUSQU_A = 1000,  NOMBRE = 1000)
                        )

## AJOUT : calcul d'une reponse dynamique
## avec la matrice de raideur hysteretique et la matrice d'amortissement
## fabriquee avec AMOR_ALPHA et AMOR_BETA

DEP_DYN=DYNA_VIBRA(TYPE_CALCUL='HARM',BASE_CALCUL='PHYS',
                       MATR_MASS=MATASM,
                       MATR_RIGI=RIHYAS,
                       MATR_AMOR=MATAMS,
                       LIST_FREQ=lis_freq,
                       EXCIT=_F(VECT_ASSE=VECASS,FONC_MULT=FONC,
                       ))

TEST_RESU(RESU=(_F(NUME_ORDRE=200,
                   GROUP_NO='A2',
                   VALE_CALC_C=0j,
                   TOLE_MACHINE=1.E-3,
                   RESULTAT=DEP_DYN,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=350,
                   GROUP_NO='A2',
                   VALE_CALC_C=0j,
                   TOLE_MACHINE=1.E-3,
                   RESULTAT=DEP_DYN,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=600,
                   GROUP_NO='A2',
                   VALE_CALC_C=0j,
                   TOLE_MACHINE=1.E-3,
                   RESULTAT=DEP_DYN,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=800,
                   GROUP_NO='A2',
                   VALE_CALC_C=0j,
                   TOLE_MACHINE=1.E-3,
                   RESULTAT=DEP_DYN,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   CRITERE='ABSOLU',
                   ),
                ),
          )

FIN()
#
