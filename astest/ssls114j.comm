# TITRE NON REGRESSION SUR QUART DE VIROLE CYLINDRIQUE
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# COQUE_C_PLAN

DEBUT(CODE=_F(NOM='SSLS114J',
              NIV_PUB_WEB='INTERNET',),
      DEBUG=_F(SDVERI='OUI',),);

MAIL=LIRE_MAILLAGE(UNITE=20,
                   INFO=2,);

MO=AFFE_MODELE(MAILLAGE=MAIL,
               AFFE=_F(GROUP_MA='CYLINDRE',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='COQUE_C_PLAN',),);

MAIL=MODI_MAILLAGE(reuse =MAIL,
                   MAILLAGE=MAIL,
                   ORIE_NORM_COQUE=_F(GROUP_MA='CYLINDRE',
                                      VECT_NORM=(-1.0,0.0,),
                                      GROUP_NO='PA',),
                   INFO=2,);

MATE=DEFI_MATERIAU(ELAS=_F(E=2.E11,
                           NU=0.3,
                           RHO=1234.0,
                           ALPHA=0.,),);

CM=AFFE_MATERIAU(MAILLAGE=MAIL,
                 AFFE=_F(GROUP_MA='CYLINDRE',
                         MATER=MATE,),);

CA=AFFE_CARA_ELEM(MODELE=MO,
                  COQUE=_F(GROUP_MA='CYLINDRE',
                           EPAIS=0.05,
                           MODI_METRIQUE='NON',),);

CA_M=AFFE_CARA_ELEM(MODELE=MO,
                    COQUE=_F(GROUP_MA='CYLINDRE',
                             EPAIS=0.05,
                             MODI_METRIQUE='OUI',),);

FM10=DEFI_FONCTION(NOM_PARA='Y',NOM_RESU='PRES',VALE=(0.,10.,
                         0.5,10.,
                         ),INTERPOL='LIN',PROL_DROITE='CONSTANT',PROL_GAUCHE='CONSTANT',);

CH=AFFE_CHAR_MECA(MODELE=MO,
                  DDL_IMPO=(_F(GROUP_NO='PA',
                               DY=0.,
                               DRZ=0.,),
                            _F(GROUP_NO='PC',
                               DX=0.,
                               DRZ=0.,),),);

MASS_INE=POST_ELEM(MASS_INER=_F(TOUT='OUI',),
                   MODELE=MO,
                   CHAM_MATER=CM,
                   CARA_ELEM=CA,);

IMPR_TABLE(TABLE=MASS_INE,
           NOM_PARA=('LIEU','ENTITE','MASSE','CDG_X','CDG_Y','CDG_Z',),);

IMPR_TABLE(TABLE=MASS_INE,
           NOM_PARA=('LIEU','ENTITE','IX_G','IY_G','IZ_G','IXY_G','IXZ_G','IYZ_G',),);

IMPR_TABLE(TABLE=MASS_INE,
           NOM_PARA=('LIEU','ENTITE','IX_PRIN_G','IY_PRIN_G','IZ_PRIN_G','ALPHA','BETA','GAMMA',),);

TEST_TABLE(REFERENCE='ANALYTIQUE',
           VALE_CALC=96.918072020,
           VALE_REFE=96.918133363500004,
           NOM_PARA='MASSE',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(REFERENCE='ANALYTIQUE',
           VALE_CALC=0.636619369,
           VALE_REFE=0.63661977199999997,
           NOM_PARA='CDG_X',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(REFERENCE='ANALYTIQUE',
           VALE_CALC=0.636619369,
           VALE_REFE=0.63661977199999997,
           NOM_PARA='CDG_Y',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(CRITERE='ABSOLU',
           REFERENCE='ANALYTIQUE',
           VALE_CALC= 0.00000000E+00,
           VALE_REFE=0.0,
           NOM_PARA='CDG_Z',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(
           VALE_CALC=9.1796100000000003,
           TOLE_MACHINE=1.E-3,
           NOM_PARA='IX_G',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(
           VALE_CALC=9.1796100000000003,
           TOLE_MACHINE=1.E-3,
           NOM_PARA='IY_G',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

TEST_TABLE(
           VALE_CALC=18.359200000000001,
           TOLE_MACHINE=1.E-3,
           NOM_PARA='IZ_G',
           TABLE=MASS_INE,
           FILTRE=_F(NOM_PARA='LIEU',
                     VALE_K='MAIL',),
           )

#____________________ MODELISATION PRES_REP REEL _______________

F_PRES=DEFI_CONSTANTE(NOM_RESU='PRES',
                      VALE=0.,);

CHP_F=AFFE_CHAR_MECA_F(MODELE=MO,
                       PRES_REP=_F(GROUP_MA='CYLINDRE',
                                   PRES=F_PRES,),);

CHP=AFFE_CHAR_MECA(MODELE=MO,
                   PRES_REP=_F(GROUP_MA='CYLINDRE',
                               PRES=10.,),);

RESP=MECA_STATIQUE(MODELE=MO,
                   CHAM_MATER=CM,
                   CARA_ELEM=CA_M,
                   EXCIT=(_F(CHARGE=CH,),
                          _F(CHARGE=CHP,),
                          _F(CHARGE=CHP_F,),),);

RESP=CALC_CHAMP(reuse=RESP,RESULTAT=RESP,CONTRAINTE=('SIGM_ELNO'))


RESPMOY=POST_CHAMP(RESULTAT=RESP,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='MOY',),);
RESPMOY=CALC_CHAMP(reuse =RESPMOY,
                RESULTAT=RESPMOY,
                CONTRAINTE='SIGM_NOEU',);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESP,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESP,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   RESULTAT=RESPMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.029999999999999999,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   RESULTAT=RESPMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.029999999999999999,),
                ),
          )

#____________________ MODELISATION FORCE_COQUE REEL ________________

CHF=AFFE_CHAR_MECA(MODELE=MO,
                   FORCE_COQUE=_F(GROUP_MA='CYLINDRE',
                                  PRES=10.,),);

RESF=MECA_STATIQUE(MODELE=MO,
                   CHAM_MATER=CM,
                   CARA_ELEM=CA_M,
                   EXCIT=(_F(CHARGE=CH,),
                          _F(CHARGE=CHF,),),);

RESF=CALC_CHAMP(reuse=RESF,RESULTAT=RESF,CONTRAINTE=('SIGM_ELNO'))


RESFMOY=POST_CHAMP(RESULTAT=RESF,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='MOY',),);
RESFMOY=CALC_CHAMP(reuse =RESFMOY,
                RESULTAT=RESFMOY,
                CONTRAINTE='SIGM_NOEU',);

RESFINF=POST_CHAMP(RESULTAT=RESF,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='INF',),);
RESFINF=CALC_CHAMP(reuse =RESFINF,
                RESULTAT=RESFINF,
                CONTRAINTE='SIGM_NOEU',);

RESFSUP=POST_CHAMP(RESULTAT=RESF,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='SUP',),);
RESFSUP=CALC_CHAMP(reuse =RESFSUP,
                RESULTAT=RESFSUP,
                CONTRAINTE='SIGM_NOEU',);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESF,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESF,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=195.092881530,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=195.092881530,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=205.072896881,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESFSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=205.072896881,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                ),
          )

#____________________ MODELISATION FORCE_COQUE FONCTION ___________

CHG=AFFE_CHAR_MECA_F(MODELE=MO,
                     FORCE_COQUE=_F(GROUP_MA='CYLINDRE',
                                    PRES=FM10,),);

RESG=MECA_STATIQUE(MODELE=MO,
                   CHAM_MATER=CM,
                   CARA_ELEM=CA_M,
                   EXCIT=(_F(CHARGE=CH,),
                          _F(CHARGE=CHG,),),);

RESG=CALC_CHAMP(reuse=RESG,RESULTAT=RESG,CONTRAINTE=('SIGM_ELNO'))


RESGMOY=POST_CHAMP(RESULTAT=RESG,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='MOY',),);
RESGMOY=CALC_CHAMP(reuse =RESGMOY,
                RESULTAT=RESGMOY,
                CONTRAINTE='SIGM_NOEU',);

RESGINF=POST_CHAMP(RESULTAT=RESG,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='INF',),);
RESGINF=CALC_CHAMP(reuse =RESGINF,
                RESULTAT=RESGINF,
                CONTRAINTE='SIGM_NOEU',);

RESGSUP=POST_CHAMP(RESULTAT=RESG,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='SUP',),);
RESGSUP=CALC_CHAMP(reuse =RESGSUP,
                RESULTAT=RESGSUP,
                CONTRAINTE='SIGM_NOEU',);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESG,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESG,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC= 9.99789672E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.958438858,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=195.092881530,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=195.092881530,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=205.072896881,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESGSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=205.072896881,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                ),
          )

#______ MODELISATION FORCE_COQUE FONCTION SANS MODI_METRIQUE___________

CHM=AFFE_CHAR_MECA_F(MODELE=MO,
                     FORCE_COQUE=_F(GROUP_MA='CYLINDRE',
                                    PRES=FM10,),);

RESM=MECA_STATIQUE(MODELE=MO,
                   CHAM_MATER=CM,
                   CARA_ELEM=CA,
                   EXCIT=(_F(CHARGE=CH,),
                          _F(CHARGE=CHM,),),);

RESM=CALC_CHAMP(reuse=RESM,RESULTAT=RESM,CONTRAINTE=('SIGM_ELNO'))


RESMMOY=POST_CHAMP(RESULTAT=RESM,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='MOY',),);
RESMMOY=CALC_CHAMP(reuse =RESMMOY,
                RESULTAT=RESMMOY,
                CONTRAINTE='SIGM_NOEU',);

RESMINF=POST_CHAMP(RESULTAT=RESM,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='INF',),);
RESMINF=CALC_CHAMP(reuse =RESMINF,
                RESULTAT=RESMINF,
                CONTRAINTE='SIGM_NOEU',);

RESMSUP=POST_CHAMP(RESULTAT=RESM,
                   EXTR_COQUE=_F(NOM_CHAM='SIGM_ELNO',
                                 NUME_COUCHE=1,
                                 NIVE_COUCHE='SUP',),);
RESMSUP=CALC_CHAMP(reuse =RESMSUP,
                RESULTAT=RESMSUP,
                CONTRAINTE='SIGM_NOEU',);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESM,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 9.99997407E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESM,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC= 9.99997407E-10,
                   VALE_REFE=9.8190700000000005E-10,
                   CRITERE='RELATIF',
                   PRECISION=0.080000000000000002,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984405,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMMOY,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984405,
                   VALE_REFE=194.93754000000001,
                   CRITERE='RELATIF',
                   PRECISION=0.028000000000000001,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984477,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.051999999999999998,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMINF,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984477,
                   VALE_REFE=190.125,
                   CRITERE='RELATIF',
                   PRECISION=0.051999999999999998,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PA',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984332,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=1.E-3,),
                _F(NUME_ORDRE=1,
                   GROUP_NO='PC',
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESMSUP,
                   NOM_CHAM='SIGM_NOEU',
                   NOM_CMP='SIXX',
                   VALE_CALC=199.999984332,
                   VALE_REFE=200.125,
                   CRITERE='RELATIF',
                   PRECISION=1.E-3,),
                ),
          )

FIN();
