# TITRE CHOC D UNE POUTRE SUR UN APPUI : SOUS-STRUCTURATION
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS_TEST__: SDNL104B
#
# CALCUL DE LA REPONSE TRANSITOIRE NON-LINEAIRE D UNE POUTRE EN FLEXION
# CHOQUANT SUR UN APPUI ELASTIQUE PAR SOUS-STRUCTURATION
#
DEBUT(  CODE=_F( NOM = 'SDNL104B',NIV_PUB_WEB='INTERNET'),
       DEBUG=_F(SDVERI='OUI'))

#
MAYA=LIRE_MAILLAGE( )

MAYA=DEFI_GROUP( reuse=MAYA,   MAILLAGE=MAYA,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

#
MATERIO=DEFI_MATERIAU(  ELAS=_F( RHO = 1.E06,  NU = 0.3,  E = 1.E10))

#
CHMAT=AFFE_MATERIAU(  MAILLAGE=MAYA,
                              AFFE=_F( TOUT = 'OUI',   MATER = MATERIO))

#
BARRE=AFFE_MODELE(  MAILLAGE=MAYA,
                            AFFE=_F( TOUT = 'OUI',
                                  MODELISATION = 'POU_D_E',
                                  PHENOMENE = 'MECANIQUE'))

#
CARA=AFFE_CARA_ELEM(  MODELE=BARRE,
                           POUTRE=_F(
       GROUP_MA = 'TOUT',
   SECTION = 'CERCLE',
                                   CARA = 'R',     VALE = 0.1))

#
###
###### PARTIE GAUCHE :
###
#
GUIDAGE1=AFFE_CHAR_MECA(    MODELE=BARRE,DDL_IMPO=(
                           _F( TOUT = 'OUI',
                                     DX = 0., DZ = 0., DRX = 0., DRY = 0.),
                           _F( NOEUD = 'A',  DY = 0., DRZ = 0.)))

#
CHARGE1=AFFE_CHAR_MECA(    MODELE=BARRE,
                           DDL_IMPO=_F( NOEUD = 'N5',  DY = 0., DRZ = 0.))

#
K_ELEM1=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='RIGI_MECA',
                               CHARGE=(GUIDAGE1,  CHARGE1,))

#
M_ELEM1=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='MASS_MECA',
                               CHARGE=(GUIDAGE1,  CHARGE1,))

#
NUM1=NUME_DDL(  MATR_RIGI=K_ELEM1)

#
K_ASSE1=ASSE_MATRICE(  MATR_ELEM=K_ELEM1,
                           NUME_DDL=NUM1)

#
M_ASSE1=ASSE_MATRICE(  MATR_ELEM=M_ELEM1,
                           NUME_DDL=NUM1)

#
MODES1=MODE_ITER_SIMULT(     MATR_RIGI=K_ASSE1,
                                 MATR_MASS=M_ASSE1,
                             CALC_FREQ=_F( NMAX_FREQ = 04) )


#
INTERF1=DEFI_INTERF_DYNA(   NUME_DDL=NUM1,
                             INTERFACE=_F( NOM = 'DROITE',
                                        TYPE = 'CRAIGB',
                                        MASQUE = ('DX', 'DZ', 'DRX', 'DRY',),
                                        NOEUD = 'N5')
                              )

#
BAMO1=DEFI_BASE_MODALE( CLASSIQUE=_F( INTERF_DYNA = INTERF1,
                                        MODE_MECA = MODES1,
                                        NMAX_MODE = 04)
                             )

#
MACRO1=MACR_ELEM_DYNA(  BASE_MODALE=BAMO1,)

#
###
###### PARTIE DROITE :
###
#
GUIDAGE2=AFFE_CHAR_MECA(    MODELE=BARRE,
                           DDL_IMPO=_F( TOUT = 'OUI',
                                     DX = 0., DZ = 0., DRX = 0., DRY = 0.))

#
CHARGE2=AFFE_CHAR_MECA(    MODELE=BARRE,
                           DDL_IMPO=_F( NOEUD = 'A',  DY = 0., DRZ = 0.))

#
CHARTRAN=AFFE_CHAR_MECA(        MODELE=BARRE,
                           FORCE_NODALE=_F( NOEUD = 'N5',  FY = -1000.))

#
K_ELEM2=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='RIGI_MECA',
                               CHARGE=(GUIDAGE2,  CHARGE2,))

#
M_ELEM2=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='MASS_MECA',
                               CHARGE=(GUIDAGE2,  CHARGE2,))

#
V_ELEM=CALC_VECT_ELEM( OPTION='CHAR_MECA',
                            CHARGE=CHARTRAN)

#
NUM2=NUME_DDL(  MATR_RIGI=K_ELEM2)

#
K_ASSE2=ASSE_MATRICE(  MATR_ELEM=K_ELEM2,
                           NUME_DDL=NUM2)

#
M_ASSE2=ASSE_MATRICE(  MATR_ELEM=M_ELEM2,
                           NUME_DDL=NUM2)

#
V_ASSE=ASSE_VECTEUR(  VECT_ELEM=V_ELEM,
                           NUME_DDL=NUM2)

#
MODES2=MODE_ITER_SIMULT(     MATR_RIGI=K_ASSE2,
                                 MATR_MASS=M_ASSE2,
                             CALC_FREQ=_F( NMAX_FREQ = 05) )


#
INTERF2=DEFI_INTERF_DYNA(   NUME_DDL=NUM2,
                             INTERFACE=_F( NOM = 'GAUCHE',
                                        TYPE = 'CRAIGB',
                                        MASQUE = ('DX', 'DZ', 'DRX', 'DRY',),
                                        NOEUD = 'A')
                              )

#
BAMO2=DEFI_BASE_MODALE( CLASSIQUE=_F( INTERF_DYNA = INTERF2,
                                        MODE_MECA = MODES2,
                                        NMAX_MODE = 05)
                            )

#
MACRO2=MACR_ELEM_DYNA(  BASE_MODALE=BAMO2,)

#
###
###### MODELE GENERALISE :
###
#
MODEGE=DEFI_MODELE_GENE( SOUS_STRUC=(_F( NOM = 'COTE1',
                                         MACR_ELEM_DYNA = MACRO1,
                                         ANGL_NAUT = (0., 0., 0.,),
                                         TRANS=(0.,0.,0.),),
                             _F( NOM = 'COTE2',
                                         MACR_ELEM_DYNA = MACRO2,
                                         ANGL_NAUT = (180.,180.,180.,),
                                         TRANS=(0.5,0.,0.),)),
                                LIAISON=_F( SOUS_STRUC_1 = 'COTE1',
                                         SOUS_STRUC_2 = 'COTE2',
                                         INTERFACE_1 = 'DROITE',
                                         INTERFACE_2 = 'GAUCHE'))

#
NUMEGE=NUME_DDL_GENE(  MODELE_GENE=MODEGE)

#
K_GENE=ASSE_MATR_GENE(  NUME_DDL_GENE=NUMEGE,
                                  OPTION='RIGI_GENE')

#
M_GENE=ASSE_MATR_GENE(  NUME_DDL_GENE=NUMEGE,
                                  OPTION='MASS_GENE')

#
F_GENE=ASSE_VECT_GENE(    NUME_DDL_GENE=NUMEGE,
                           CHAR_SOUS_STRUC=_F( SOUS_STRUC = 'COTE2',
                                            VECT_ASSE = V_ASSE))

#
###
###### CALCUL MODAL :
###
#
RESGEN=MODE_ITER_SIMULT(     MATR_RIGI=K_GENE,
                                 MATR_MASS=M_GENE,
                             CALC_FREQ=_F( NMAX_FREQ = 5))


#
SQUEL=DEFI_SQUELETTE(  MODELE_GENE=MODEGE,SOUS_STRUC=(
                            _F( NOM = 'COTE1',
                                     GROUP_MA='TOUT'
                               ),
                            _F( NOM = 'COTE2',
                                     GROUP_MA='TOUT'
                                )))


DEBUG(SDVERI='OUI')
MODGLO=REST_SOUS_STRUC(   RESU_GENE=RESGEN, SQUELETTE=SQUEL, TOUT_ORDRE='OUI')
DEBUG(SDVERI='OUI')


NUMEMODE=NUME_DDL_GENE(BASE=RESGEN, STOCKAGE='DIAG')
K_PROJ=PROJ_MATR_BASE(BASE=RESGEN, NUME_DDL_GENE=NUMEMODE, MATR_ASSE_GENE=K_GENE)
M_PROJ=PROJ_MATR_BASE(BASE=RESGEN, NUME_DDL_GENE=NUMEMODE, MATR_ASSE_GENE=M_GENE)
F_PROJ=PROJ_VECT_BASE(BASE=RESGEN, NUME_DDL_GENE=NUMEMODE, VECT_ASSE_GENE=F_GENE)


# DEFINITION DE L OBSTACLE
OBSTACLE=DEFI_OBSTACLE( TYPE='CERCLE')

#
###
###### CALCULS TRANSITOIRES SUR BASE MODALE
###
#
TRAN_GE1=DYNA_VIBRA(TYPE_CALCUL='TRAN',BASE_CALCUL='GENE',
                             SCHEMA_TEMPS=_F(SCHEMA='EULER',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                                EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                            INCREMENT=_F(  INST_INIT = 0.,
                                        INST_FIN = 1.,
                                        PAS = 1.E-4),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N5',
                                       SOUS_STRUC_1 = 'COTE2',
                                       OBSTACLE = OBSTACLE,
                                       REPERE = 'COTE2',
                                       ORIG_OBST = (0.5, 0., 0.,),
                                       NORM_OBST = (1.,  0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                             )

#
TRAN_GE2=DYNA_VIBRA(TYPE_CALCUL='TRAN',BASE_CALCUL='GENE',
                             SCHEMA_TEMPS=_F(SCHEMA='DEVOGE',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                                EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                            INCREMENT=_F(  INST_INIT = 0.,
                                        INST_FIN = 1.,
                                        PAS = 1.E-4),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N5',
                                       SOUS_STRUC_1 = 'COTE2',
                                       OBSTACLE = OBSTACLE,
                                       ORIG_OBST = (1., 0., 0.,),
                                       NORM_OBST = (1., 0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                             )

#
TRAN_GE3=DYNA_VIBRA(TYPE_CALCUL='TRAN',BASE_CALCUL='GENE',
                             SCHEMA_TEMPS=_F(SCHEMA='ADAPT_ORDRE2',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                             EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                            INCREMENT=_F(  INST_INIT = 0.,
                                        INST_FIN = 1.,
# ON IMPOSE PAS_MAXI = PAS POUR RETROUVER LES ANCIENS RESULTATS
# POUR AMELIORER LE TEMPS CPU IL VAUT MIEUX TESTER AVEC UN PAS_MAXI PLUS GRAND
                                        PAS_MAXI = 1.E-4,
                                        PAS = 1.E-4),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N5',
                                       SOUS_STRUC_1 = 'COTE2',
                                       OBSTACLE = OBSTACLE,
                                       ORIG_OBST = (1., 0., 0.,),
                                       NORM_OBST = (1., 0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                             )

#
LIST_R=DEFI_LIST_REEL(       DEBUT=0.,
                           INTERVALLE=_F( JUSQU_A = 1.,
                                       NOMBRE = 10))

LIST_R2=DEFI_LIST_REEL(DEBUT=0.1, INTERVALLE=_F( JUSQU_A = 1., NOMBRE = 9))

#
DEBUG(SDVERI='OUI')

TRAN1=REST_GENE_PHYS(   RESU_GENE=TRAN_GE1,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN',
                             MODE_MECA=MODGLO )

#
TRAN2=REST_GENE_PHYS(   RESU_GENE=TRAN_GE2,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN',
                             MODE_MECA=MODGLO )

#
TRAN3=REST_GENE_PHYS(   RESU_GENE=TRAN_GE3,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN',
                             MODE_MECA=MODGLO )

DEBUG(SDVERI='OUI')

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='DEPL',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=-1.2548299999999999E-4,
                  TOLE_MACHINE=5.0000000000000001E-4,
                  CRITERE='RELATIF',
                  ),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='VITE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC= 8.28908211E-04,
                  VALE_REFE=8.3521300000000001E-4,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=1.E-2,),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='ACCE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=0.386958868,
                  VALE_REFE=0.363952,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=0.070000000000000007,),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='DEPL',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=-1.25437E-4,
                  TOLE_MACHINE=5.0000000000000001E-4,
                  CRITERE='RELATIF',
                  ),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='VITE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC= 8.31932263E-04,
                  VALE_REFE=8.4098500000000004E-4,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=0.014999999999999999,),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='ACCE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=0.307855088,
                  VALE_REFE=0.28545399999999999,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=0.080000000000000002,),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='DEPL',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=-1.25525E-4,
                  TOLE_MACHINE=5.0000000000000001E-4,
                  CRITERE='RELATIF',
                  ),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='VITE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC= 8.25849828E-04,
                  VALE_REFE=8.3285499999999997E-4,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=8.9999999999999993E-3,),
          )

#
TEST_RESU(RESU=_F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='ACCE',
                  NOEUD='NO12',
                  NOM_CMP='DY',
                  VALE_CALC=0.368281679,
                  VALE_REFE=0.34578100000000001,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=0.070000000000000007,),
          )

mod1=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                    SOUS_STRUC='COTE1',
                    TOUT_ORDRE='OUI');

resu_dy1=REST_GENE_PHYS(   RESU_GENE=TRAN_GE1,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN',
                             MODE_MECA=mod1 )

resu_dy=CALC_CHAMP(RESULTAT= resu_dy1,
                   MODELE=BARRE,CHAM_MATER=CHMAT,CARA_ELEM=CARA,
                    CONTRAINTE=('SIPM_ELNO'))


TEST_RESU(RESU=_F(INST=0.0,
                  REFERENCE='NON_DEFINI',
                  RESULTAT=resu_dy,
                  NOM_CHAM='SIPM_ELNO',
                  NOEUD='N1',
                  NOM_CMP='SIXX',
                  VALE_CALC=665.452033642,
                  VALE_REFE=665.452,
                  CRITERE='RELATIF',
                  PRECISION=8.9999999999999993E-3,
                  MAILLE='S1',
                  ),
          )

t_cin1=POST_ELEM(ENER_CIN=_F(GROUP_MA='TOUT',
                            ),
                MODELE=BARRE,
                CHAM_MATER=CHMAT,
                CARA_ELEM=CARA,
                RESULTAT=resu_dy1,
                LIST_INST=LIST_R2,
                );


TEST_TABLE(
           VALE_CALC=0.019380000000000001,
           TOLE_MACHINE=1.E-3,
           NOM_PARA='TOTALE',
           TABLE=t_cin1,
           FILTRE=(_F(NOM_PARA='LIEU',
                      VALE_K='TOUT',),
                   _F(NOM_PARA='INST',
                      VALE=1.0,),
                   ),
           )

mod2=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                    SOUS_STRUC='COTE2',
                    TOUT_ORDRE='OUI');

resu_dy2=REST_GENE_PHYS(   RESU_GENE=TRAN_GE1,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN',
                             MODE_MECA=mod2 )


t_cin2=POST_ELEM(ENER_CIN=_F(GROUP_MA='TOUT',
                            ),
                MODELE=BARRE,
                CHAM_MATER=CHMAT,
                CARA_ELEM=CARA,
                RESULTAT=resu_dy2,
                LIST_INST=LIST_R2,
                );


TEST_TABLE(
           VALE_CALC=0.049381399999999999,
           TOLE_MACHINE=1.E-3,
           NOM_PARA='TOTALE',
           TABLE=t_cin2,
           FILTRE=(_F(NOM_PARA='LIEU',
                      VALE_K='TOUT',),
                   _F(NOM_PARA='INST',
                      VALE=1.0,),
                   ),
           )

#
FIN()
#
