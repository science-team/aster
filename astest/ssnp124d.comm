# person_in_charge: romeo.fernandes at edf.fr
# ======================================================================
# TITRE ETUDE : ESSAI BIAXIAL DRAINE NON LOCAL
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#               AVEC LE MODELE DE TYPE DRUCKER-PRAGER A ECROUISSAGE
#               NEGATIF PARABOLIQUE POUR UN CONFINEMENT DE 2 MPA
# ======================================================================
DEBUT(CODE=_F(NOM='SSNP124D', NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

MAILLAGE = LIRE_MAILLAGE( );

MATER    = DEFI_MATERIAU(  ELAS    = _F(  E  = 5800.0E6,
                                          NU = 0.3  ,),
                           DRUCK_PRAGER = _F(  ECROUISSAGE = 'PARABOLIQUE',
                                          ALPHA       =    0.33      ,
                                          P_ULTM      =    0.01      ,
                                          SY          =    2.57E6    ,
                                          SY_ULTM     =    0.57E6    ,),
     NON_LOCAL= _F( LONG_CARA   =    1.0000 ,),
                        );

CHMAT    = AFFE_MATERIAU(  MAILLAGE = MAILLAGE,
                           AFFE     = _F(  TOUT  = 'OUI',
                           MATER = MATER )
                        );

MODELE   = AFFE_MODELE(    MAILLAGE = MAILLAGE,
                                               AFFE     = _F( TOUT='OUI' ,
                                                                     PHENOMENE    = 'MECANIQUE' ,
                                                                     MODELISATION = 'D_PLAN_GRAD_EPSI',),
                                            );

#***********************************************************************
# *** CHARGEMENT MECANIQUE : FORCE DE COMPRESSION + DDL IMPOSES ********
#***********************************************************************
SIGINIT=CREA_CHAMP(TYPE_CHAM='CART_SIEF_R',
                  OPERATION='AFFE',
                  MODELE=MODELE,
                  AFFE=_F(TOUT='OUI',
                          NOM_CMP=('SIXX','SIYY','SIZZ','SIXY','SIXZ','SIYZ','SIP','M11','FH11X','FH11Y',),
                          VALE=(-2.E6,-2.E6,-2.E6,0.0,0.0,0.0,0,0.0,0.0,0.0,),),);

CHAR_UM3 = AFFE_CHAR_MECA( MODELE   = MODELE,
                           PRES_REP = _F(  MAILLE = 'M3',
                                           PRES   = 2.0E6)
                        )

CHAR_UM4 = AFFE_CHAR_MECA( MODELE   = MODELE,
                           PRES_REP = _F(  MAILLE = 'M4',
                                           PRES = 2.0E6)
                        )

CHAR_UM5 = AFFE_CHAR_MECA( MODELE   = MODELE,
                           PRES_REP = _F(  MAILLE = 'M5',
                                           PRES = 2.0E6)
                        )

DEPL_M2  = AFFE_CHAR_MECA( MODELE   = MODELE,
                           DDL_IMPO = _F(  MAILLE = 'M2',
                                           DY     = 0.)
                        )

DEPL_M5  = AFFE_CHAR_MECA( MODELE   = MODELE,
                           DDL_IMPO = _F(  MAILLE = 'M5',
                                           DX     = 0.)
                         )

DEPL_M4  = AFFE_CHAR_MECA( MODELE   = MODELE,
                           FACE_IMPO= _F(  MAILLE = 'M4',
                                           DY = 1.)
                         )

COEF2    = DEFI_FONCTION(  NOM_PARA    = 'INST',
                           PROL_DROITE = 'CONSTANT',
                           PROL_GAUCHE = 'CONSTANT',
                           VALE=( 1.0,   0.000,
                                  2.0,  -0.015,    )
                        )

TEMPS    = DEFI_LIST_REEL( DEBUT       = 1.,
                           INTERVALLE  = (
                                           _F(  JUSQU_A = 2.000, NOMBRE = 100, ),),
                          )

U2       = STAT_NON_LINE(
                          MODELE     = MODELE,
                          CHAM_MATER = CHMAT,
                          EXCIT      = (
                                        _F( CHARGE        = CHAR_UM3,),
                                        _F( CHARGE        = DEPL_M5, ),
     _F( CHARGE        = DEPL_M2, ),
            _F( CHARGE        = DEPL_M4,
                                            FONC_MULT     = COEF2,),),
                         COMP_INCR   = _F( RELATION       = 'DRUCK_PRAGER'),
                         NEWTON      = _F( MATRICE        = 'TANGENTE',
                                           REAC_ITER      = 1,),
                         ETAT_INIT   = _F( SIGM           = SIGINIT,),
                         CONVERGENCE = _F( RESI_GLOB_RELA = 1.E-6,
                                           ITER_GLOB_MAXI = 50),
                         INCREMENT   = _F( LIST_INST      = TEMPS,)
                        )

U2=CALC_CHAMP(reuse=U2,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=U2)


U2       = CALC_CHAMP( reuse    = U2,
                    RESULTAT = U2,
                    CONTRAINTE='SIGM_NOEU',VARI_INTERNE='VARI_NOEU'
                  )

TEST_RESU(RESU=(_F(INST=2.0,
                   RESULTAT=U2,
                   NOM_CHAM='SIGM_NOEU',
                   NOEUD='N4',
                   NOM_CMP='SIXX',
                   VALE_CALC=-2.E6,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.0700000000000001,
                   RESULTAT=U2,
                   NOM_CHAM='SIGM_NOEU',
                   NOEUD='N4',
                   NOM_CMP='SIYY',
                   VALE_CALC=-8.6923076923076995E6,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.1599999999999999,
                   RESULTAT=U2,
                   NOM_CHAM='SIGM_NOEU',
                   NOEUD='N4',
                   NOM_CMP='SIYY',
                   VALE_CALC=-1.3730958677478001E7,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.3400000000000001,
                   RESULTAT=U2,
                   NOM_CHAM='SIGM_NOEU',
                   NOEUD='N4',
                   NOM_CMP='SIYY',
                   VALE_CALC=-9.9025376094531994E6,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.53,
                   RESULTAT=U2,
                   NOM_CHAM='SIGM_NOEU',
                   NOEUD='N4',
                   NOM_CMP='SIYY',
                   VALE_CALC=-9.9066954425514005E6,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.0700000000000001,
                   RESULTAT=U2,
                   NOM_CHAM='VARI_NOEU',
                   NOEUD='N4',
                   NOM_CMP='V1',
                   VALE_CALC=0.0,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='ABSOLU',
                   ),
                _F(INST=1.1599999999999999,
                   RESULTAT=U2,
                   NOM_CHAM='VARI_NOEU',
                   NOEUD='N4',
                   NOM_CMP='V1',
                   VALE_CALC=1.2614416568446001E-3,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.3400000000000001,
                   RESULTAT=U2,
                   NOM_CHAM='VARI_NOEU',
                   NOEUD='N4',
                   NOM_CMP='V1',
                   VALE_CALC=0.011235397672183,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.53,
                   RESULTAT=U2,
                   NOM_CHAM='VARI_NOEU',
                   NOEUD='N4',
                   NOM_CMP='V1',
                   VALE_CALC=0.020109688452757998,
                   TOLE_MACHINE=1.E-3,
                   ),
                _F(INST=1.0700000000000001,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=U2,
                   NOM_CHAM='DEPL',
                   NOEUD='N4',
                   NOM_CMP='DY',
                   VALE_CALC=-1.05000000E-03,
                   VALE_REFE=-1.0499999999999999E-3,
                   PRECISION=1.E-3,),
                _F(INST=1.1599999999999999,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=U2,
                   NOM_CHAM='DEPL',
                   NOEUD='N4',
                   NOM_CMP='DY',
                   VALE_CALC=-2.40000000E-03,
                   VALE_REFE=-2.3999999999999998E-3,
                   PRECISION=1.E-3,),
                _F(INST=1.3400000000000001,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=U2,
                   NOM_CHAM='DEPL',
                   NOEUD='N4',
                   NOM_CMP='DY',
                   VALE_CALC=-5.10000000E-03,
                   VALE_REFE=-5.1000000000000004E-3,
                   PRECISION=1.E-3,),
                _F(INST=1.53,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=U2,
                   NOM_CHAM='DEPL',
                   NOEUD='N4',
                   NOM_CMP='DY',
                   VALE_CALC=-7.95000000E-03,
                   VALE_REFE=-7.9500000000000005E-3,
                   PRECISION=1.E-3,),
                ),
          )

########################################################################
# --- DONNEES UTILES POUR IMPRESSION ----------------------------------#
########################################################################
#SIGMA    = POST_RELEVE_T( ACTION = _F( INTITULE  = 'SIGMA',
#                                       RESULTAT  =  U2,
#                                       NOEUD     = 'N4',
#                                       NOM_CHAM  = 'SIGM_ELNO',
#                                       TOUT_CMP  = 'OUI',
#                                       OPERATION = 'EXTRACTION',),);
#
#DEFORMAT = POST_RELEVE_T( ACTION = _F( INTITULE  = 'DEFORMATION',
#                                       RESULTAT  =  U2,
#                                       NOEUD     = 'N4',
#                                       NOM_CHAM  = 'VARI_ELNO',
#                                       TOUT_CMP  = 'OUI',
#                                       OPERATION = 'EXTRACTION',),);
#
#DEPLACEM = POST_RELEVE_T( ACTION = _F( INTITULE  = 'DEPLACEMENT',
#                                       RESULTAT  =  U2,
#                                       NOEUD     = 'N4',
#                                       NOM_CHAM  = 'DEPL',
#                                       TOUT_CMP  = 'OUI',
#                                       OPERATION = 'EXTRACTION',),);
#
#IMPR_TABLE(TABLE=SIGMA,   );
#IMPR_TABLE(TABLE=DEFORMAT,);
#IMPR_TABLE(TABLE=DEPLACEM,);
#######################################################################

FIN();
