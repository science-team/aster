
# TITRE : FLAMBEMENT D'UNE PLAQUE CIRCULAIRE SOUMISE A UNE
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#         FORCE DE COMPRESSION SUR SON CONTOUR
# AUTEUR : F. LEBOUVIER (DeltaCAD)
# DATE   : 2/03/04
# MODELISATION : AXIS
# MAILLAGE     : 1840 QUAD8, 6449 NOEUDS

DEBUT(CODE=_F(NOM='SSLV139B',NIV_PUB_WEB='INTERNET',),DEBUG=_F(SDVERI='OUI'))

MAIL=LIRE_MAILLAGE(FORMAT='MED',);

MAIL=DEFI_GROUP(reuse =MAIL,
                MAILLAGE=MAIL,
                CREA_GROUP_MA=_F(NOM='TOUT',
                                 TOUT='OUI',),);

MODMECA=AFFE_MODELE(MAILLAGE=MAIL,
                    AFFE=_F(GROUP_MA='TOUT',
                            PHENOMENE='MECANIQUE',
                            MODELISATION='AXIS',),);

MAIL=MODI_MAILLAGE(reuse =MAIL,
                   MAILLAGE=MAIL,
                   ORIE_PEAU_2D=_F(GROUP_MA='BC',),
                   );

MAT1=DEFI_MATERIAU(ELAS=_F(E=210000000000.0,
                           NU=0.3,
                           RHO=7800.0,),);

CHMAT=AFFE_MATERIAU(MAILLAGE=MAIL,
                    MODELE=MODMECA,
                    AFFE=_F(TOUT='OUI',
                            MATER=MAT1,),);
PCRIT = 1./(2.*pi);


CLS=AFFE_CHAR_MECA(MODELE=MODMECA,
                   DDL_IMPO=(_F(GROUP_MA='DA',
                                DX=0.0,),
                             _F(GROUP_MA='BC',
                                DY=0.0,),),);

CHAR=AFFE_CHAR_MECA(MODELE=MODMECA,
                    FORCE_CONTOUR=_F(GROUP_MA='BC',
                                     FX=PCRIT,),);

ELAS=MACRO_ELAS_MULT(MODELE=MODMECA,
                     CHAM_MATER=CHMAT,
                     CHAR_MECA_GLOBAL=CLS,
                     CAS_CHARGE=_F(MODE_FOURIER=0,
                                   TYPE_MODE='SYME',
                                   CHAR_MECA=CHAR,),);

SIG=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
               OPERATION='EXTR',
               RESULTAT=ELAS,
               NOM_CHAM='SIEF_ELGA',
               TYPE_MAXI='MINI',
               TYPE_RESU='VALE',);

MAT_RI_M=CALC_MATR_ELEM(OPTION='RIGI_MECA',
                        MODELE=MODMECA,
                        CHAM_MATER=CHMAT,
                        CHARGE=(CLS,CHAR,),);

MAT_RI_G=CALC_MATR_ELEM(OPTION='RIGI_GEOM',
                        MODELE=MODMECA,
                        SIEF_ELGA=SIG,);

MAT_MAS=CALC_MATR_ELEM(OPTION='MASS_MECA',
                       MODELE=MODMECA,
                       CHAM_MATER=CHMAT,);

NUM=NUME_DDL(MATR_RIGI=MAT_RI_M,);

MAS_RI_M=ASSE_MATRICE(MATR_ELEM=MAT_RI_M,
                      NUME_DDL=NUM,);

MAS_RI_G=ASSE_MATRICE(MATR_ELEM=MAT_RI_G,
                      NUME_DDL=NUM,);

MASMA=ASSE_MATRICE(MATR_ELEM=MAT_MAS,
                   NUME_DDL=NUM,);

RESULT=MODE_ITER_SIMULT(MATR_RIGI=MAS_RI_M,
                        MATR_RIGI_GEOM=MAS_RI_G,
                        METHODE='SORENSEN',
                        NMAX_ITER_SOREN=20,
                        TYPE_RESU='MODE_FLAMB',
                        CALC_CHAR_CRIT=_F(
                        OPTION='PLUS_PETITE',
                        NMAX_CHAR_CRIT=10,),
                        VERI_MODE=_F(SEUIL=0.0001,),);


MODE=MODE_ITER_SIMULT(MATR_RIGI=MAS_RI_M,
                      MATR_MASS=MASMA,
                      METHODE='SORENSEN',
                      NMAX_ITER_SOREN=20,
                      VERI_MODE=_F(SEUIL=0.0001,),);


MODE=CALC_CHAMP(reuse=MODE,RESULTAT=MODE,CONTRAINTE=('SIEF_ELGA'),ENERGIE=('ETOT_ELGA','ETOT_ELNO','ETOT_ELEM'))


DEPL=CREA_CHAMP(TYPE_CHAM='NOEU_DEPL_R',
                OPERATION='EXTR',
                RESULTAT=MODE,
                NOM_CHAM='DEPL',
                NUME_ORDRE=1,);

EVOLELAS=CREA_RESU(OPERATION='AFFE',
                   TYPE_RESU='EVOL_ELAS',
                   NOM_CHAM='DEPL',
                   AFFE=_F(
                   CHAM_GD=DEPL,
                   MODELE=MODMECA,
                   CHAM_MATER=CHMAT,
                   INST=0.0,),);

EVOLELAS=CALC_CHAMP(reuse=EVOLELAS,RESULTAT=EVOLELAS,CONTRAINTE=('SIEF_ELGA'),ENERGIE=('ETOT_ELGA','ETOT_ELNO','ETOT_ELEM'))


RES_NORM=NORM_MODE(MODE=RESULT,
                   NORME='TRAN',);

CHA_DEPL=CREA_CHAMP(TYPE_CHAM='NOEU_DEPL_R',
                    OPERATION='EXTR',
                    RESULTAT=RESULT,
                    NOM_CHAM='DEPL',
                    NUME_MODE=1,);

MODE_1=CREA_CHAMP(TYPE_CHAM='NOEU_DEPL_R',
                  OPERATION='EXTR',
                  RESULTAT=RES_NORM,
                  NOM_CHAM='DEPL',
                  NUME_ORDRE=1,);

TEST_RESU(RESU=_F(PARA='CHAR_CRIT',
                  NUME_MODE=1,
                  REFERENCE='ANALYTIQUE',
                  RESULTAT=RES_NORM,
                  VALE_CALC=8.48470464E+05,
                  VALE_REFE=8.4935093960000004E5,
                  PRECISION=2.E-3,),
          CHAM_NO=_F(NOEUD='N4',
                     REFERENCE='ANALYTIQUE',
                     NOM_CMP='DY',
                     CHAM_GD=MODE_1,
                     VALE_CALC=0.999985449,
                     VALE_REFE=1.0,),
          )

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   POINT=1,
                   RESULTAT=EVOLELAS,
                   NOM_CHAM='ETOT_ELEM',
                   NOM_CMP='TOTALE',
                   VALE_CALC=0.066948111485693004,
                   TOLE_MACHINE=1.E-3,
                   MAILLE='M20',
                   ),
                _F(NUME_ORDRE=1,
                   POINT=1,
                   RESULTAT=EVOLELAS,
                   NOM_CHAM='ETOT_ELGA',
                   NOM_CMP='TOTALE',
                   VALE_CALC=6.7300645536777997E8,
                   TOLE_MACHINE=1.E-3,
                   MAILLE='M10',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=EVOLELAS,
                   NOM_CHAM='ETOT_ELNO',
                   NOEUD='N10',
                   NOM_CMP='TOTALE',
                   VALE_CALC=7.2802885013540006E8,
                   TOLE_MACHINE=1.E-3,
                   MAILLE='M3',
                   ),
                ),
          )

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   REFERENCE='AUTRE_ASTER',
                   POINT=1,
                   RESULTAT=MODE,
                   NOM_CHAM='ETOT_ELEM',
                   NOM_CMP='TOTALE',
                   VALE_CALC=0.066948134,
                   VALE_REFE=0.066948111485693004,
                   MAILLE='M20',),
                _F(NUME_ORDRE=1,
                   REFERENCE='AUTRE_ASTER',
                   POINT=1,
                   RESULTAT=MODE,
                   NOM_CHAM='ETOT_ELGA',
                   NOM_CMP='TOTALE',
                   VALE_CALC= 6.73006699E+08,
                   VALE_REFE=6.7300645536777997E8,
                   MAILLE='M10',),
                _F(NUME_ORDRE=1,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=MODE,
                   NOM_CHAM='ETOT_ELNO',
                   NOEUD='N10',
                   NOM_CMP='TOTALE',
                   VALE_CALC= 7.28029113E+08,
                   VALE_REFE=7.2802885013540006E8,
                   MAILLE='M3',),
                ),
          )

FIN();
