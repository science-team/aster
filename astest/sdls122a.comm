# person_in_charge: mathieu.corus at edf.fr
# TITRE ANALYSE MODALE D'UNE PLAQUE APPUYEE SUR SES COINS : SOUS-STRUCTURATION
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
DEBUT(CODE=_F(NOM='SDLS122A', NIV_PUB_WEB='INTERNET'),
      DEBUG=_F(SDVERI='NON'));

PRE_IDEAS(UNITE_MAILLAGE=20,
          UNITE_IDEAS=19,
          );
          
PRE_IDEAS(UNITE_MAILLAGE=30,
          UNITE_IDEAS=29,
          );         

PRE_IDEAS(UNITE_MAILLAGE=40,
          UNITE_IDEAS=39,
          );         

#-- ON CHERCHE ICI A CALCULER DE 2 FACONS DIFFERENTES LES MODES D'UNE PLAQUE 
#-- D'ACIER RECTANGULAIRE DE 2m x 1m x 1cm APPUYEE SUR SES 4 COINS. ON COMPARE
#-- UNE METHODE DE SOUS STRUCTURATION DYNAMIQUE, EN CONSIDERANT 2 PLAQUES CARREES,
#-- MAILLEES AVEC DES FINESSES DIFFERENTES (15x15 ELEMENTS POUR LA PREMIERE, 14x14
#-- POUR LA SECONDE) ET UNE METHODE DIRECTE S'APPUYANT SUR UN MAILLAGE FIN DE LA 
#-- PLAQUE COMPRENANT 20x40 ELEMENTS


#------------------------------------------------------#
#--                                                  --#
#-- DEFINITION D'UN PREMIER CALCUL SUR MODELE REDUIT --#
#--     ET ASSSEMBLAGE DU MODELE EF DE REFERENCE     --#
#--                                                  --#
#------------------------------------------------------#

#-- MAILLAGES DES DEUX PARTIES CARREES
MAILLA1=LIRE_MAILLAGE(UNITE=20,FORMAT='ASTER',)
MAILLA2=LIRE_MAILLAGE(UNITE=30,FORMAT='ASTER',)

#-- MAILLAGE DU RECTANGLE COMPLET
MAILLA3=LIRE_MAILLAGE(UNITE=40,FORMAT='ASTER',)


#-- CONSTRUCTION DES MODELES
MODELE1=AFFE_MODELE(MAILLAGE=MAILLA1,
                    AFFE=_F( GROUP_MA = 'CARRE1',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'DKT'),);

MODELE2=AFFE_MODELE(MAILLAGE=MAILLA2,
                    AFFE=_F( GROUP_MA = 'CARRE2',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'DKT'),);

MODELE3=AFFE_MODELE(MAILLAGE=MAILLA3,
                    AFFE=_F( GROUP_MA = 'RECT',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'DKT'),);
                             
#-- DEFINITION DU MATERIAU
MATER=DEFI_MATERIAU(  ELAS=_F( E = 2.1E11,  NU = 0.30,  RHO = 7800.0))

#-- AFFECTATION DES MATERIAUX
CHAMAT1=AFFE_MATERIAU(  MAILLAGE=MAILLA1,
                       AFFE=_F( GROUP_MA = 'CARRE1',
                                    MATER = MATER))
CHAMAT2=AFFE_MATERIAU(  MAILLAGE=MAILLA2,
                       AFFE=_F( GROUP_MA = 'CARRE2',
                                    MATER = MATER))   

CHAMAT3=AFFE_MATERIAU(  MAILLAGE=MAILLA3,
                       AFFE=_F( GROUP_MA = 'RECT',
                                    MATER = MATER))   
                                                                     
#-- DEFINITION DES CARACTERISTIQUES GEOMETRIQUES
PARAM1=AFFE_CARA_ELEM(  MODELE=MODELE1,
                            COQUE=_F( EPAIS = 0.01,
                                   GROUP_MA = 'CARRE1'))
PARAM2=AFFE_CARA_ELEM(  MODELE=MODELE2,
                            COQUE=_F( EPAIS = 0.01,
                                   GROUP_MA = 'CARRE2'))
PARAM3=AFFE_CARA_ELEM(  MODELE=MODELE3,
                            COQUE=_F( EPAIS = 0.01,
                                   GROUP_MA = 'RECT'))
                                   
#-- DEFINITION DES CONDITIONS AUX LIMITES                                   
CHARGE_1=AFFE_CHAR_MECA(MODELE=MODELE1,
                        DDL_IMPO=(
                                  _F( GROUP_NO = 'BC1', 
                                      DX = 0.0,
                                      DY = 0.0,
                                      DZ = 0.0,),
                                  _F( GROUP_NO = 'INTF1',
                                      DX = 0.0,
                                      DY = 0.0,
                                      DZ = 0.0,
                                      DRX = 0.0,
                                      DRY = 0.0,
                                      DRZ = 0.0),
                                   ),)


CHARGE_2=AFFE_CHAR_MECA(MODELE=MODELE2,
                        DDL_IMPO=(
                                  _F( GROUP_NO = 'BC2', 
                                      DX = 0.0,
                                      DY = 0.0,
                                      DZ = 0.0,),
                                  _F( GROUP_NO = 'INTF2',
                                      DX = 0.0,
                                      DY = 0.0,
                                      DZ = 0.0,
                                      DRX = 0.0,
                                      DRY = 0.0,
                                      DRZ = 0.0),
                                   ),)
                                   
CHARGE_3=AFFE_CHAR_MECA(MODELE=MODELE3,
                        DDL_IMPO=(_F( GROUP_NO = 'BC', 
                                      DX = 0.0,
                                      DY = 0.0,
                                      DZ = 0.0,),
                                   ),)
#-- ASSEMBLAGE DES MATRICES
ASSEMBLAGE(MODELE=MODELE1,
                CARA_ELEM=PARAM1,
                NUME_DDL=CO("NUME1"),
                CHARGE=CHARGE_1,
                CHAM_MATER=CHAMAT1,
                MATR_ASSE=(_F(  MATRICE = CO("RAID1"),  OPTION = 'RIGI_MECA'),
                           _F(  MATRICE = CO("MASS1"),  OPTION = 'MASS_MECA'))
                 )
                 
ASSEMBLAGE(MODELE=MODELE2,
                CARA_ELEM=PARAM2,
                NUME_DDL=CO("NUME2"),
                CHARGE=CHARGE_2,
                CHAM_MATER=CHAMAT2,
                MATR_ASSE=(_F(  MATRICE = CO("RAID2"),  OPTION = 'RIGI_MECA'),
                           _F(  MATRICE = CO("MASS2"),  OPTION = 'MASS_MECA'))
                 )

ASSEMBLAGE(MODELE=MODELE3,
                CARA_ELEM=PARAM3,
                NUME_DDL=CO("NUME3"),
                CHARGE=CHARGE_3,
                CHAM_MATER=CHAMAT3,
                MATR_ASSE=(_F(  MATRICE = CO("RAID3"),  OPTION = 'RIGI_MECA'),
                           _F(  MATRICE = CO("MASS3"),  OPTION = 'MASS_MECA'))
                 )

#-- CALCULS DES MODES AVEC LES C.L. ASSOCIEES AUX TYPES D'INTERFACE              
MODES_1=MODE_ITER_SIMULT(MATR_RIGI=RAID1,
                         MATR_MASS=MASS1,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F(OPTION='PLUS_PETITE',
                                      NMAX_FREQ=1,),
                         SOLVEUR=_F(RENUM='AUTO',
                                      METHODE='MUMPS',),             
                         VERI_MODE=_F(STOP_ERREUR='NON',
                                      STURM='NON',),);

MODES_2=MODE_ITER_SIMULT(MATR_RIGI=RAID2,
                         MATR_MASS=MASS2,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F(OPTION='PLUS_PETITE',
                                      NMAX_FREQ=1,),
                         SOLVEUR=_F(RENUM='AUTO',
                                      METHODE='MUMPS',),                                       
                         VERI_MODE=_F(STOP_ERREUR='NON',
                                      STURM='NON',),);

MODES_3=MODE_ITER_SIMULT(MATR_RIGI=RAID3,
                         MATR_MASS=MASS3,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F(OPTION='PLUS_PETITE',
                                      NMAX_FREQ=5,),
                         SOLVEUR=_F(RENUM='AUTO',
                                      METHODE='MUMPS',),                                       
                         VERI_MODE=_F(STOP_ERREUR='NON',
                                      STURM='NON',),);

#-- TEST DE L'ANALYSE MODALE DE LA PLAQUE COMPLETE
TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=MODES_3,
                   VALE_CALC=5.806,
                   TOLE_MACHINE=1.1E-4,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   RESULTAT=MODES_3,
                   VALE_CALC=17.175000000000001,
                   TOLE_MACHINE=1.1E-4,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   RESULTAT=MODES_3,
                   VALE_CALC=20.515999999999998,
                   TOLE_MACHINE=1.1E-4,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   RESULTAT=MODES_3,
                   VALE_CALC=32.421999999999997,
                   TOLE_MACHINE=1.1E-4,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=MODES_3,
                   VALE_CALC=39.844999999999999,
                   TOLE_MACHINE=1.1E-4,
                   CRITERE='RELATIF',
                   ),
                ),
          )

#--  DEFINITION DES INTERFACES
LINT1_CL=DEFI_INTERF_DYNA(NUME_DDL=NUME1,
                          INTERFACE=_F(NOM='INTF1',
                                       TYPE='CRAIGB',
                                       GROUP_NO='INTF1',),);

LINT2_CL=DEFI_INTERF_DYNA(NUME_DDL=NUME2,
                          INTERFACE=_F(NOM='INTF2',
                                       TYPE='CRAIGB',
                                       GROUP_NO='INTF2',),);

#-- CALCUL DES MODES D'INTERFACE
MODESTA1=MODE_STATIQUE(MATR_RIGI=RAID1,
                       MATR_MASS=MASS1,
                       MODE_INTERF=_F(GROUP_NO=('INTF1'),
                                      TOUT_CMP='OUI',
                                      NBMOD=6,
                                      ),
                      );

MODESTA2=MODE_STATIQUE(MATR_RIGI=RAID2,
                       MATR_MASS=MASS2,
                       MODE_INTERF=_F(GROUP_NO=('INTF2'),
                                      TOUT_CMP='OUI',
                                      NBMOD=6,
                                      ),
                      );

#-- CONSTRUCTION DES BASES : 1 MODE A INTERFACE FIXE / 6 MODES D'INTERFACE
BAMO1_CL=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODES_1,
                                NMAX_MODE=1,),
                             _F(MODE_INTF=MODESTA1,
                                NMAX_MODE=6,),),
                       INTERF_DYNA=LINT1_CL,
                       NUME_REF=NUME1,
                       );
                       
BAMO2_CL=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODES_2,
                                NMAX_MODE=1,),
                             _F(MODE_INTF=MODESTA2,
                                NMAX_MODE=6,),),
                       INTERF_DYNA=LINT2_CL,
                       NUME_REF=NUME2,
                       );


#-- CONSTRUCTION DES MACRO-ELEMENTS
MACEL1=MACR_ELEM_DYNA(BASE_MODALE=BAMO1_CL,
                      MATR_RIGI=RAID1,
                      MATR_MASS=MASS1,);

MACEL2=MACR_ELEM_DYNA(BASE_MODALE=BAMO2_CL,
                      MATR_RIGI=RAID2,
                      MATR_MASS=MASS2,);

#--  ASSEMBLAGE DU MODELE GENERALISE                     
ASSE_ELEM_SSD(RESU_ASSE_SSD = _F(MODELE = CO('MODEGE'),
                                 NUME_DDL_GENE = CO('NUMEGE'),
                                 RIGI_GENE = CO('RIGGEN'),
                                 MASS_GENE = CO('MASGEN'),
                                 ),
              SOUS_STRUC = (_F(NOM = 'CARRE1',
                               MACR_ELEM_DYNA = MACEL1,
                               TRANS = (0.,0.0,0.),
                               ANGL_NAUT = (0.,0.,0.),),
                            _F(NOM = 'CARRE2',
                               MACR_ELEM_DYNA = MACEL2,
                               TRANS = (0.,0.0,0.),
                               ANGL_NAUT = (0.,0.,0.),),
                          ),
              LIAISON = (_F(SOUS_STRUC_1 = 'CARRE1',
                            INTERFACE_1 = 'INTF1',

                            SOUS_STRUC_2 = 'CARRE2',
                            INTERFACE_2 = 'INTF2',

                            GROUP_MA_MAIT_2 = 'CARRE2',
                            OPTION = 'REDUIT',
                           ),
                        ),
              VERIF = _F( STOP_ERREUR = 'OUI',
                          PRECISION = 1.E-6,
                          CRITERE = 'RELATIF'),
              METHODE = 'ELIMINE',
               ),

#-- CALCUL DES MODES DU MODELE REDUIT
RESGEN=MODE_ITER_SIMULT( MATR_RIGI=RIGGEN,
                         MATR_MASS=MASGEN,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F( OPTION = 'PLUS_PETITE',
                                       NMAX_FREQ=5,),
                         VERI_MODE=_F(PREC_SHIFT=5.0000000000000001E-3,
                                      STOP_ERREUR='NON',
                                      STURM='NON',
                                      SEUIL=9.9999999999999995E-01),
                         STOP_BANDE_VIDE='NON',
                        )

#-- TEST DES RESULTATS
#--   AVEC LA BASE AINSI DEFINIE, LES 4 PREMIERS MODES SONT CORECTS
#--                               LE 5EME EST MAL CALCULE
TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=RESGEN,
                   VALE_CALC=5.821571445,
                   VALE_REFE=5.806,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.050999999999999997,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   RESULTAT=RESGEN,
                   VALE_CALC=17.753058291,
                   VALE_REFE=17.175000000000001,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.11,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   RESULTAT=RESGEN,
                   VALE_CALC=20.646431122,
                   VALE_REFE=20.515999999999998,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.050999999999999997,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   RESULTAT=RESGEN,
                   VALE_CALC=33.640175288,
                   VALE_REFE=32.421999999999997,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.11,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=RESGEN,
                   VALE_CALC=67.566865120,
                   VALE_REFE=39.844999999999999,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=1.51,),
                ),
          )

#-- PARTIE RESTITUTION : NON TESTEE, JUSTE LA POUR L'EXEMPLE
#--   EN PARTICULIER, EVITER L'UTILISATION DE DEFI_SQUELETTE AVEC
#--   TOUT='OUI'. IL EST PREFERABLE D'INDIQUER lES GROUPES DE MAILLE
#--   SUR LESQUELS ON RESTITUE. C'EST PLUS EFFICACE, ET LE RESULTAT
#--   EST MOINS GROS
           
#-- CREATION DU MAILLAGE SQUELETTE DE LA STRUCTURE GLOBALE
SQUEL=DEFI_SQUELETTE(  MODELE_GENE=MODEGE,SOUS_STRUC=(
                            _F( NOM = 'CARRE1',
                                GROUP_MA = 'CARRE1',
                              ),
                            _F( NOM = 'CARRE2',
                                GROUP_MA = 'CARRE2',
                                ))
                                )
                                
#-- RESTITUTION SUR MAILLAGE SQUELETTE
MODGLO=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                       SQUELETTE=SQUEL,
                       TOUT_ORDRE='OUI',
                       TOUT_CHAM='OUI')

#-- CALCUL DES TRAVAUX ET DES CORRECTIONS ASSOCIEES 
CALC_ENR=CALC_CORR_SSD(MODELE_GENE=MODEGE,
                     RESU_GENE=RESGEN,
                     SHIFT=1.,
                     UNITE=6),

#---------------------------------------------------------------------#
#--                                                                 --#
#-- ENRICHISSEMENT DES MACROS ELEMENTS EN UTILISANT LES CORRECTIONS --#
#--                    CALCULEES AVEC CALC_CORR_SSD                 --#
#--                                                                 --#
#---------------------------------------------------------------------#


#-- MENAGE DANS LES CONCEPTS PRECEDENTS--#
DETRUIRE(CONCEPT=(_F(NOM=MACEL1,),
                  _F(NOM=MACEL2,),
                  _F(NOM=MODEGE,),
                  _F(NOM=RESGEN,),
                  _F(NOM=NUMEGE,),
                  _F(NOM=MASGEN,),
                  _F(NOM=RIGGEN,),
                  _F(NOM=BAMO1_CL,),
                  _F(NOM=BAMO2_CL,),
                  ),);


#-- EXTRACTION DES VECTEURS DE CORRECTIONS

#--   POUR LA SOUS STRUCTURE 1
S1_LIBRE=EXTR_TABLE(TABLE=CALC_ENR,
                    TYPE_RESU='MODE_MECA',
                    NOM_PARA='NOM_SD',
                    FILTRE=(_F(NOM_PARA='NOM_SST',
                              VALE_K='CARRE1',),
                            _F(NOM_PARA='INTERF',
                              VALE_K='LIBRE',),),);
                              
S1_ENCAS=EXTR_TABLE(TABLE=CALC_ENR,
                    TYPE_RESU='MODE_MECA',
                    NOM_PARA='NOM_SD',
                    FILTRE=(_F(NOM_PARA='NOM_SST',
                              VALE_K='CARRE1',),
                            _F(NOM_PARA='INTERF',
                              VALE_K='ENCAS',),),);


#--   POUR LA SOUS STRUCTURE 2
S2_LIBRE=EXTR_TABLE(TABLE=CALC_ENR,
                    TYPE_RESU='MODE_MECA',
                    NOM_PARA='NOM_SD',
                    FILTRE=(_F(NOM_PARA='NOM_SST',
                              VALE_K='CARRE2',),
                            _F(NOM_PARA='INTERF',
                              VALE_K='LIBRE',),),);
S2_ENCAS=EXTR_TABLE(TABLE=CALC_ENR,
                    TYPE_RESU='MODE_MECA',
                    NOM_PARA='NOM_SD',
                    FILTRE=(_F(NOM_PARA='NOM_SST',
                              VALE_K='CARRE2',),
                            _F(NOM_PARA='INTERF',
                              VALE_K='ENCAS',),),);
                              
#-- ENRICHISSEMENTS DES BASES MODALES 
#--
#--            /!\ /!\ /!\ ATTENTION /!\ /!\ /!\
#-- IL FAUT FAIRE ATTENTION DE TRAITER D'UNE PART LES MODES A INTERFACE 
#-- LIBRE, ET, DE L'AUTRE, LES MODES A INTERFACE FIXE POUR ASSURER UN
#-- BON RECOLLEMENT AUX INTERFACES. ON ORTHONORMALISE D'ABORD UNE FAMILLE,
#-- PUIS L'AUTRE, ET ON CONCATENE LES DEUX BASES.

#-- ORTHONORMALISATION DES FAMILLES DES VECTEURS A INTERFACE LIBRE                                         
LIB_S1=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODESTA1,
                                NMAX_MODE=6,),
                             _F(MODE_INTF=S1_LIBRE,
                                NMAX_MODE=4,),
                              ), 
                        INTERF_DYNA=LINT1_CL,
                        NUME_REF=NUME1,
                        ORTHO='OUI',
                        MATRICE=MASS1,
                        );
                        
LIB_S2=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODESTA2,
                                NMAX_MODE=6,),
                             _F(MODE_INTF=S2_LIBRE,
                                NMAX_MODE=4,),
                              ), 
                        INTERF_DYNA=LINT2_CL,
                        NUME_REF=NUME2,
                        ORTHO='OUI',
                        MATRICE=MASS2,
                        );

                        
#-- ORTHONORMALISATION DES FAMILLES DES VECTEURS A INTERFACE FIXE                        
ENC_S1=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODES_1,
                                NMAX_MODE=999,),
                             _F(MODE_INTF=S1_ENCAS,
                                NMAX_MODE=999,),
                              ), 
                        INTERF_DYNA=LINT1_CL,
                        NUME_REF=NUME1,
                        ORTHO='OUI',
                        MATRICE=MASS1,
                        );
                        
ENC_S2=DEFI_BASE_MODALE(RITZ=(_F(MODE_MECA=MODES_2,
                                NMAX_MODE=999,),
                             _F(MODE_INTF=S2_ENCAS,
                                NMAX_MODE=999,),
                              ), 
                        INTERF_DYNA=LINT2_CL,
                        NUME_REF=NUME2,
                        ORTHO='OUI',
                        MATRICE=MASS2,
                        );

#-- CONCATENATION DES DIFFERENTES FAMILLES
BAMO_S1=DEFI_BASE_MODALE(RITZ=(_F(BASE_MODALE=LIB_S1,
                                NMAX_MODE=999,),
                             _F(MODE_INTF=ENC_S1,
                                NMAX_MODE=999,),
                              ), 
                        INTERF_DYNA=LINT1_CL,
                        NUME_REF=NUME1,
                        );

                        
                              
BAMO_S2=DEFI_BASE_MODALE(RITZ=(_F(BASE_MODALE=LIB_S2,
                                  NMAX_MODE=999,),
                             _F(MODE_INTF=ENC_S2,
                                NMAX_MODE=999,),
                              ), 
                        INTERF_DYNA=LINT2_CL,
                        NUME_REF=NUME2,
                        );

#-- CONSTRUCTION DES NOUVEAUX MACROS ELEMENTS
MACR1=MACR_ELEM_DYNA(BASE_MODALE=BAMO_S1,
                      MATR_RIGI=RAID1,
                      MATR_MASS=MASS1,);                      

MACR2=MACR_ELEM_DYNA(BASE_MODALE=BAMO_S2,
                      MATR_RIGI=RAID2,
                      MATR_MASS=MASS2,);
                      
#-- DEFINITION D'UN NOUVEAU MODELE GENERALISE  / ANCIENNE SYNTAXE 
#--               ALTERNATIVE A ASSE_ELEM_SSD
MODEGE2=DEFI_MODELE_GENE(SOUS_STRUC=(_F(NOM='SS1',
                                       MACR_ELEM_DYNA=MACR1,
                                       ANGL_NAUT=(0.,0.,0.,),
                                       TRANS=(0.,0.,0.,),),
                                    _F(NOM='SS2',
                                       MACR_ELEM_DYNA=MACR2,
                                       ANGL_NAUT=(0.,0.,0.,),
                                       TRANS=(0.,0.,0.,),),
                                    ),
                        LIAISON=(_F(SOUS_STRUC_1='SS1',INTERFACE_1='INTF1',
                                    SOUS_STRUC_2='SS2',INTERFACE_2='INTF2',
                                    
                                    GROUP_MA_MAIT_2='CARRE2',
                                    
                                    OPTION='REDUIT',),
                                 ),
                        VERIF=_F(STOP_ERREUR='OUI',),);

#--  NUMEROTATION DU PROBLEME GENERALISE
NUM_G2=NUME_DDL_GENE(MODELE_GENE=MODEGE2,
                     METHODE='ELIMINE',);

#--  ASSEMBLAGE DES MATRICES RAIDEUR ET MASSE GENERALISES
M_G2=ASSE_MATR_GENE(NUME_DDL_GENE=NUM_G2,
                      OPTION='MASS_GENE',);

K_G2=ASSE_MATR_GENE(NUME_DDL_GENE=NUM_G2,
                      OPTION='RIGI_GENE',);

#-- RESOLUTION DU PROBLEME GENERALISE
RESGEN2=MODE_ITER_SIMULT( MATR_RIGI=K_G2,
                         MATR_MASS=M_G2,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F( OPTION = 'PLUS_PETITE',
                                       NMAX_FREQ=5,),
                         VERI_MODE=_F(PREC_SHIFT=5.0000000000000001E-3,
                                      STOP_ERREUR='NON',
                                      STURM='NON',
                                      SEUIL=9.9999999999999995E-01),
                         STOP_BANDE_VIDE='NON',
                        )

#-- CALCUL DES INDICATEURS DE QUALITE
INDIC=CALC_CORR_SSD(MODELE_GENE=MODEGE2,
                     RESU_GENE=RESGEN2,
                     SHIFT=1.,
                     UNITE=6),

#-- TEST DE LA QUALITE DU NOUVEAU MODELE REDUIT PAR RAPPORT AU MODELE DE REFERENCE
TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=RESGEN2,
                   VALE_CALC=5.813938725,
                   VALE_REFE=5.806,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-2,      #TODO variabilite
                   PRECISION=5.1000000000000004E-3,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   RESULTAT=RESGEN2,
                   VALE_CALC=17.174810905,
                   VALE_REFE=17.175000000000001,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-2,
                   PRECISION=1.1000000000000001E-3,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   RESULTAT=RESGEN2,
                   VALE_CALC=20.528005155,
                   VALE_REFE=20.515999999999998,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-2,
                   PRECISION=2.0999999999999999E-3,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   RESULTAT=RESGEN2,
                   VALE_CALC=32.459064597,
                   VALE_REFE=32.421999999999997,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-2,      #TODO variabilite
                   PRECISION=5.1000000000000004E-3,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=RESGEN2,
                   VALE_CALC=40.819099773,
                   VALE_REFE=39.844999999999999,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-2,      #TODO variabilite
                   PRECISION=0.050999999999999997,),
                ),
          )

#-- TESTER EGALEMENT UN DIMINUTION DE LA VALEUR DES INDICATEURS                        
FIN();
