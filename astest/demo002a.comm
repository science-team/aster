#
# TITRE AILETTE FISSUREE AVEC CONTACT
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: mathieu.courtois at edf.fr
import aster_core

# ILLUSTRATION  DE DYNA_NON_LINE ET DE POST_TRAITEMENT NUMERIQUE
# TRACE DE COURBE AVEC XMGRACE

DEBUT(CODE=_F(NOM='DEMO002A', NIV_PUB_WEB='INTERNET', VISU_EFICAS='NON'), PAR_LOT='NON', DEBUG=_F(SDVERI='OUI'))

##############################################################################################
# Ce fichier demontre les possibilites de couplage entre Python (et tous ses outils annexes)
# et Code_Aster
##############################################################################################

##############################################################################################
# Outils Python utilises
#  - numpy: module permettant des operations numeriques de haut niveau.
#     Dans cet exemple, on utilise les transformeee de Fourier rapides (FFT)
#     integres dans le package
#  - XMGRACE: module permettant de creer des graphiques
##############################################################################################

##############################################################################################
#
# Ce fichier de commande genere quatre types de resultats:
# 1/ Le fichier resu standard d'ASTER (ailette.resu)
# 2/ Un fichier post-processing au format 'universel' I-DEAS (ailette.unv)
# 3/ Un fichier post-processing au format GMSH (le nom doit etre precise dans la variable
#     <fichier_GMSH>)
# 4/ Trois courbes au format XMGRACE
#    Il faut noter que la troisieme courbe XMGRACE permet de generer un fichier postscript
#
### GMSH
#
# On peut choisir de lancer GMSH en mode interactif (a preciser par la variable
# <launch_GMSH>). Dans ce cas, ne pas oublier de renseigner la variable <executable_GMSH> sur
# l'emplacement de l'executable GMSH.
#
### XMGRACE
#
# On peut aussi demander a executer XMGRACE en mode interactif (variable <launch_XMGRACE>)
# Le fichier postscript sera genere independamment de la variable <launch_XMGRACE>.
#
##############################################################################################

##############################################################################################
#
# DEBUT
#
##############################################################################################

#=============================================================================================
# Variables globales
#=============================================================================================

# Executer GMSH: 1=OUI, 0=NON
launch_GMSH      = 0

# Emplacement de l'executable gmsh
# executable_GMSH  = "/home/mabbas/GMSH/gmsh-batch"
import os
executable_GMSH  = os.path.join(aster_core.get_option('repout'), 'gmsh')

# Executer XMGRACE en interactif: 1=OUI, 0=NON
launch_XMGRACE   = 0

#=============================================================================================
# Unites logiques
#=============================================================================================
# Unite logique pour sortie POSTSCRIPT
unite_PS       = 41

# Unite logique pour sortie GMSH
unite_GMSH       = 37

# Nom du fichier GMSH
fichier_GMSH     = "depl.pos"

#=============================================================================================
# Importation des modules Python
#=============================================================================================

import numpy as NP
import numpy.fft as FFT

##############################################################################################
#
# ETUDE ASTER
#
##############################################################################################

#=============================================================================================
# Preparation du calcul
#=============================================================================================

# Lecture du maillage
MA=LIRE_MAILLAGE(FORMAT='MED', );

# Re-orientation du maillage de peau
MA=MODI_MAILLAGE(reuse =MA,
                 MAILLAGE=MA,
                 ORIE_PEAU_2D=(_F(GROUP_MA='L3',),
                               _F(GROUP_MA='L4',),),
                 );

# Definition du modele: probleme mecanique en contraintes planes
MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='C_PLAN',
                       ),);

# Definition du materiau
MAT=DEFI_MATERIAU(ELAS=_F(E=8.E3,
                          NU=0.29999999999999999,
                          RHO=7800.0,),);

# Affectation du materiau au maillage
CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=_F(TOUT='OUI',
                            MATER=MAT,),);

#=============================================================================================
# Conditions limites
#=============================================================================================
# Deplacement impose sur L1
CHCAS1=AFFE_CHAR_MECA(MODELE=MO,
                      DDL_IMPO=_F(GROUP_MA='L1',
                                  DX=0.5,
                                  DY=0.0,
                                  ),);

# Definition du contact au niveau de la fissure
CHCONT = DEFI_CONTACT(MODELE      = MO,
                      FORMULATION = 'DISCRETE',
                      ZONE = _F(
                                GROUP_MA_MAIT='L3',
                                GROUP_MA_ESCL='L4',
                                SANS_GROUP_NO='F2',
                                TOLE_PROJ_EXT = 0.75,
                             ),
                      );
#=============================================================================================
# Chargement
#=============================================================================================

fin = 500.
n = 50


# Chargement: definition de la fonction sinusoidale

FREQ = 0.01;

LINST=DEFI_LIST_REEL(DEBUT=0.0,
                     INTERVALLE=_F(JUSQU_A=fin,
                                   NOMBRE =n,),);

FONC = FORMULE(VALE = 'sin(2.*pi*FREQ*INST)',
               NOM_PARA='INST')

FMULT = CALC_FONC_INTERP(FONCTION=FONC,
                       LIST_PARA=LINST,
                       NOM_PARA = 'INST',
                      );

#=============================================================================================
# Calcul
#=============================================================================================

# Definition de l'archivage des resultats
LARCH=DEFI_LIST_REEL(DEBUT=0.0,
                     INTERVALLE=_F(JUSQU_A=fin,
                                   NOMBRE =n,),);

# Avec contact (CHCONT)
RESU=DYNA_NON_LINE(MODELE=MO,
                   CHAM_MATER=CHMAT,
                  EXCIT=(_F(CHARGE=CHCAS1,
                            FONC_MULT=FMULT,),
                          ),
                   CONTACT  = CHCONT,
                   COMP_INCR=_F(RELATION='ELAS',
                                TOUT='OUI',),
                   INCREMENT=_F(LIST_INST=LINST,),
                   SCHEMA_TEMPS=_F(FORMULATION='DEPLACEMENT',
                                   SCHEMA='HHT',
                                   ALPHA=0.0,),
                   NEWTON=_F(REAC_INCR=0,
                             PREDICTION='ELASTIQUE',
                             REAC_ITER=0,),
                   SOLVEUR=_F(SYME='OUI',
                              METHODE='MULT_FRONT',
                              RENUM='METIS',),
                   CONVERGENCE=_F(RESI_GLOB_RELA=1.E-05,
                                  ITER_GLOB_MAXI=20,),
                   ARCHIVAGE=_F(LIST_INST=LARCH,),);

# Sortie d'un fichier universel (unv) IDEAS

IMPR_RESU(FORMAT='IDEAS',VERSION=5,UNITE=30,
          RESU=_F(MAILLAGE=MA,
                  RESULTAT=RESU,),);

DEFI_FICHIER(ACTION='LIBERER',UNITE=30 )


# Sans contact
RESU2=DYNA_NON_LINE(MODELE=MO,
                   CHAM_MATER=CHMAT,
                  EXCIT=(_F(CHARGE=CHCAS1,
                            FONC_MULT=FMULT,),
                          ),
                   COMP_INCR=_F(RELATION='ELAS',
                                TOUT='OUI',),
                   INCREMENT=_F(LIST_INST=LINST,),
                   SCHEMA_TEMPS=_F(FORMULATION='DEPLACEMENT',
                                   SCHEMA='HHT',
                                   ALPHA=0.0,),
                   NEWTON=_F(REAC_INCR=0,
                             PREDICTION='ELASTIQUE',
                             REAC_ITER=0,),
                   SOLVEUR=_F(SYME='OUI',
                              METHODE='MULT_FRONT',
                              RENUM='METIS',),
                   CONVERGENCE=_F(RESI_GLOB_RELA=1.E-05,
                                  ITER_GLOB_MAXI=20,),
                   ARCHIVAGE=_F(LIST_INST=LARCH,),);


#=============================================================================================
# Resultats du cas avec contact
#=============================================================================================

IMPR_RESU(RESU=_F(RESULTAT=RESU,
                  NOM_CHAM='DEPL',
                  GROUP_NO='A4',
                  INST=500),);

IMPR_RESU(RESU=_F(RESULTAT=RESU,
                  NOM_CHAM='VITE',
                  GROUP_NO='A4',
                  INST=500),);

TEST_RESU(RESU=(_F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=0.17314507066042001,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=0.042272314049942999,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU,
                   NOM_CHAM='VITE',
                   NOM_CMP='DX',
                   VALE_CALC=0.086164293311672996,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU,
                   NOM_CHAM='VITE',
                   NOM_CMP='DY',
                   VALE_CALC=6.0296076997463002E-3,
                   ),
                ),
          )

#=============================================================================================
# Resultats du cas sans contact
#=============================================================================================

TEST_RESU(RESU=(_F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=-0.25335999999999997,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-0.041168472828804002,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU2,
                   NOM_CHAM='VITE',
                   NOM_CMP='DX',
                   VALE_CALC=0.040665600000000003,
                   ),
                _F(GROUP_NO='A4',
                   INST=500,
                   RESULTAT=RESU2,
                   NOM_CHAM='VITE',
                   NOM_CMP='DY',
                   VALE_CALC=8.0224200000000006E-3,
                   ),
                ),
          )

IMPR_RESU(RESU=_F(RESULTAT=RESU2,
                  NOM_CHAM='DEPL',
                  GROUP_NO='A4',
                  INST=500,),);


IMPR_RESU(RESU=_F(RESULTAT=RESU2,
                  NOM_CHAM='VITE',
                  GROUP_NO='A4',
                  INST=500,),);

##############################################################################################
#
# POST-TRAITEMENT
#
##############################################################################################

#=============================================================================================
# GMSH
#=============================================================================================

# Creation d'un fichier de sortie au format GMSH

# Ecriture du resultat
IMPR_RESU(MODELE=MO,FORMAT='GMSH',UNITE=unite_GMSH,
          RESU=_F(NOM_CHAM='DEPL',
                  RESULTAT=RESU,),);

# Fermeture de l'unite logique FORTRAN
DEFI_FICHIER(ACTION='LIBERER',UNITE=unite_GMSH )


# Lancement de GMSH
if launch_GMSH == 1:
   # Penser a avoir le fichier_GMSH dans le bon repertoire pour lancer GMSH !
   # Copie du fichier fort.55 vers fichier_GMSH. C'est ce fichier qui pourra etre
   # ouvert avec GMSH
   os.system('cp fort.'+str(unite_GMSH)+' '+fichier_GMSH)

   # On peut aussi utiliser
   #shutil.copyfile('fort.+'+`Unite_GMSH`,Fichier_GMSH);
   os.system(executable_GMSH+' '+fichier_GMSH)

#=============================================================================================
# Passage d'ASTER a numpy
#=============================================================================================

# Extraction pour le cas avec contact
TAB=POST_RELEVE_T(ACTION=_F(INTITULE='DEPLA',
                            GROUP_NO='A4',
                            TOUT_CMP='OUI',
                            RESULTAT=RESU,
                            NOM_CHAM='DEPL',
                            TOUT_ORDRE='OUI',
                            OPERATION='EXTRACTION',),);

TABe= TAB.EXTR_TABLE()
DEPLX  = TABe.Array('INST','DY')

# Extraction pour le cas sans contact
TAB2=POST_RELEVE_T(ACTION=_F(INTITULE='DEPLA',
                            GROUP_NO='A4',
                            TOUT_CMP='OUI',
                            RESULTAT=RESU2,
                            NOM_CHAM='DEPL',
                            TOUT_ORDRE='OUI',
                            OPERATION='EXTRACTION',),);

TAB2e= TAB2.EXTR_TABLE()
DEPLX2 = TAB2e.Array('INST','DY')

#=============================================================================================
# Calcul des transformees de Fourier
#=============================================================================================
FFT_DEPL  = FFT.fft(DEPLX[:,1])
PUISS     = FFT_DEPL*NP.conjugate(FFT_DEPL)

FFT_DEPL2 = FFT.fft(DEPLX2[:,1])
PUISS2    = FFT_DEPL2*NP.conjugate(FFT_DEPL2)

absc = (NP.arange(n/2)/fin).tolist()

#=============================================================================================
# XMGRACE
#=============================================================================================

# Trace 1 (deplacements)

F_TAB  = RECU_FONCTION(TABLE=TAB ,PARA_X='INST',PARA_Y='DY',)
F_TAB2 = RECU_FONCTION(TABLE=TAB2,PARA_X='INST',PARA_Y='DY')

if launch_XMGRACE == 1:
   IMPR_FONCTION(
        FORMAT='XMGRACE',
        PILOTE='INTERACTIF',
        BORNE_X=(0.,500.),
        BORNE_Y=(-0.15,0.15),
        GRILLE_X=50,
        GRILLE_Y=0.05,
        COURBE=( _F( FONCTION   = F_TAB,
                     MARQUEUR = 1,
                     COULEUR  = 2,
                     STYLE  = 1,
                     FREQ_MARQUEUR = 2,
                     LEGENDE  = 'Deplacement avec contact'),
                 _F( FONCTION   = F_TAB2,
                     MARQUEUR = 2,
                     COULEUR  = 3,
                     STYLE  = 1,
                     FREQ_MARQUEUR = 2,
                     LEGENDE  = 'Deplacement sans contact')
               ),
        TITRE='Deplacements',
        LEGENDE_X='Instants',
        LEGENDE_Y='Deplacement suivant Y',
        )

# Trace 2 (FFT des deplacements)

if launch_XMGRACE == 1:
    IMPR_FONCTION(
         FORMAT='XMGRACE',
         PILOTE='INTERACTIF',
         BORNE_X=(0.,0.05),
         GRILLE_X=0.005,
         GRILLE_Y=0.5,
         BORNE_Y=(0.,3.),
         COURBE=( _F( ABSCISSE = absc,
                      ORDONNEE = PUISS.real[0:n/2].tolist(),
                      MARQUEUR = 1,
                      COULEUR  = 2,
                      STYLE    = 1,
                      FREQ_MARQUEUR = 2,
                      LEGENDE  = 'FFT_DEPL avec contact',
                 ),
                  _F( ABSCISSE = absc,
                      ORDONNEE = PUISS2.real[0:n/2].tolist(),
                      MARQUEUR = 2,
                      COULEUR  = 3,
                      STYLE    = 1,
                      FREQ_MARQUEUR = 2,
                      LEGENDE  = 'FFT_DEPL sans contact',
                ),
          ),
         )

# Trace 3 : Sortie des courbes en Postscript

IMPR_FONCTION(
     UNITE = unite_PS,
     FORMAT='XMGRACE',
     PILOTE='POSTSCRIPT',
     BORNE_X=(0.,0.05),
     GRILLE_X=0.005,
     GRILLE_Y=0.5,
     BORNE_Y=(0.,3.),
     COURBE=( _F( ABSCISSE = absc,
                  ORDONNEE = PUISS.real[0:n/2].tolist(),
                  MARQUEUR = 1,
                  COULEUR  = 2,
                  STYLE    = 1,
                  FREQ_MARQUEUR = 2,
                  LEGENDE  = 'FFT_DEPL avec contact',
             ),
              _F( ABSCISSE = absc,
                  ORDONNEE = PUISS2.real[0:n/2].tolist(),
                  MARQUEUR = 2,
                  COULEUR  = 3,
                  STYLE    = 1,
                  FREQ_MARQUEUR = 2,
                  LEGENDE  = 'FFT_DEPL sans contact',
             ),
      ),
     )

FIN();
