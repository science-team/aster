# TITRE MUR PLAN EN THERMIQUE STATIONNAIRE (QUAD8/TRIA6)
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS TEST TPLL01B

DEBUT(CODE=_F( NOM = 'TPLL01B',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

PHI0=DEFI_CONSTANTE(    NOM_RESU='FLUN',  VALE=0.0        )

PHI1=DEFI_CONSTANTE(    NOM_RESU='FLUN',  VALE=-1200.0    )

COEH=DEFI_CONSTANTE(    NOM_RESU='FLUN',  VALE=30.0       )

T_EX=DEFI_CONSTANTE(    NOM_RESU='FLUN',  VALE=140.0      )

T_IM=DEFI_CONSTANTE(    NOM_RESU='FLUN',  VALE=100.0      )

ACIER=DEFI_MATERIAU( THER=_F(  LAMBDA = 0.75, RHO_CP = 2.0))

MAIL=LIRE_MAILLAGE(FORMAT='MED', )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MAIL,
                AFFE=_F(  TOUT = 'OUI', MATER = ACIER) )

MOTH=AFFE_MODELE(  MAILLAGE=MAIL,
                      AFFE=_F( TOUT = 'OUI', MODELISATION = 'PLAN',
                                      PHENOMENE = 'THERMIQUE'))

T40=DEFI_CONSTANTE(    VALE=40. )

CHTH=AFFE_CHAR_THER_F(  MODELE=MOTH,FLUX_REP=(
          _F( GROUP_MA = 'GRMA13', FLUN = PHI0),
          _F( GROUP_MA = 'GRMA12', FLUN = PHI1)),
           ECHANGE=_F( GROUP_MA = 'GRMA14', COEF_H = COEH, TEMP_EXT = T_EX),
          TEMP_IMPO=_F( GROUP_NO = 'GRNM15', TEMP = T_IM),
          LIAISON_DDL=_F( NOEUD = ('N13', 'N6',), COEF_MULT = (1., -1.,),
                       COEF_IMPO = T40)
         )

LR8=DEFI_LIST_REEL(        DEBUT=0.,
       INTERVALLE=_F(    JUSQU_A = 10.0,   NOMBRE = 2)
                       )

TEMPE=THER_LINEAIRE(        MODELE=MOTH,
           ETAT_INIT=_F( STATIONNAIRE = 'OUI'),
          INCREMENT=_F( LIST_INST = LR8),
             CHAM_MATER=CHMAT, EXCIT=_F( CHARGE = CHTH)
             )

TEMPE=CALC_CHAMP(reuse=TEMPE,RESULTAT=TEMPE,THERMIQUE=('FLUX_ELNO','FLUX_ELGA'))


CHEM2=INTE_MAIL_2D(   MAILLAGE=MAIL,
                      DEFI_SEGMENT=_F( ORIGINE = ( 0.04,  0.07, ),
                                    EXTREMITE = ( 0.07,  0.03, ))
                   )

TR1_C=POST_RELEVE_T(    ACTION=_F(  INTITULE = 'FLU_NORM',
                           CHEMIN = CHEM2,
                           RESULTAT = TEMPE,
                           NOM_CHAM = 'FLUX_ELNO',
                           NUME_ORDRE = 2,
                           TRAC_NOR = 'OUI',
                           NOM_CMP = ( 'FLUX', 'FLUY', ),
                           OPERATION = 'EXTRACTION')  )

TEST_TABLE(VALE_REFE=1200.0,
           VALE_CALC=1200.0017523818,
           PRECISION=1.E-3,
           REFERENCE='ANALYTIQUE',
           NOM_PARA='TRAC_NOR',
           TABLE=TR1_C,
           FILTRE=_F(NOM_PARA='ABSC_CURV',
                     VALE=0.0,),
           )

TEST_TABLE(VALE_REFE=1200.0,
           VALE_CALC=1199.999446011,
           PRECISION=1.E-3,
           REFERENCE='ANALYTIQUE',
           NOM_PARA='TRAC_NOR',
           TABLE=TR1_C,
           FILTRE=_F(NOM_PARA='ABSC_CURV',
                     VALE=0.025000000000000001,),
           )

TEST_TABLE(VALE_REFE=1200.0,
           VALE_CALC=1200.0011516795,
           PRECISION=1.E-3,
           REFERENCE='ANALYTIQUE',
           NOM_PARA='TRAC_NOR',
           TABLE=TR1_C,
           FILTRE=_F(NOM_PARA='ABSC_CURV',
                     VALE=0.050000000000000003,),
           )

T2=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_TEMP_R',
NOM_CHAM='TEMP', RESULTAT=TEMPE,
                   NUME_ORDRE=2)

TEST_RESU(RESU=(_F(NUME_ORDRE=2,
                   RESULTAT=TEMPE,
                   NOM_CHAM='TEMP',
                   NOEUD='N22',
                   NOM_CMP='TEMP',
                   VALE_REFE=100.0,
                   VALE_CALC=100.0,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,),
                _F(NUME_ORDRE=2,
                   RESULTAT=TEMPE,
                   NOM_CHAM='TEMP',
                   NOEUD='N6',
                   NOM_CMP='TEMP',
                   VALE_REFE=20.0,
                   VALE_CALC=19.999998972827,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,),
                _F(NUME_ORDRE=2,
                   RESULTAT=TEMPE,
                   NOM_CHAM='TEMP',
                   NOEUD='N13',
                   NOM_CMP='TEMP',
                   VALE_REFE=60.0,
                   VALE_CALC=59.999998972827,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,),
                _F(NUME_ORDRE=2,
                   POINT=3,
                   RESULTAT=TEMPE,
                   NOM_CHAM='FLUX_ELNO',
                   NOM_CMP='FLUX',
                   VALE_REFE=960.0,
                   VALE_CALC=960.00098247149,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,
                   MAILLE='M1',),
                _F(NUME_ORDRE=2,
                   POINT=2,
                   RESULTAT=TEMPE,
                   NOM_CHAM='FLUX_ELNO',
                   NOM_CMP='FLUY',
                   VALE_REFE=720.0,
                   VALE_CALC=720.00022543269,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,
                   MAILLE='M2',),
                _F(NUME_ORDRE=2,
                   POINT=3,
                   RESULTAT=TEMPE,
                   NOM_CHAM='FLUX_ELGA',
                   NOM_CMP='FLUX',
                   VALE_REFE=960.0,
                   VALE_CALC=960.00077617621,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,
                   MAILLE='M1',),
                _F(NUME_ORDRE=2,
                   POINT=2,
                   RESULTAT=TEMPE,
                   NOM_CHAM='FLUX_ELGA',
                   NOM_CMP='FLUY',
                   VALE_REFE=720.0,
                   VALE_CALC=720.00018526969,
                   REFERENCE='ANALYTIQUE',
                   PRECISION=1.E-3,
                   MAILLE='M2',),
                ),
          )

#==============================================================
# POUR TESTER LES POST-TRAITEMENTS AUX POINTS DE GAUSS :
# ----------------------------------------------------------

MACR_ECLA_PG(   MODELE_INIT = MOTH,  RESU_INIT = TEMPE,
                NOM_CHAM = ('FLUX_ELGA',), INST=(0.,10.),
                RESULTAT = CO("U2B"), MAILLAGE = CO("MA2"),
                 )

TEST_RESU(RESU=(_F(NUME_ORDRE=0,
                   TYPE_TEST='SOMM_ABS',
                   RESULTAT=U2B,
                   NOM_CHAM='FLUX_ELGA',
                   VALE_CALC=2.6208004533937E5,),
                _F(NUME_ORDRE=2,
                   TYPE_TEST='SOMM_ABS',
                   RESULTAT=U2B,
                   NOM_CHAM='FLUX_ELGA',
                   VALE_CALC=2.6208004533937E5,),
                ),
          )

# POUR FAIRE DU POST_RELEVE_T :
# --------------------------------
CHEM1=INTE_MAIL_2D(   MAILLAGE=MA2,  INFO=2,
       DEFI_SEGMENT=_F(
       ORIGINE = (0., 0.05,), EXTREMITE = (0.1, 0.04,))
       )

RELTEMP=POST_RELEVE_T(
     ACTION=_F(  CHEMIN = CHEM1,
            INTITULE = 'EXT_FLUX',  RESULTAT = U2B,
            NOM_CHAM = 'FLUX_ELGA',  TOUT_ORDRE = 'OUI',
            TOUT_CMP = 'OUI', OPERATION = 'EXTRACTION')
            )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=23040.00364925,
           NOM_PARA='FLUX',
           TYPE_TEST='SOMM_ABS',
           TABLE=RELTEMP,)

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.88104057668374,
           NOM_PARA='ABSC_CURV',
           TYPE_TEST='SOMM_ABS',
           TABLE=RELTEMP,)

#==============================================================
# pour tester MACR_ECLA_PG sur un ensemble de mailles

MACR_ECLA_PG(   MODELE_INIT = MOTH,  RESU_INIT = TEMPE,
                NOM_CHAM = ('FLUX_ELGA',), INST=(5.,), MAILLE=('M3','M5'),
                RESULTAT = CO("U3B"), MAILLAGE = CO("MA3B"),
                 )

TEST_RESU(RESU=_F(NUME_ORDRE=1,
                  RESULTAT=U3B,
                  NOM_CHAM='FLUX_ELGA',
                  NOEUD='N5',
                  NOM_CMP='FLUX',
                  VALE_CALC=960.00023993481,),
          )

FIN()
#
