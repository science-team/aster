# ======================================================================
# TITRE VALIDATION ARCHITECTURE PARTITIONNEMENT METIS ET CALCUL FETI
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# ======================================================================
# CUBE 3D
# PARALLELE: VALEUR CONSEILLEE 16 PROCS.
# ======================================================================
# person_in_charge: olivier.boiteau at edf.fr

DEBUT(CODE=_F(NOM='FETI007B',
NIV_PUB_WEB='INTERNET',),DEBUG=_F(SDVERI='OUI'))

MAIL1=LIRE_MAILLAGE(FORMAT='MED',);

MATERI=DEFI_MATERIAU(ELAS=_F(E=180000.,
                             NU=0.30,
                             RHO=7700.,
                             ALPHA=15.E-6,),);

MODM=AFFE_MODELE(MAILLAGE=MAIL1,
                 AFFE=_F(TOUT='OUI',
                         PHENOMENE='MECANIQUE',
                         MODELISATION='3D',),);

MAIL1=MODI_MAILLAGE(reuse =MAIL1,
                    MAILLAGE=MAIL1,
                    ORIE_PEAU_3D=_F(GROUP_MA='PRESS',),);

CH1=AFFE_CHAR_MECA(MODELE=MODM,
                   PRES_REP=_F(GROUP_MA='PRESS',
                               PRES=-20000.,),);

CH2=AFFE_CHAR_MECA(MODELE=MODM,
                   DDL_IMPO=_F(GROUP_NO='DDLIMP',
                               DX=0.0,
                               DY=0.0,
                               DZ=0.0,),);

# Partitionnement avec verification de la connexite

SDFETI2=DEFI_PART_FETI(MODELE=MODM,
                       NB_PART=16,
                       EXCIT=(_F(CHARGE=CH1,),
                              _F(CHARGE=CH2,),),
                       METHODE='PMETIS',
                       CORRECTION_CONNEX='OUI',
                       NOM_GROUP_MA='TOTO',
                       TRAITER_BORDS='NON',
                       INFO=1,);

# Partitionnement avec gestion des mailles de bords
SDFETI =DEFI_PART_FETI(MODELE=MODM,
                      NB_PART=16,
                      EXCIT=(_F(CHARGE=CH1,),
                             _F(CHARGE=CH2,),),
                      METHODE='KMETIS',
                      CORRECTION_CONNEX='NON',
                      NOM_GROUP_MA='SD',
                      TRAITER_BORDS='OUI',
                      INFO=1,);

CHMAT=AFFE_MATERIAU(MAILLAGE=MAIL1,
                    AFFE=_F(TOUT='OUI',
                            MATER=MATERI,),);

RESU=MECA_STATIQUE(MODELE=MODM,
                   CHAM_MATER=CHMAT,
                   EXCIT=(_F(CHARGE=CH1,),
                          _F(CHARGE=CH2,),),
                   SOLVEUR=_F(METHODE='FETI',
                              PARTITION=SDFETI,
                              RESI_RELA=1.E-7,
                              NMAX_ITER=200,
                              NB_REORTHO_DD=30,
                              VERIF_SDFETI='OUI',
                              TEST_CONTINU=1.E-8,
                              TYPE_REORTHO_DD='GSM',
                              PRE_COND='LUMPE',
                              SCALING='MULT',
                              RENUM='METIS',
                              INFO_FETI='FFFFFFFFF',
                              NPREC=5,),
                   INFO=1,);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='N2',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=-1.623142965764E-20,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N2',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-5.1237211561755002E-21,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N2',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=-8.3330295250455998E-21,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N1',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=1.0179192829954001E-21,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N1',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-5.9520580033708995E-20,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N1',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=4.6064426733403001E-21,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N5',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=-9.3118583696395002E-20,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N5',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-1.2652046411437E-20,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N5',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=-4.1696957771226999E-20,
                   CRITERE='ABSOLU',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N3',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=0.052002305908117,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N3',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=2.0438605483257E-04,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N3',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=0.021402668751424,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N7',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=0.051912619704714,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N7',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-0.058940276632982,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N7',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=0.039338770157931,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N8',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=0.087941005987430,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N8',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-0.035913070939127,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='N8',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=0.045090673987027,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

FIN();
