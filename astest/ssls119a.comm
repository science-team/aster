# TITRE  : TEST DU RAASCH HOOK
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#---------------------------------------------------------------------
#- CROCHET SOUS CISAILLEMENT
#- MODELISATION AVEC DES ELEMENTS 'DST' (QUA4)
#---------------------------------------------------------------------
# person_in_charge: patrick.massin at edf.fr
#

DEBUT(CODE=_F(NOM='SSLS119A',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

MAIL=LIRE_MAILLAGE(VERI_MAIL=_F(VERIF='OUI'),FORMAT='MED',);

MO=AFFE_MODELE(AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='DST',),
               MAILLAGE=MAIL,);

CAR_ELE=AFFE_CARA_ELEM(MODELE=MO,
                       COQUE=_F(GROUP_MA='TOUT_ELT',
                                EPAIS=0.0508,),);

MAT=DEFI_MATERIAU(ELAS=_F(NU=0.35,
                          E=22752510.,),);

CHAM_MAT=AFFE_MATERIAU(AFFE=_F(MATER=MAT,
                               TOUT='OUI',),
                       MAILLAGE=MAIL,);

CHA=AFFE_CHAR_MECA(DDL_IMPO=_F(GROUP_MA='ENCASTR',
                               DZ=0.0,
                               DX=0.0,
                               DY=0.0,
                               DRZ=0.0,
                               DRX=0.0,
                               DRY=0.0,),
                   MODELE=MO,
                   FORCE_ARETE=_F(GROUP_MA='LIBRE',
                                  FZ=8.7594,),);

RESU=MECA_STATIQUE(CHAM_MATER=CHAM_MAT,
                   MODELE=MO,
                   CARA_ELEM=CAR_ELE,
                   EXCIT=_F(CHARGE=CHA,),);

RESU=CALC_CHAMP(reuse=RESU,RESULTAT=RESU,CONTRAINTE=('SIGM_ELNO'))


DEP_Z=POST_RELEVE_T(ACTION=_F(OPERATION='EXTRACTION',
                              INTITULE='TAB_NORM',
                              GROUP_NO= ('C','D'),
                              NOM_CMP='DZ',
                              RESULTAT=RESU,
                              NOM_CHAM='DEPL',),);

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   GROUP_NO='C',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=0.44569399999999998,
                   TOLE_MACHINE=1.0000000000000001E-05,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   GROUP_NO='D',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DZ',
                   VALE_CALC=0.44569399999999998,
                   TOLE_MACHINE=1.0000000000000001E-05,
                   CRITERE='RELATIF',
                   ),
                ),
          )

FIN();
