# TITRE POUTRE ELANCEE, ENCASTREE-LIBRE, REPLIEE SUR ELLE-MEME
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS_TEST__: SDLL02B
# SDLL02/B                COMMANDES                             17/1/95
# POUTRE ELANCEE ENCASTREE-LIBRE REPLIEE SUR ELLE-MEME
# SECTION RECT: 5*50MM --- L=2*0.5M --- 2D
#                                                  REF: SFM.VPCS SDLL02
# MODELISATION POU_D_E: 21 NOEUDS  --   20 MAILLES SEG2
# POINTS  A    B    C
# MODELISATION :
#    CONDITIONS AUX LIMITES IMPOSEES PAR AFFE_CHAR_CINE
#    RECHERCHE DES ELEMENTS PROPRES : METHODE JACOBI
#=======================================================================

DEBUT(CODE=_F(  NOM = 'SDLL02B',NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(  )

MA=DEFI_GROUP( reuse=MA,   MAILLAGE=MA,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

MO=AFFE_MODELE(  MAILLAGE=MA,
                  AFFE=_F(  TOUT = 'OUI',    PHENOMENE = 'MECANIQUE',
                         MODELISATION = 'POU_D_E')  )

CARELEM=AFFE_CARA_ELEM(  MODELE=MO,
             POUTRE=_F(
       GROUP_MA = 'TOUT',
     SECTION = 'RECTANGLE',
                      CARA = (  'HY',   'HZ', ),
                      VALE = ( 0.005,  0.050, ))  )

MAT=DEFI_MATERIAU( ELAS=_F(  E = 2.1E+11,    NU = 0.3,    RHO = 7800.) )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MA,
          AFFE=_F(  TOUT = 'OUI',     MATER = MAT) )

#   PB PLAN --- POINT A ENCASTRE
CH2=AFFE_CHAR_CINE(  MODELE=MO,MECA_IMPO=(
       _F(  TOUT = 'OUI', DZ = 0., DRX = 0., DRY = 0.),
                _F(  NOEUD = 'A',   DX = 0., DY = 0.,  DRZ = 0.)) )

#--------------------------------------------------------------------
# DEUXIEME MODELISATION

MELR2=CALC_MATR_ELEM(  MODELE=MO,
                         CARA_ELEM=CARELEM,
                         CHAM_MATER=CHMAT,   OPTION='RIGI_MECA' )

MELM2=CALC_MATR_ELEM(  MODELE=MO,
                         CARA_ELEM=CARELEM,
                         CHAM_MATER=CHMAT,   OPTION='MASS_MECA' )

NUM2=NUME_DDL(  MATR_RIGI=MELR2 )

MATASSR2=ASSE_MATRICE(  MATR_ELEM=MELR2,   NUME_DDL=NUM2,
                           CHAR_CINE=CH2 )

MATASSM2=ASSE_MATRICE(  MATR_ELEM=MELM2,   NUME_DDL=NUM2,
                           CHAR_CINE=CH2)

#--------------------------------------------------------------------
#--------------------------------------------------------------------


FREQ2=MODE_ITER_SIMULT(
              METHODE='JACOBI',
                OPTION='SANS',
               MATR_RIGI=MATASSR2,        MATR_MASS=MATASSM2,
               CALC_FREQ=_F(  OPTION = 'PLUS_PETITE',
                           NMAX_FREQ = 8),
                TITRE='METHODE DE BATHE ET WILSON' )


FREQ2=NORM_MODE(reuse=FREQ2,   MODE=FREQ2,   NORME='TRAN' )

# ---------------------------------------------------------------------
MODE12=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=1
                           )

MODE22=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=2
                           )

MODE32=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=3
                           )

MODE42=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=4
                           )

MODE72=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=7
                           )

MODE82=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ2,
                      NUME_ORDRE=8
                           )

# ---------------------------------------------------------------------


TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=11.764183404,
                   VALE_REFE=11.76,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=11.764183438,
                   VALE_REFE=11.76,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=105.881130032,
                   VALE_REFE=105.88,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=105.881203672,
                   VALE_REFE=105.88,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=294.177995325,
                   VALE_REFE=294.10000000000002,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=6,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=294.180625511,
                   VALE_REFE=294.10000000000002,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=7,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=576.980226710,
                   VALE_REFE=576.44000000000005,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   RESULTAT=FREQ2,
                   VALE_CALC=577.007933722,
                   VALE_REFE=576.44000000000005,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                ),
          CHAM_NO=(_F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      TOLE_MACHINE=5.e-4,   #TODO variabilite
                      PRECISION=0.02,
                      CHAM_GD=MODE12,
                      VALE_CALC=-0.707108931,
                      VALE_REFE=-0.69999999999999996,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE12,
                      VALE_CALC=1.000000000,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      TOLE_MACHINE=5.e-4,   #TODO variabilite
                      PRECISION=0.040000000000000001,
                      CHAM_GD=MODE22,
                      VALE_CALC=0.707102141,
                      VALE_REFE=0.70699999999999996,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE22,
                      VALE_CALC=1.000000000,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE32,
                      VALE_CALC=0.707107144,
                      VALE_REFE=0.70699999999999996,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE32,
                      VALE_CALC=1.000000000,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE42,
                      TOLE_MACHINE=5.E-6,       # ajustement pour clap0f0q
                      VALE_CALC=-0.370146882,
                      VALE_REFE=-0.37,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE42,
                      TOLE_MACHINE=5.E-6,       # ajustement pour clap0f0q
                      VALE_CALC=0.523468389,
                      VALE_REFE=0.52300000000000002,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE72,
                      VALE_CALC=0.70710679968782,
                      VALE_REFE=0.70699999999999996,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE72,
                      VALE_CALC=1.000000000,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE82,
                      VALE_CALC=-0.388465763,
                      VALE_REFE=-0.38800000000000001,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE82,
                      VALE_CALC=0.549373634,
                      VALE_REFE=0.54900000000000004,
                      REFERENCE='ANALYTIQUE',),
                   ),
          )

# ---------------------------------------------------------------------

FREQ2=NORM_MODE(reuse=FREQ2,   MODE=FREQ2,   NORME='EUCL_TRAN' )

TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='MASS_GENE',
                   RESULTAT=FREQ2,
                   VALE_CALC=0.084511993,
                   VALE_REFE=0.084364300000000003,
                   REFERENCE='ANALYTIQUE',
                   TOLE_MACHINE=1.e-4,   #TODO variabilite
                   PRECISION=2.3999999999999998E-3,),
                _F(NUME_ORDRE=2,
                   PARA='MASS_GENE',
                   RESULTAT=FREQ2,
                   VALE_CALC=0.091503742,
                   VALE_REFE=0.091677499999999995,
                   REFERENCE='ANALYTIQUE',
                   TOLE_MACHINE=1.e-4,   #TODO variabilite
                   PRECISION=2.5000000000000001E-3,),
                _F(NUME_ORDRE=3,
                   PARA='RIGI_GENE',
                   RESULTAT=FREQ2,
                   VALE_CALC=35204.153131825),
                _F(NUME_ORDRE=4,
                   PARA='RIGI_GENE',
                   RESULTAT=FREQ2,
                   VALE_CALC=4.1597199999999997E4,),
                _F(NUME_ORDRE=5,
                   RESULTAT=FREQ2,
                   NOM_CHAM='DEPL',
                   NOEUD='C',
                   NOM_CMP='DY',
                   VALE_CALC=0.52043099999999998,),
                _F(NUME_ORDRE=6,
                   RESULTAT=FREQ2,
                   NOM_CHAM='DEPL',
                   NOEUD='N6',
                   NOM_CMP='DRZ',
                   VALE_CALC=-6.4744200000000003,),
                ),
          )

# ---------------------------------------------------------------------

FIN()
#
