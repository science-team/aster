
#
# TITRE CONTACT DE DEUX SPHERES
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
#              SEE THE FILE "LICENSE.TERMS" FOR INFORMATION ON USAGE AND
#              REDISTRIBUTION OF THIS FILE.
# ======================================================================
#
# MODELISATION 2D AXI ELEMENTS LINEAIRES : TRIA3 ET QUAD4
######


DEBUT(CODE=_F(NOM='SSNV104F',
NIV_PUB_WEB='INTERNET',),DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(FORMAT='MED',);

MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='AXIS',),);
#

MAT=DEFI_MATERIAU(ELAS=_F(E=20000.0,
                          NU=0.3,
                          ALPHA=0.01,),);
#

CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=_F(TOUT='OUI',
                            MATER=MAT,),);
#

CHAR = AFFE_CHAR_MECA(MODELE=MO,
                    DDL_IMPO=(_F(GROUP_NO='SPHSUP',
                                 DY=-2.0,),
                              _F(GROUP_NO='SPHINF',
                                 DY=2.0,),
                              _F(GROUP_NO='AXE',
                                 DX=0.0,),),);

CHA2=DEFI_CONTACT(MODELE      = MO,
                  FORMULATION = 'DISCRETE',
                  ZONE=_F(APPARIEMENT='NODAL',
                               NORMALE='MAIT_ESCL',
                               ALGO_CONT='LAGRANGIEN',
                               GROUP_MA_MAIT='CON1',
                               GROUP_MA_ESCL='CON2',),
                    INFO=2,);
#

CHA3=DEFI_CONTACT(MODELE      = MO,
                  FORMULATION = 'DISCRETE',
                  ZONE=_F(APPARIEMENT='MAIT_ESCL',
                               NORMALE='MAIT_ESCL',
                               ALGO_CONT='LAGRANGIEN',
                               GROUP_MA_MAIT='CON1',
                               GROUP_MA_ESCL='CON2',),
                    INFO=2,);
#

CHA4=DEFI_CONTACT(MODELE      = MO,
                  FORMULATION = 'DISCRETE',
                  ZONE=_F(NORMALE='MAIT_ESCL',
                               ALGO_CONT='CONTRAINTE',
                               GROUP_MA_MAIT='CON1',
                               GROUP_MA_ESCL='CON2',),
                    INFO=2,);
#

CHA5=DEFI_CONTACT(MODELE      = MO,
                  FORMULATION = 'DISCRETE',
                  LISSAGE='OUI',
                  ZONE=_F(
                               NORMALE='MAIT_ESCL',
                               ALGO_CONT='CONTRAINTE',
                               GROUP_MA_MAIT='CON1',
                               GROUP_MA_ESCL='CON2',),
                    INFO=2,);
#

RAMPE=DEFI_FONCTION(NOM_PARA='INST',VALE=(0.0,0.0,
                          1.0,1.0,
                          ),PROL_DROITE='LINEAIRE',PROL_GAUCHE='LINEAIRE',);
#

L_INST=DEFI_LIST_REEL(DEBUT=0.0,
                      INTERVALLE=_F(JUSQU_A=1.,
                                    NOMBRE=10,),);
#

RESU2=STAT_NON_LINE(MODELE=MO,
                    CHAM_MATER=CHMAT,
                    EXCIT=_F(CHARGE=CHAR,
                             FONC_MULT=RAMPE,),
                    CONTACT  = CHA2,
                    COMP_ELAS=_F(RELATION='ELAS',),
                    INCREMENT=_F(LIST_INST=L_INST,
                                 NUME_INST_FIN=10,),
                    NEWTON=_F(MATRICE='ELASTIQUE',),
                    CONVERGENCE=_F(ITER_GLOB_MAXI=10,
                                   ARRET='OUI',),
                    SOLVEUR=_F(SYME='OUI',),);

RESU2=CALC_CHAMP(reuse=RESU2,RESULTAT=RESU2,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'))

#

SIELGA2=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU2,
                   NOM_CHAM='SIEF_ELGA',
                   NUME_ORDRE=10,);
#

SIELNO2=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU2,
                   NOM_CHAM='SIGM_ELNO',
                   NUME_ORDRE=10,);
# TEST DE LA CONFORMITE AVEC LA SOLUTION DE REFERENCE ANALYTIQUE

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       REFERENCE='ANALYTIQUE',
                       NOM_CMP='SIYY',
                       PRECISION=0.070000000000000007,
                       MAILLE='M31',
                       CHAM_GD=SIELNO2,
                       VALE_CALC=-2.97136718E+03,
                       VALE_REFE=-2798.3000000000002,),
          )

# TEST DE LA CONFORMITE PAR RAPPORT AUX RESULTATS ASTER
# PRECEDENTS

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       MAILLE='M31',
                       CHAM_GD=SIELNO2,
                       VALE_CALC=-2971.3699999999999,
                       TOLE_MACHINE=1.E-3,),
          RESU=(_F(NUME_ORDRE=10,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='N291',
                   NOM_CMP='DX',
                   VALE_CALC= 1.5340931106E-19,
                   VALE_REFE=0.0,
                   TOLE_MACHINE=1.E-10,
                   CRITERE='ABSOLU',
                   PRECISION=1.E-10,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DX',
                   VALE_CALC=-0.110211,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DY',
                   VALE_CALC=-0.162911,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DX',
                   VALE_CALC=-0.16594600000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DY',
                   VALE_CALC=-0.62966599999999995,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

RESU3=STAT_NON_LINE(MODELE=MO,
                    CHAM_MATER=CHMAT,
                    EXCIT=_F(CHARGE=CHAR,
                             FONC_MULT=RAMPE,),
                             CONTACT  = CHA3,
                    COMP_ELAS=_F(RELATION='ELAS',),
                    INCREMENT=_F(LIST_INST=L_INST,
                                 NUME_INST_FIN=10,),
                    NEWTON=_F(MATRICE='ELASTIQUE',),
                    CONVERGENCE=_F(ITER_GLOB_MAXI=10,
                                   ARRET='OUI',),
                    SOLVEUR=_F(SYME='OUI',),);

RESU3=CALC_CHAMP(reuse=RESU3,RESULTAT=RESU3,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'))

#

SIELGA3=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU3,
                   NOM_CHAM='SIEF_ELGA',
                   NUME_ORDRE=10,);
#

SIELNO3=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU3,
                   NOM_CHAM='SIGM_ELNO',
                   NUME_ORDRE=10,);

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       REFERENCE='ANALYTIQUE',
                       NOM_CMP='SIYY',
                       PRECISION=0.070000000000000007,
                       MAILLE='M31',
                       CHAM_GD=SIELNO3,
                       VALE_CALC=-2.96715640E+03,
                       VALE_REFE=-2798.3000000000002,),
          )

# TEST DE LA CONFORMITE PAR RAPPORT AUX RESULTATS ASTER
# PRECEDENTS

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       MAILLE='M31',
                       CHAM_GD=SIELNO3,
                       VALE_CALC=-2966.0,
                       TOLE_MACHINE=1.E-3,),
          RESU=(_F(NUME_ORDRE=10,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESU3,
                   NOM_CHAM='DEPL',
                   NOEUD='N291',
                   NOM_CMP='DX',
                   VALE_CALC=-1.5905402278E-19,
                   VALE_REFE=0.0,
                   TOLE_MACHINE=1.E-10,
                   CRITERE='ABSOLU',
                   PRECISION=1.E-10,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU3,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DX',
                   VALE_CALC=-0.1106784,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU3,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DY',
                   VALE_CALC=-0.16289110000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU3,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DX',
                   VALE_CALC=-0.16719400000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU3,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DY',
                   VALE_CALC=-0.62894700000000003,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

RESU4=STAT_NON_LINE(MODELE=MO,INFO=2,
                    CHAM_MATER=CHMAT,
                    EXCIT=_F(CHARGE=CHAR,
                             FONC_MULT=RAMPE,),
                    CONTACT  = CHA4,
                    COMP_ELAS=_F(RELATION='ELAS',),
                    INCREMENT=_F(LIST_INST=L_INST,
                                 NUME_INST_FIN=10,),
                    NEWTON=_F(MATRICE='ELASTIQUE',),
                    CONVERGENCE=_F(ITER_GLOB_MAXI=10,
                                   ARRET='OUI',),
                    SOLVEUR=_F(SYME='OUI',),);

RESU4=CALC_CHAMP(reuse=RESU4,RESULTAT=RESU4,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'))

#

SIELGA4=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU4,
                   NOM_CHAM='SIEF_ELGA',
                   NUME_ORDRE=10,);
#

SIELNO4=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU4,
                   NOM_CHAM='SIGM_ELNO',
                   NUME_ORDRE=10,);

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       REFERENCE='ANALYTIQUE',
                       NOM_CMP='SIYY',
                       PRECISION=0.070000000000000007,
                       MAILLE='M31',
                       CHAM_GD=SIELNO4,
                       VALE_CALC=-2.96715640E+03,
                       VALE_REFE=-2798.3000000000002,),
          )

# TEST DE LA CONFORMITE PAR RAPPORT AUX RESULTATS ASTER
# PRECEDENTS

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       MAILLE='M31',
                       CHAM_GD=SIELNO4,
                       VALE_CALC=-2966.0,
                       TOLE_MACHINE=1.E-3,),
          RESU=(_F(NUME_ORDRE=10,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESU4,
                   NOM_CHAM='DEPL',
                   NOEUD='N291',
                   NOM_CMP='DX',
                   VALE_CALC=-3.0944535403E-19,
                   VALE_REFE=0.0,
                   TOLE_MACHINE=1.E-10,
                   CRITERE='ABSOLU',
                   PRECISION=1.E-10,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU4,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DX',
                   VALE_CALC=-0.110678,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU4,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DY',
                   VALE_CALC=-0.16290099999999999,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU4,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DX',
                   VALE_CALC=-0.16719400000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU4,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DY',
                   VALE_CALC=-0.62894749999999999,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

# TEST DE LA CONFORMITE AVEC LA SOLUTION DE REFERENCE ANALYTIQUE

RESU5=STAT_NON_LINE(MODELE=MO,
                    CHAM_MATER=CHMAT,
                    EXCIT=_F(CHARGE=CHAR,
                             FONC_MULT=RAMPE,),
                    CONTACT  = CHA5,
                    COMP_ELAS=_F(RELATION='ELAS',),
                    INCREMENT=_F(LIST_INST=L_INST,
                                 NUME_INST_FIN=10,),
                    NEWTON=_F(MATRICE='ELASTIQUE',),
                    CONVERGENCE=_F(ITER_GLOB_MAXI=10,
                                   ARRET='OUI',),
                    SOLVEUR=_F(SYME='OUI',),);

RESU5=CALC_CHAMP(reuse=RESU5,RESULTAT=RESU5,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'))

#

SIELGA5=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU5,
                   NOM_CHAM='SIEF_ELGA',
                   NUME_ORDRE=10,);
#

SIELNO5=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                   OPERATION='EXTR',
                   RESULTAT=RESU5,
                   NOM_CHAM='SIGM_ELNO',
                   NUME_ORDRE=10,);

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       REFERENCE='ANALYTIQUE',
                       NOM_CMP='SIYY',
                       PRECISION=0.070000000000000007,
                       MAILLE='M31',
                       CHAM_GD=SIELNO5,
                       VALE_CALC=-2.97136622E+03,
                       VALE_REFE=-2798.3000000000002,),
          )

# TEST DE LA CONFORMITE PAR RAPPORT AUX RESULTATS ASTER
# PRECEDENTS

TEST_RESU(CHAM_ELEM=_F(NOEUD='N291',
                       CRITERE='RELATIF',
                       NOM_CMP='SIYY',
                       MAILLE='M31',
                       CHAM_GD=SIELNO5,
                       VALE_CALC=-2971.3699999999999,
                       TOLE_MACHINE=1.E-3,),
          RESU=(_F(NUME_ORDRE=10,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=RESU5,
                   NOM_CHAM='DEPL',
                   NOEUD='N291',
                   NOM_CMP='DX',
                   VALE_CALC=-4.0491563748E-21,
                   VALE_REFE=0.0,
                   TOLE_MACHINE=1.E-10,
                   CRITERE='ABSOLU',
                   PRECISION=1.E-10,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU5,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DX',
                   VALE_CALC=-0.110211,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU5,
                   NOM_CHAM='DEPL',
                   NOEUD='N287',
                   NOM_CMP='DY',
                   VALE_CALC=-0.162911,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU5,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DX',
                   VALE_CALC=-0.16594600000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU5,
                   NOM_CHAM='DEPL',
                   NOEUD='N285',
                   NOM_CMP='DY',
                   VALE_CALC=-0.62966599999999995,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

# TEST DE LA CONFORMITE AVEC LA SOLUTION DE REFERENCE ANALYTIQUE

FIN();
#
