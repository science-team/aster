# TITRE : TRACTION SUR UN POLYCRISTAL A 10 GRAINS, ACIER BAINITIQUE
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: jean-michel.proix at edf.fr

DEBUT(CODE=_F(NOM='SSNV194B',
NIV_PUB_WEB='INTERNET',),DEBUG=_F(SDVERI='NON'))
# SDVERI='NON' car la verification est trop couteuse en CPU

CUBE=LIRE_MAILLAGE();

PRECIS=1.E-6

CUBE=DEFI_GROUP(reuse =CUBE,
              MAILLAGE=CUBE,
              DETR_GROUP_NO=_F(NOM=('GAUCHE','ARRIERE','HAUT','BAS',),),
              CREA_GROUP_NO=(_F(OPTION='PLAN',
                                NOM='BAS',
                                POINT=(0.0,0.0,0.0,),
                                VECT_NORMALE=(0.0,0.0,1.0,),
                                PRECISION=PRECIS,),
                             _F(OPTION='PLAN',
                                NOM='HAUT',
                                POINT=(0.0,0.0,1.0,),
                                VECT_NORMALE=(0.0,0.0,1.0,),
                                PRECISION=PRECIS,),
                             _F(OPTION='PLAN',
                                NOM='GAUCHE',
                                POINT=(0.0,0.0,0.0,),
                                VECT_NORMALE=(0.0,1.0,0.0,),
                                PRECISION=PRECIS,),
                             _F(OPTION='PLAN',
                                NOM='ARRIERE',
                                POINT=(0.0,0.0,0.0,),
                                VECT_NORMALE=(1.0,0.0,0.0,),
                                PRECISION=PRECIS,),),);

TROISD=AFFE_MODELE(MAILLAGE=CUBE,
                   AFFE=_F(TOUT='OUI',
                           PHENOMENE='MECANIQUE',
                           MODELISATION='3D',),);
TFIN = 0.005;




LINST=DEFI_LIST_REEL(DEBUT=0.0,
                     INTERVALLE=(_F(JUSQU_A=0.001,
                                    NOMBRE=1,),
                                 _F(JUSQU_A=0.0018,
                                    NOMBRE=140,),
                                 _F(JUSQU_A=TFIN,
                                    NOMBRE=150,),
                                    ),);

ACIER=DEFI_MATERIAU(ELAS=_F(E=145200.0,
                            NU=0.3,),
                    MONO_VISC1=_F(N=10.0,
                                  K=40.0,
                                  C=1.0,),
                    MONO_ISOT1=_F(R_0=75.5,
                                  Q=9.77,
                                  B=19.34,
                                  H1=1.0,
                                  H2=1.0,
                                  H3=1.0,
                                  H4=1.0,
                                  ),
                    MONO_CINE1=_F(D=36.68,),);

COEF=DEFI_FONCTION(NOM_PARA='INST',
                   VALE=(0.0,0.0,1.0,1.0,),
                   PROL_DROITE='LINEAIRE',
                   PROL_GAUCHE='LINEAIRE',);

MAT=AFFE_MATERIAU(MAILLAGE=CUBE,
                  AFFE=_F(TOUT='OUI',
                          MATER=ACIER,),);

TRAC=AFFE_CHAR_MECA(MODELE=TROISD,
                     DDL_IMPO=(
                     _F(GROUP_NO='ARRIERE',
                                 DX=0.0,),
                        _F(GROUP_NO='BAS',
                                 DZ=0.0,),
                       _F(GROUP_NO='GAUCHE',
                                 DY=0.0,),
                       _F(GROUP_NO='HAUT',
                                   DZ=1.0),
                                   ),)

table=LIRE_TABLE( UNITE=40,
                   FORMAT='ASTER',
                   NUME_TABLE=1,
                   SEPARATEUR=' ',)


COMPORT=DEFI_COMPOR(MONOCRISTAL=(_F(MATER=ACIER,
                                    ECOULEMENT='MONO_VISC1',
                                    ECRO_ISOT='MONO_ISOT1',
                                    ECRO_CINE='MONO_CINE1',
                                    ELAS='ELAS',
                                    FAMI_SYST_GLIS='BCC24',),),
                      MATR_INTER= table,
                                    );



COMPORP=DEFI_COMPOR(POLYCRISTAL=(
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-150.646,33.864,55.646,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-137.138,41.5917,142.138,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-166.271,35.46958,171.271,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-77.676,15.61819,154.676,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-78.6463,33.864,155.646,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-65.1378,41.5917,142.138,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-94.2711,35.46958,71.271,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-5.67599,15.61819,154.676,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(-6.64634,33.864,155.646,),),
                        _F(MONOCRISTAL=COMPORT,FRAC_VOL=0.1,ANGL_EULER=(6.86224,41.5917,142.138,),),
                        ),
                    LOCALISATION='BETA',DL=0.,DA=0.,
                    );


SOLNL=STAT_NON_LINE(MODELE=TROISD,
                     CHAM_MATER=MAT,
                     EXCIT=(_F(CHARGE=TRAC,
                               FONC_MULT=COEF,
                               TYPE_CHARGE='FIXE_CSTE',),),
                       INCREMENT=(_F(LIST_INST=LINST,),),
                    COMP_INCR=(_F(RELATION='POLYCRISTAL',
                                  COMPOR=COMPORP,
                                  DEFORMATION='PETIT',
                                  ALGO_INTE='RUNGE_KUTTA',
                                  TOUT='OUI',
                                  RESI_INTE_RELA=1.E-6
                                  ),),
                     NEWTON=(_F(PREDICTION='EXTRAPOLE',
                                MATRICE='ELASTIQUE',
                                REAC_ITER=0,),),
                     CONVERGENCE=(_F(ITER_GLOB_MAXI=50,
                                     RESI_GLOB_RELA=1.E-4
                                     ),),);

SOLNL=CALC_CHAMP(reuse=SOLNL,RESULTAT=SOLNL,DEFORMATION=('EPSI_ELGA','EPSP_ELGA'))



# Calcul des moyennes

INT_SI=POST_ELEM(MODELE=TROISD,
                   RESULTAT=SOLNL,
                   INTEGRALE=_F(NOM_CHAM='SIEF_ELGA',
                                NOM_CMP=('SIXX','SIYY','SIZZ'),
                                TOUT='OUI'),)

IMPR_TABLE(TABLE=INT_SI)

INT_EP=POST_ELEM(MODELE=TROISD,
                   RESULTAT=SOLNL,
                   INTEGRALE=_F(NOM_CHAM='EPSP_ELGA',
                                NOM_CMP=('EPXX','EPYY','EPZZ'),
                                TOUT='OUI'),)

IMPR_TABLE(TABLE=INT_EP)

# comparaison a la modelisation A - agregat 10 grains
TEST_TABLE(REFERENCE='AUTRE_ASTER',
           PRECISION=0.089999999999999997,
           VALE_CALC=302.718799025,
           VALE_REFE=279.77699999999999,
           NOM_PARA='INTE_SIZZ',
           TABLE=INT_SI,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=5.0000000000000001E-3,),
           )

TEST_TABLE(REFERENCE='AUTRE_ASTER',
           PRECISION=0.059999999999999998,
           VALE_CALC= 2.91516004E-03,
           VALE_REFE=3.0731600000000001E-3,
           NOM_PARA='INTE_EPZZ',
           TABLE=INT_EP,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=5.0000000000000001E-3,),
           )

F_SI=RECU_FONCTION(TABLE=INT_SI,
                           PARA_X='INST',
                           PARA_Y='INTE_SIZZ',);

F_EP=RECU_FONCTION(TABLE=INT_EP,
                           PARA_X='INST',
                           PARA_Y='INTE_EPZZ',);


IMPR_FONCTION(
FORMAT ='XMGRACE',
UNITE=18,
#PILOTE='INTERACTIF',
COURBE=_F(FONC_X=F_EP, FONC_Y=F_SI))

FIN()
