DEBUT(CODE=_F(NOM='SDNL132A',NIV_PUB_WEB='INTERNET'),
      DEBUG=_F(SDVERI='OUI'),IGNORE_ALARM='ALGELINE3_66')
# TITRE PENDULE RIGIDE AVEC CONTACT ET FROTTEMENT
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================

tfin = .1;

pas = 0.0001;


mail=LIRE_MAILLAGE();

poutre=AFFE_MODELE(MAILLAGE=mail,
                   AFFE=(_F(GROUP_MA='POUT',
                            PHENOMENE='MECANIQUE',
                            MODELISATION='POU_D_T',),
                         _F(MAILLE='M4',
                            PHENOMENE='MECANIQUE',
                            MODELISATION='DIS_TR',),),);

acier=DEFI_MATERIAU(ELAS=_F(E=2.e11,
                            NU=0.3,
                            RHO=9167.0,),);

aff_mail=AFFE_MATERIAU(MAILLAGE=mail,
                       AFFE=_F(GROUP_MA='POUT',
                               MATER=acier,),);

elem=AFFE_CARA_ELEM(MODELE=poutre,
                    POUTRE=_F(GROUP_MA='POUT',
                              SECTION='CERCLE',
                              CARA='R',
                              VALE=0.1291,),
                    DISCRET=(_F(CARA='K_TR_D_N',
                               MAILLE='M4',
                               VALE=(0.0,0.0,0.0,0.0,588.6,0.0,),),
                               _F(CARA='M_TR_D_N',
                               MAILLE='M4',
                               VALE=(0.0,0.0,0.0,0.0,0.0,0.0,0.,0.,0.,0.,),),),);

f=AFFE_CHAR_MECA(MODELE=poutre,
                 FORCE_NODALE=_F(GROUP_NO='GRA',
                                 FZ=-100.0,),);

cl=AFFE_CHAR_MECA(MODELE=poutre,
                  DDL_IMPO=_F(GROUP_NO='C_HAU',
                              DY=0.0,
                              DRX=0.0,
                              DRZ=0.0,),
                  FORCE_NODALE=_F(NOEUD='N4',
                                  FX=-2354.4,),);

vec_f=CALC_VECT_ELEM(OPTION='CHAR_MECA',
                     CHARGE=f,);

vec_cl=CALC_VECT_ELEM(OPTION='CHAR_MECA',
                      CHARGE=cl,);

obst=DEFI_OBSTACLE(TYPE='PLAN_Z',);

obst_hau=DEFI_OBSTACLE(TYPE='PLAN_Z',);

obs_frot=DEFI_OBSTACLE(TYPE='PLAN_Z',);

ASSEMBLAGE(MODELE=poutre,
           CHAM_MATER=aff_mail,
           CARA_ELEM=elem,
           CHARGE=cl,
           NUME_DDL=CO('NU'),
           MATR_ASSE=(_F(MATRICE=CO('K'),
                         OPTION='RIGI_MECA',),
                      _F(MATRICE=CO('M'),
                         OPTION='MASS_MECA',),),);

ass_f=ASSE_VECTEUR(VECT_ELEM=vec_f,
                   NUME_DDL=NU,);

ass_cl=ASSE_VECTEUR(VECT_ELEM=vec_cl,
                    NUME_DDL=NU,);

mode=MODE_ITER_SIMULT(MATR_RIGI=K,
                      MATR_MASS=M,
                      METHODE='TRI_DIAG',
                      OPTION='MODE_RIGIDE',
                      CALC_FREQ=_F(OPTION='BANDE',
                                   FREQ=(1.e-3,30.0,),),
                      VERI_MODE=_F(STOP_ERREUR='NON',
                                   PREC_SHIFT=0.005,
                                   SEUIL=1e-02,
                                   STURM='NON',),
                      STOP_BANDE_VIDE='NON',);


freq=DEFI_LIST_REEL(DEBUT=0.0,
                    INTERVALLE=_F(JUSQU_A=50.0,
                                  NOMBRE=100,),);

PROJ_BASE(BASE=mode,
          STOCKAGE='DIAG',
          MATR_ASSE_GENE=(_F(MATRICE=CO('K_ASS'),
                             MATR_ASSE=K,),
                          _F(MATRICE=CO('M_ASS'),
                             MATR_ASSE=M,),),
          VECT_ASSE_GENE=(_F(VECTEUR=CO('F_ASS'),
                             VECT_ASSE=ass_f,),
                          _F(VECTEUR=CO('CL_ASS'),
                             VECT_ASSE=ass_cl,),),);

f_sin = FORMULE(VALE='sin((INST-0.5)*2*pi)',
                NOM_PARA='INST',);

rep_gene=DYNA_VIBRA(TYPE_CALCUL='TRAN',BASE_CALCUL='GENE',
                         MATR_MASS=M_ASS,
                         MATR_RIGI=K_ASS,
                         SCHEMA_TEMPS=_F(SCHEMA='EULER',),
                         AMOR_MODAL=_F(AMOR_REDUIT=0.0,),
                         INCREMENT=_F(INST_INIT=0.0,
                                      INST_FIN=tfin,
                                      PAS=pas,),
                         EXCIT=(_F(VECT_ASSE_GENE=CL_ASS,
                                   COEF_MULT=1.0,),
                                _F(VECT_ASSE_GENE=F_ASS,
                                   FONC_MULT=f_sin,),),
                         CHOC=(_F(NOEUD_1='N2',
                                  OBSTACLE=obst,
                                  ORIG_OBST=(0.2,0.0,0.5,),
                                  NORM_OBST=(0.0,1.0,0.0,),
                                  JEU=0.502,
                                  RIGI_NOR=1.e8,
                                  AMOR_NOR=1000.0,),
                               _F(NOEUD_1='N5',
                                  OBSTACLE=obst_hau,
                                  ORIG_OBST=(0.501,0.0,-0.5,),
                                  NORM_OBST=(0.0,1.0,0.0,),
                                  JEU=0.502,
                                  RIGI_NOR=1.e8,
                                  AMOR_NOR=1000.0,),
                               _F(NOEUD_1='N4',
                                  OBSTACLE=obs_frot,
                                  ORIG_OBST=(0.501,0.0,0.0,),
                                  NORM_OBST=(0.0,0.0,1.0,),
                                  JEU=1.e-3,
                                  RIGI_NOR=1.e8,
                                  AMOR_NOR=100000.0,
                                  RIGI_TAN=1.e8,
                                  AMOR_TAN=100000.0,
                                  FROTTEMENT='COULOMB',
                                  COULOMB=0.3,),),);

TRAN=REST_GENE_PHYS(RESU_GENE=rep_gene, NOM_CHAM=('ACCE','DEPL','VITE',),);

angg=RECU_FONCTION(RESU_GENE=rep_gene,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DRY',
                   GROUP_NO='GRA',);

V_N4_DZ=RECU_FONCTION(RESU_GENE=rep_gene,
                      NOM_CHAM='VITE',
                      NOM_CMP='DZ',
                      NOEUD='N4',);

V_GRA_DZ=RECU_FONCTION(RESU_GENE=rep_gene,
                       NOM_CHAM='VITE',
                       NOM_CMP='DZ',
                       GROUP_NO='GRA',);

VGRADZ=CREA_TABLE(FONCTION=_F(FONCTION=V_GRA_DZ,
                              PARA=('INST','VITGRA',),),
                  TYPE_TABLE='TABLE',);

VN4DZ=CREA_TABLE(FONCTION=_F(FONCTION=V_N4_DZ,
                             PARA=('INST','VITN4',),),
                 TYPE_TABLE='TABLE',);

LINST=DEFI_LIST_REEL(DEBUT=0.0,
                     INTERVALLE=_F(JUSQU_A=tfin,
                                   PAS=pas,),);

F_SIN=CALC_FONC_INTERP(FONCTION=f_sin,
                       LIST_PARA=LINST,);

SSIN=CREA_TABLE(FONCTION=_F(FONCTION=F_SIN,
                            PARA=('INST','FSIN',),),);

mulsidep = FORMULE(VALE='-100*FSIN*VITGRA',
                   NOM_PARA=('FSIN','VITGRA',),);

PUI=CALC_TABLE(TABLE=SSIN,
               ACTION=(_F(OPERATION='COMB',
                          TABLE=VGRADZ,
                          NOM_PARA='INST',),
                       _F(OPERATION='OPER',
                          FORMULE=mulsidep,
                          NOM_PARA='TRA',),),);

PUIS=RECU_FONCTION(TABLE=PUI,
                   PARA_X='INST',
                   PARA_Y='TRA',);

INJECT=CALC_FONCTION(INTEGRE=_F(METHODE='SIMPSON',
                                FONCTION=PUIS,),);

FNN2=RECU_FONCTION(RESU_GENE=rep_gene,
                   NOEUD_CHOC='N2',
                   PARA_X='INST',
                   PARA_Y='FN',);

FNN5=RECU_FONCTION(RESU_GENE=rep_gene,
                   NOEUD_CHOC='N5',
                   PARA_X='INST',
                   PARA_Y='FN',);

FNN4=RECU_FONCTION(RESU_GENE=rep_gene,
                   NOEUD_CHOC='N4',
                   PARA_X='INST',
                   PARA_Y='FN',);

FT1N4=RECU_FONCTION(RESU_GENE=rep_gene,
                    NOEUD_CHOC='N4',
                    PARA_X='INST',
                    PARA_Y='FT1',);

FT2N4=RECU_FONCTION(RESU_GENE=rep_gene,
                    NOEUD_CHOC='N4',
                    PARA_X='INST',
                    PARA_Y='FT2',);


FN_N2=CREA_TABLE(FONCTION=_F(FONCTION=FNN2,
                             PARA=('INST','FN_N2',),),);

FN_N5=CREA_TABLE(FONCTION=_F(FONCTION=FNN5,
                             PARA=('INST','FN_N5',),),);

FN_N4=CREA_TABLE(FONCTION=_F(FONCTION=FNN4,
                             PARA=('INST','FN_N4',),),);

V_N2_DZ=RECU_FONCTION(RESU_GENE=rep_gene,
                      NOM_CHAM='VITE',
                      NOM_CMP='DZ',
                      NOEUD='N2',);

V_N5_DZ=RECU_FONCTION(RESU_GENE=rep_gene,
                      NOM_CHAM='VITE',
                      NOM_CMP='DZ',
                      NOEUD='N5',);

V_N4_DX=RECU_FONCTION(RESU_GENE=rep_gene,
                      NOM_CHAM='VITE',
                      NOM_CMP='DX',
                      NOEUD='N4',);

VN4DX=CREA_TABLE(FONCTION=_F(FONCTION=V_N4_DX,
                             PARA=('INST','VIXN4',),),
                 TYPE_TABLE='TABLE',);

VN2DZ=CREA_TABLE(FONCTION=_F(FONCTION=V_N2_DZ,
                             PARA=('INST','VITN2',),),);

VN5DZ=CREA_TABLE(FONCTION=_F(FONCTION=V_N5_DZ,
                             PARA=('INST','VITN5',),),);

pdis2 = FORMULE(VALE='-VITN2*FN_N2',
                NOM_PARA=('VITN2','FN_N2',),);

pdis5 = FORMULE(VALE='VITN5*FN_N5',
                NOM_PARA=('VITN5','FN_N5',),);

pdis = FORMULE(VALE='PDIS2+PDIS5',
               NOM_PARA=('PDIS2','PDIS5',),);

DISAMOCH=CALC_TABLE(TABLE=FN_N2,
                    ACTION=(_F(OPERATION='COMB',
                               TABLE=VN2DZ,
                               NOM_PARA='INST',),
                            _F(OPERATION='COMB',
                               TABLE=FN_N5,
                               NOM_PARA='INST',),
                            _F(OPERATION='COMB',
                               TABLE=VN5DZ,
                               NOM_PARA='INST',),
                            _F(OPERATION='COMB',
                               TABLE=VN4DX,
                               NOM_PARA='INST',),
                            _F(OPERATION='COMB',
                               TABLE=FN_N4,
                               NOM_PARA='INST',),
                            _F(OPERATION='OPER',
                               FORMULE=pdis2,
                               NOM_PARA='PDIS2',),
                            _F(OPERATION='OPER',
                               FORMULE=pdis5,
                               NOM_PARA='PDIS5',),
                            _F(OPERATION='OPER',
                               FORMULE=pdis,
                               NOM_PARA='PDIS',),),);

FISAMOCH=RECU_FONCTION(TABLE=DISAMOCH,
                       PARA_X='INST',
                       PARA_Y='PDIS',);

FISAMOC2=RECU_FONCTION(TABLE=DISAMOCH,
                       PARA_X='INST',
                       PARA_Y='PDIS2',);

FISAMOC5=RECU_FONCTION(TABLE=DISAMOCH,
                       PARA_X='INST',
                       PARA_Y='PDIS5',);

TISAMOCH=CALC_FONCTION(INTEGRE=_F(METHODE='SIMPSON',
                                  FONCTION=FISAMOCH,),);

TISAMOC2=CALC_FONCTION(INTEGRE=_F(METHODE='SIMPSON',
                                  FONCTION=FISAMOC2,),);

TISAMOC5=CALC_FONCTION(INTEGRE=_F(METHODE='SIMPSON',
                                  FONCTION=FISAMOC5,),);

FTN4=CREA_TABLE(FONCTION=_F(FONCTION=FT2N4,
                            PARA=('INST','FTN4',),),);

pfrot = FORMULE(VALE='-VITN4*FTN4',
                NOM_PARA=('VITN4','FTN4',),);

PFROTN4=CALC_TABLE(TABLE=VN4DZ,
                   ACTION=(_F(OPERATION='COMB',
                              TABLE=FTN4,
                              NOM_PARA='INST',),
                           _F(OPERATION='OPER',
                              FORMULE=pfrot,
                              NOM_PARA='PFROT',),),);

FFROTN4=RECU_FONCTION(TABLE=PFROTN4,
                      PARA_X='INST',
                      PARA_Y='PFROT',);

TFROTN4=CALC_FONCTION(INTEGRE=_F(METHODE='SIMPSON',
                                 FONCTION=FFROTN4,),);



EPOT=POST_ELEM(ENER_POT=_F(TOUT='OUI',),
               MODELE=poutre,
               CHAM_MATER=aff_mail,
               CARA_ELEM=elem,
               RESULTAT=TRAN,);

ECIN=POST_ELEM(ENER_CIN=_F(TOUT='OUI',),
               MODELE=poutre,
               CHAM_MATER=aff_mail,
               CARA_ELEM=elem,
               RESULTAT=TRAN,);

ENERCIN=RECU_FONCTION(TABLE=ECIN,
                      PARA_X='INST',
                      PARA_Y='TOTALE',);

ENERPOT=RECU_FONCTION(TABLE=EPOT,
                      PARA_X='INST',
                      PARA_Y='TOTALE',);

ENERTOT=CALC_FONCTION(COMB=(_F(FONCTION=ENERCIN,
                               COEF=1.0,),
                            _F(FONCTION=ENERPOT,
                               COEF=1.0,),),);

TRAVAIL=CALC_FONCTION(COMB=(_F(FONCTION=INJECT,
                               COEF=1,),
                            _F(FONCTION=TISAMOCH,
                               COEF=-1,),
                            _F(FONCTION=TFROTN4,
                               COEF=-1,),),);

BILAN=CALC_FONCTION(COMB=(_F(FONCTION=TRAVAIL,
                             COEF=-1.0,),
                          _F(FONCTION=ENERTOT,
                             COEF=1,),),);

IMPR_FONCTION(FORMAT='XMGRACE',
              UNITE=29,
              COURBE=(_F(FONCTION=INJECT,
                         MARQUEUR=0,),
                      _F(FONCTION=TISAMOCH,
                         MARQUEUR=0,),
                      _F(FONCTION=ENERCIN,
                         MARQUEUR=0,),
                      _F(FONCTION=ENERPOT,
                         MARQUEUR=0,),
                      _F(FONCTION=ENERTOT,
                         MARQUEUR=0,),
                      _F(FONCTION=BILAN,
                         MARQUEUR=0,),
                      _F(FONCTION=TFROTN4,
                         MARQUEUR=0,),
                      _F(FONCTION=TISAMOC2,
                         MARQUEUR=0,),
                      _F(FONCTION=TISAMOC5,
                         MARQUEUR=0,),
                      _F(FONCTION=TRAVAIL,
                         MARQUEUR=0,),),);

TEST_FONCTION(VALEUR=(_F(VALE_CALC=0.01308227,
                         TOLE_MACHINE=1.E-3,
                         VALE_PARA=0.10000000000000001,
                         FONCTION=ENERCIN,
                         ),
                      _F(VALE_CALC=4.2696999999999999E-4,
                         TOLE_MACHINE=1.E-3,
                         VALE_PARA=0.10000000000000001,
                         FONCTION=ENERPOT,
                         ),
                      ),
              )

IMPR_RESU(FORMAT='GMSH',
          UNITE=37,
          RESU=_F(RESULTAT=TRAN,
                  TYPE_CHAM='VECT_3D',
                  NOM_CHAM='DEPL',
                  NOM_CMP=('DX','DY','DZ',),),);



FIN();
