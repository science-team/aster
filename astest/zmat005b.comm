# TITRE TRACTION THERMIQUE PLASTIQUE GRANDES DEFORMATIONS AXI ZMAT
# person_in_charge: jean-michel.proix at edf.fr
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# REFERENCE HSVN121B

DEBUT(CODE=_F(NOM='ZMAT005B',
              NIV_PUB_WEB='INTERNET',),
      DEBUG=_F(SDVERI='OUI',),);

MA=LIRE_MAILLAGE();

MO=AFFE_MODELE(MAILLAGE=MA,
               VERIF='MAILLE',
               AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='AXIS',),);

YOUNG=DEFI_FONCTION(NOM_PARA='TEMP',VALE=(20.,250000.,
                          120.,200000.,
                          ),PROL_DROITE='CONSTANT',);

POISSON=DEFI_CONSTANTE(VALE=0.3,);

DILAT=DEFI_CONSTANTE(VALE=0.0001,);

SIGY=DEFI_CONSTANTE(VALE=1000.,);

PENTE=DEFI_FONCTION(NOM_PARA='TEMP',VALE=(20.,2500.,
                          120.,2000.,
                          ),PROL_DROITE='CONSTANT',);

ACIER_L=DEFI_MATERIAU(ELAS_FO=_F(E=YOUNG,
                                 NU=POISSON,
                                 TEMP_DEF_ALPHA=20.0,
                                 ALPHA=DILAT,),
                      ECRO_LINE_FO=_F(D_SIGM_EPSI=PENTE,
                                      SY=SIGY,),);

L_INST=DEFI_LIST_REEL(DEBUT=0.0,
                      INTERVALLE=(_F(JUSQU_A=1.00,
                                     NOMBRE=1,),
                                  _F(JUSQU_A=2.00,
                                     NOMBRE=20,),),);

F_CHAR=DEFI_FONCTION(NOM_PARA='INST',VALE=(0.,0.,
                           1.,0.,
                           2.,293.3,
                           ),PROL_DROITE='CONSTANT',);

F_TEMP=DEFI_FONCTION(NOM_PARA='INST',NOM_RESU='TEMP',VALE=(0.,20.,
                           1.,120.,
                           2.,120.,
                           ),PROL_DROITE='CONSTANT',);

CHP_TEMP=CREA_CHAMP(TYPE_CHAM='NOEU_TEMP_F',
                    OPERATION='AFFE',
                    MAILLAGE=MA,
                    AFFE=_F(TOUT='OUI',
                            NOM_CMP='TEMP',
                            VALE_F=F_TEMP,),);

TEMP=CREA_RESU(OPERATION='AFFE',
               TYPE_RESU='EVOL_THER',
               NOM_CHAM='TEMP',
               AFFE=_F(CHAM_GD=CHP_TEMP,
                       LIST_INST=L_INST,),);

CHP_MATL=AFFE_MATERIAU(MAILLAGE=MA,
                       AFFE=_F(TOUT='OUI',
                               MATER=ACIER_L,),
                       AFFE_VARC=_F(NOM_VARC='TEMP',
                                    EVOL=TEMP,
                                    VALE_REF=20.,),);

CHR_LIAI=AFFE_CHAR_MECA(MODELE=MO,
                        DDL_IMPO=(_F(NOEUD='NO1',
                                     DY=0.,),
                                  _F(NOEUD='NO2',
                                     DY=0.,),),);

CHR_TRAC=AFFE_CHAR_MECA(MODELE=MO,
                        FACE_IMPO=_F(MAILLE='MA2',
                                     DY=1.,),);

VL=STAT_NON_LINE(MODELE=MO,
                 CHAM_MATER=CHP_MATL,
                 EXCIT=_F(CHARGE=CHR_LIAI,),
                 COMP_INCR=_F(RELATION='ZMAT',
                              NB_VARI=22,
                              UNITE=33,
                              DEFORMATION='GDEF_HYPO_ELAS',),
                 INCREMENT=_F(LIST_INST=L_INST,
                              NUME_INST_FIN=1,),
                 NEWTON=_F(MATRICE='TANGENTE',
                           REAC_ITER=1,),
                 RECH_LINEAIRE=_F(ITER_LINE_MAXI=3,),
                 CONVERGENCE=_F(RESI_GLOB_MAXI=0.1,
                                ITER_GLOB_MAXI=50,),);

VL=STAT_NON_LINE(reuse =VL,
                 MODELE=MO,
                 CHAM_MATER=CHP_MATL,
                 EXCIT=(_F(CHARGE=CHR_LIAI,),
                        _F(CHARGE=CHR_TRAC,
                           FONC_MULT=F_CHAR,
                           TYPE_CHARGE='DIDI',),),
                 COMP_INCR=_F(RELATION='ZMAT',
                              NB_VARI=22,
                              UNITE=33,
                              DEFORMATION='GDEF_HYPO_ELAS',),
                 ETAT_INIT=_F(EVOL_NOLI=VL,
                              NUME_ORDRE=1,),
                 INCREMENT=_F(LIST_INST=L_INST,),
                 NEWTON=_F(MATRICE='TANGENTE',
                           REAC_ITER=1,),
                 RECH_LINEAIRE=_F(ITER_LINE_MAXI=0,),
                 CONVERGENCE=_F(RESI_GLOB_RELA=1.E-6,
                                ITER_GLOB_MAXI=50,),);

TEST_RESU(RESU=(_F(INST=2.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=VL,
                   NOM_CHAM='DEPL',
                   NOEUD='NO3',
                   NOM_CMP='DY',
                   VALE_CALC=303.350251256,
                   VALE_REFE=303.0,
                   PRECISION=1.E-2,),
                _F(INST=2.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=VL,
                   NOM_CHAM='DEPL',
                   NOEUD='NO3',
                   NOM_CMP='DX',
                   VALE_CALC=-109.496030006,
                   VALE_REFE=-110.0,
                   PRECISION=0.02,),
                _F(INST=2.0,
                   REFERENCE='AUTRE_ASTER',
                   POINT=1,
                   RESULTAT=VL,
                   NOM_CHAM='SIEF_ELGA',
                   NOM_CMP='SIYY',
                   VALE_CALC=1451.166503061,
                   TOLE_MACHINE=1.e-3,
                   VALE_REFE=1453.0,
                   PRECISION=0.040000000000000001,
                   MAILLE='MA1',),
                _F(INST=2.0,
                   REFERENCE='AUTRE_ASTER',
                   POINT=1,
                   RESULTAT=VL,
                   NOM_CHAM='VARI_ELGA',
                   NOM_CMP='V7',
                   VALE_CALC=0.247435185,
                   VALE_REFE=0.2475,
                   PRECISION=0.014999999999999999,
                   MAILLE='MA1',),
                ),
          )

FIN();
