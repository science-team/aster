# TITRE RACCORD POUTRE-3D : PROBLEME DE SAINT-VENANT -
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#                           POUTRE EN TRACTION-FLEXION
#   SUR LA MOITIE DE SA LONGUEUR LA POUTRE EST MODELISEE AVEC
#   DES CUBES A 20 NOEUDS
#   SUR L'AUTRE MOITIE ELLE EST MODELISEE AVEC UNE POUTRE EULER
#   L'ENCASTREMENT EST LUI-MEME MODELISE AVEC UN RACCORD POUTRE-3D
#   ENTRE LA PARTIE MASSIVE ET LE NOEUD D'UNE POUTRE FICTIVE QUE
#   L'ON A ENCASTRE
###################################################################

DEBUT(CODE=_F(NOM='SSLX200A', NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

MATER1=DEFI_MATERIAU(  ELAS=_F(  E = 200000.,   NU = 0.3,
                                  RHO = 10000.) )

MA=LIRE_MAILLAGE(FORMAT='MED', )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MA,
             AFFE=_F(  GROUP_MA = ('POU3D','AB',),    MATER = MATER1)  )

MOD=AFFE_MODELE(  MAILLAGE=MA,AFFE=(
             _F(  GROUP_MA = 'POU3D',
                   MODELISATION = '3D',
                   PHENOMENE = 'MECANIQUE'),
             _F(  GROUP_MA = ('AB',),
                   MODELISATION = 'POU_D_E',
                   PHENOMENE = 'MECANIQUE'),
             _F(  GROUP_NO = 'C',
                   MODELISATION = 'DIS_TR',
                   PHENOMENE = 'MECANIQUE'))
                   )

CELEM=AFFE_CARA_ELEM(   MODELE=MOD,
               VERIF=( 'MAILLE',  'NOEUD',  ),
               POUTRE=_F(  GROUP_MA = ('AB',), SECTION = 'RECTANGLE',
                        CARA = ( 'HY',  'HZ', ),
                        VALE = (  3.,    2.,   )),
                DISCRET=(_F(  GROUP_NO = 'C',
                          CARA = 'K_TR_D_N',
                          VALE = (0., 0., 0.,
                                  0., 0., 0., )),
                         _F(  GROUP_NO = 'C',
                          CARA = 'M_TR_D_N',
                          VALE = (0., 0., 0.,
                                  0., 0., 0.,
                                  0., 0., 0., 0., )),)
                                                        )

CH=AFFE_CHAR_MECA(
            MODELE=MOD,LIAISON_ELEM=(
                 _F( OPTION = '3D_POU',
                               GROUP_MA_1 = 'SU',
                               GROUP_NO_2 = 'C',
                               NUME_LAGR = 'APRES'),
#
                 _F( OPTION = '3D_POU',
                               GROUP_MA_1 = 'SF',
                               GROUP_NO_2 = 'A')),
#
     DDL_IMPO=_F( GROUP_NO = 'C',  DX = 0., DY = 0., DZ = 0., DRX = 0.,
                             DRY = 0., DRZ = 0.),
#
           FORCE_NODALE=_F( GROUP_NO = 'B',  FX = 10., MY = 2., MZ = 3.)
                                       )

#
# CALCUL ELEMENTS FINIS RESOLUTION
#

MATRR=CALC_MATR_ELEM(    MODELE=MOD,
                              CHARGE=CH,
                              CHAM_MATER=CHMAT,
                              CARA_ELEM=CELEM,
                                  OPTION='RIGI_MECA' )



MATRM=CALC_MATR_ELEM(    MODELE=MOD,
                              CHARGE=CH,
                              CHAM_MATER=CHMAT,
                              CARA_ELEM=CELEM,
                                  OPTION='MASS_MECA' )

VEL=CALC_VECT_ELEM(    CHARGE=CH,
                              OPTION='CHAR_MECA' )

#
# IMPRESSION DES VECTEURS ELEMENTAIRES DU VECT_ELEM VEL
#


NU=NUME_DDL(  MATR_RIGI=MATRR,  METHODE='LDLT',  RENUM='RCMK')

MATASS=ASSE_MATRICE(    MATR_ELEM=MATRR,
                              NUME_DDL=NU   )


MATASM=ASSE_MATRICE(    MATR_ELEM=MATRM,
                              NUME_DDL=NU   )

MODES=MODE_ITER_SIMULT(   MATR_RIGI=MATASS,
                          MATR_MASS=MATASM,
                          SOLVEUR=_F(METHODE='MUMPS'),
###CDURAND###     CALC_FREQ=_F( FREQ = (0., 2.,))
                       )


RESU=MECA_STATIQUE(MODELE=MOD,  SOLVEUR=_F(METHODE='MUMPS'),
                   CHAM_MATER=CHMAT,
                   CARA_ELEM=CELEM,
                   EXCIT=_F(CHARGE=CH,),
                   );

RESU=CALC_CHAMP(reuse=RESU,RESULTAT=RESU,GROUP_MA='POU3D',CONTRAINTE=('SIGM_ELNO'))


TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N1',
                   NOM_CMP='DX',
                   VALE_CALC=8.3333333299999996E-05,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N1',
                   NOM_CMP='DY',
                   VALE_CALC=1.666666667E-4,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N1',
                   NOM_CMP='DZ',
                   VALE_CALC=-2.5000000000000001E-4,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N6',
                   NOM_CMP='DX',
                   VALE_CALC=-8.3333333333332999E-06,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N6',
                   NOM_CMP='DY',
                   VALE_CALC=4.0583333333333003E-05,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOEUD='N6',
                   NOM_CMP='DZ',
                   VALE_CALC=-6.0875000000000002E-05,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIXX',
                   VALE_CALC=-0.33333333300000001,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIYY',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIZZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIXY',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIXZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N6',
                   NOM_CMP='SIYZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M27',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIXX',
                   VALE_CALC=1.666666666667,
                   TOLE_MACHINE=9.9999999999999995E-08,
                   CRITERE='RELATIF',
                   MAILLE='M25',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIYY',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M25',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIZZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M25',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIXY',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M25',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIXZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M25',),
                _F(NUME_ORDRE=1,
                   RESULTAT=RESU,
                   NOM_CHAM='SIGM_ELNO',
                   NOEUD='N25',
                   NOM_CMP='SIYZ',
                   VALE_CALC=0.0,
                   CRITERE='ABSOLU',
                   MAILLE='M25',),
                ),
          )


TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=MODES,
                   VALE_CALC=0.014200000000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=MODES,
                   VALE_CALC=0.1066,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                _F(NUME_ORDRE=9,
                   PARA='FREQ',
                   RESULTAT=MODES,
                   VALE_CALC=0.24840000000000001,
                   TOLE_MACHINE=1.E-3,
                   CRITERE='RELATIF',
                   ),
                ),
          )

FIN()
#
