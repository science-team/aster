# TITRE BIBLIO_131 FISSURE EN MODE 2 D'UNE EPROUVETTE ELASTOPLASTIQUE
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#

DEBUT( CODE=_F( NOM = 'SSNP311A',NIV_PUB_WEB='INTERNET'))

MA=LIRE_MAILLAGE(VERI_MAIL=_F(VERIF='OUI'),FORMAT='MED',  )

MO=AFFE_MODELE(    MAILLAGE=MA,
                                AFFE=_F(  TOUT = 'OUI',
                                       PHENOMENE = 'MECANIQUE',
                                       MODELISATION = 'D_PLAN')
                        )

#
TRAC1=DEFI_FONCTION(       NOM_PARA='EPSI',
                                     VALE=(0.0046,  334.6,
                                           0.0061,  410.7,
                                           0.0075,  431.6,
                                           0.01,    443.5,
                                           0.03,    480.0,
                                           0.0550,  500.1,
                                           3.0,     505.2,
                                          ),
                              PROL_DROITE='LINEAIRE'
                         )

#
MAT1=DEFI_MATERIAU(        ELAS=_F(  E = 74200.,
                                          NU = 0.32,
                                          ALPHA = 0.),
                               TRACTION=_F(  SIGM = TRAC1)
                           )

# EP = 6.36
MAT2=DEFI_MATERIAU(    ELAS=_F(  E = 809748.,
                                      NU = 0.3,
                                      ALPHA = 0.)
                           )

CHMAT=AFFE_MATERIAU(    MAILLAGE=MA,AFFE=(
                                   _F(  GROUP_MA = 'EPROUVET',
                                          MATER = MAT1),
                                   _F(  GROUP_MA = 'PORTEPRO',
                                          MATER = MAT2))
                           )

# CONDITIONS AUX LIMITES FIXES
CLF=AFFE_CHAR_MECA(        MODELE=MO,DDL_IMPO=(
                                  _F(  GROUP_NO = 'PC0',
                                             DX = 0.),
                                           _F(  GROUP_NO = 'PC0',
                                             DY = 0.),
                                           _F(  GROUP_NO = 'PC1',
                                             DX = 0.)),LIAISON_DDL=(
                               _F(  GROUP_NO = ( 'PPEF1', 'PEF1', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF1', 'PEF1', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF2', 'PEF2', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF2', 'PEF2', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF3', 'PEF3', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF3', 'PEF3', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF4', 'PEF4', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF4', 'PEF4', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF5', 'PEF5', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF5', 'PEF5', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF6', 'PEF6', ),
                                             DDL = ( 'DX',  'DX', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.),
                                           _F(  GROUP_NO = ( 'PPEF6', 'PEF6', ),
                                             DDL = ( 'DY',  'DY', ),
                                             COEF_MULT = ( 1.,    -1.,  ),
                                             COEF_IMPO = 0.))
                           )

# CONDITIONS AUX LIMITES VARIABLES
CLV=AFFE_CHAR_MECA(          MODELE=MO,
                                FORCE_NODALE=_F(  GROUP_NO = 'PC1',
                                               FY = 1.)
                            )

#
RAMPE=DEFI_FONCTION(       NOM_PARA='INST',
                                      VALE=( 0.,  0.,  1.,  1., ),
                               PROL_GAUCHE='LINEAIRE',
                                PROL_DROITE='LINEAIRE'
                           )

# EP =6.36
L_INST=DEFI_LIST_REEL(         DEBUT=0.,INTERVALLE=(
                            _F( JUSQU_A = 1850.95, NOMBRE = 12),
                            _F( JUSQU_A = 3084.90, NOMBRE = 16),
                            _F( JUSQU_A = 3701.90, NOMBRE = 20),
                            _F( JUSQU_A = 3948.75, NOMBRE = 16))
                        )

CHAMDEPL=STAT_NON_LINE(
                            MODELE=MO,
                        CHAM_MATER=CHMAT,EXCIT=(
                             _F(
                CHARGE = CLF), _F(
                CHARGE = CLV,
                FONC_MULT = RAMPE)),
                         COMP_INCR=_F(
                RELATION = 'VMIS_ISOT_TRAC',
                GROUP_MA = (
                  'EPROUVET',  )),
                         COMP_ELAS=_F(
                RELATION = 'ELAS',
                GROUP_MA = (
                  'PORTEPRO',  )),
                         INCREMENT=_F(
                LIST_INST = L_INST),
                            NEWTON=_F(
                MATRICE = 'TANGENTE',
                REAC_ITER = 5)
              )

CHAMDEPL=CALC_CHAMP(reuse=CHAMDEPL,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=CHAMDEPL)


#
FOND=DEFI_FOND_FISS(    MAILLAGE=MA,
                           FOND_FISS=_F( GROUP_NO = ('P0',)),
                      )

#
THETA0=CALC_THETA(       MODELE=MO,
                         THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 1.00,
                                   R_SUP = 2.00),
                        DIRECTION=(0., -1., 0.,)
                     )

#
THETA1=CALC_THETA(       MODELE=MO,
                         THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 2.00,
                                   R_SUP = 3.00),
                        DIRECTION=(0., -1., 0.,)
                     )

#
THETA2=CALC_THETA(       MODELE=MO,
                         THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 3.00,
                                   R_SUP = 4.00),
                        DIRECTION=(0., -1., 0.,)
                     )

#
THETA3=CALC_THETA(       MODELE=MO,
                         THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 4.00,
                                   R_SUP = 5.00),
                        DIRECTION=(0., -1., 0.,)
                     )

#
# CALC_G POUR LES INSTANTS 22, 36, 50, 64
#
# Ici, la loi de comportement dans CALC_G (ELAS_VMIS_TRAC) 
# differe de la loi de
# comportement de STAT_NON_LINE (VMIS_ISOT_TRAC).
# Ceci est du au fait que l'on veut calculer G en supposant 
# que le chargement est monotone
# proportionnel. L'utilisation de la loi VMIS_ISOT_TRAC dans 
# CALC_G aurait conduit a
# calculer le parametre GTP (voir doc U2.82.03).

G220=CALC_G(        RESULTAT=CHAMDEPL,
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    THETA=_F(THETA=THETA0),
                    NUME_ORDRE=22
              )

#
G360=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA0),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=36
              )

#
G500=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA0),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=50
              )

#
G640=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA0),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=64
              )

#
G221=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA1),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=22
              )

#
G361=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA1),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=36
              )

#
G501=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA1),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=50
              )

#
G641=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA1),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=64
              )

#
G222=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA2),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=22
              )

#
G362=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA2),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=36
              )

#
G502=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA2),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=50
              )

#
G642=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA2),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=64
              )

#
G223=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA3),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                  NUME_ORDRE=22
              )

#
G363=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA3),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=36
              )

#
G503=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA3),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=50
              )

#
G643=CALC_G(        RESULTAT=CHAMDEPL,
                    THETA=_F(THETA=THETA3),
                    COMP_ELAS=_F(  RELATION = 'ELAS_VMIS_TRAC'),
                    NUME_ORDRE=64
              )

#
##
## TESTS DES RESULTATS COURONNE 0
## -----------------------------------
#

TEST_TABLE(PRECISION=0.050000000000000003,
           VALE_CALC=7.005604714,
           VALE_REFE=6.7450999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G220,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=22,),
           )

TEST_TABLE(PRECISION=0.070000000000000007,
           VALE_CALC=13.069179506,
           VALE_REFE=12.473000000000001,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G360,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=36,),
           )

TEST_TABLE(PRECISION=0.089999999999999997,
           VALE_CALC=19.493643225,
           VALE_REFE=18.433,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G500,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=26.840918902,
           VALE_REFE=24.600999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G640,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=64,),
           )

#
##
## TESTS DES RESULTATS COURONNE 1
## -----------------------------------
#

TEST_TABLE(PRECISION=0.050000000000000003,
           VALE_CALC=6.997283273,
           VALE_REFE=6.7450999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G221,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=22,),
           )

TEST_TABLE(PRECISION=0.070000000000000007,
           VALE_CALC=13.093750493,
           VALE_REFE=12.473000000000001,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G361,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=36,),
           )

TEST_TABLE(PRECISION=0.089999999999999997,
           VALE_CALC=19.572903428,
           VALE_REFE=18.433,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G501,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=26.976818328,
           VALE_REFE=24.600999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G641,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=64,),
           )

#
##
## TESTS DES RESULTATS COURONNE 2
## -----------------------------------
#

TEST_TABLE(PRECISION=0.050000000000000003,
           VALE_CALC=6.996408338,
           VALE_REFE=6.7450999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G222,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=22,),
           )

TEST_TABLE(PRECISION=0.070000000000000007,
           VALE_CALC=13.082576437,
           VALE_REFE=12.473000000000001,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G362,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=36,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=19.576516845,
           VALE_REFE=18.433,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G502,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=26.980501128,
           VALE_REFE=24.600999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G642,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=64,),
           )

#
##
## TESTS DES RESULTATS COURONNE 3
## -----------------------------------
#

TEST_TABLE(PRECISION=0.050000000000000003,
           VALE_CALC=6.998011559,
           VALE_REFE=6.7450999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G223,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=22,),
           )

TEST_TABLE(PRECISION=0.070000000000000007,
           VALE_CALC=13.071080817,
           VALE_REFE=12.473000000000001,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G363,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=36,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=19.574655465,
           VALE_REFE=18.433,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G503,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(PRECISION=0.10000000000000001,
           VALE_CALC=26.983385912,
           VALE_REFE=24.600999999999999,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G643,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=64,),
           )

FIN()
