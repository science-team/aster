# TITRE POST-FLAMBEMENT ELASTIQUE D'UNE STRUCTURE EN L
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================

DEBUT(CODE=_F( NOM = 'SSNL133A',NIV_PUB_WEB='INTERNET',VISU_EFICAS='OUI'),)

MA=LIRE_MAILLAGE(UNITE=17,FORMAT='MED',)

MA=DEFI_GROUP(reuse =MA,
   MAILLAGE=MA,
   CREA_GROUP_NO=_F(TOUT_GROUP_MA='OUI',),)

MO=AFFE_MODELE(
   MAILLAGE=MA,
   AFFE=_F(TOUT='OUI', PHENOMENE='MECANIQUE', MODELISATION='POU_D_T_GD',),)

ACIER=DEFI_MATERIAU(ELAS=_F(E=71240.0, NU=0.3,),)

MAT=AFFE_MATERIAU(
   MAILLAGE=MA,
   AFFE=_F(TOUT='OUI', MATER=ACIER,),)

CARA=AFFE_CARA_ELEM(
   MODELE=MO,
   POUTRE=_F(GROUP_MA='LAME', SECTION='GENERALE',
             CARA=('A','IY','IZ','AY','AZ','JX',),
             VALE=(18.0,0.54,1350.0,1.2,1.2,2.16,),),
   ORIENTATION=_F(GROUP_MA='LAME', CARA='ANGL_VRIL', VALE=0.,),)

CL=AFFE_CHAR_MECA(
   MODELE=MO,
   DDL_IMPO=_F(GROUP_NO='PA',DX=0., DY=0., DZ=0., DRX=0., DRY=0., DRZ=0.,),
   #FORCE_NODALE=_F(GROUP_NO='PC', FZ=0.001,),
)

FORCE=AFFE_CHAR_MECA(
   MODELE=MO,
   FORCE_NODALE=_F(GROUP_NO='PC', FZ=0.001,  FX=1.0,),)


FORCE_F=DEFI_FONCTION(
   NOM_PARA='INST',
   VALE=(0.,0.,10.0,10.0,),)

tfin = 5.0

TEMPS=DEFI_LIST_REEL(
   DEBUT=0.,
   INTERVALLE=(
      _F(JUSQU_A=1.0,  NOMBRE=1,),
      _F(JUSQU_A=tfin, NOMBRE=20,),),)

DEFLIST=DEFI_LIST_INST(
   DEFI_LIST=_F(LIST_INST = TEMPS,METHODE='AUTO',PAS_MINI=1.0E-12,),
   ECHEC=_F(ACTION='DECOUPE', SUBD_METHODE='MANUEL',
            SUBD_PAS=4, SUBD_NIVEAU = 10, SUBD_PAS_MINI = 1.0E-12,),
   ADAPTATION=_F(EVENEMENT='SEUIL'),
)

RESO=STAT_NON_LINE(
   INFO=1,
   MODELE=MO,
   CHAM_MATER=MAT,
   CARA_ELEM=CARA,
   EXCIT=(
      _F(CHARGE=CL,),
      _F(CHARGE=FORCE, FONC_MULT=FORCE_F,),),
   COMP_ELAS=_F(RELATION='ELAS_POUTRE_GR', DEFORMATION='GROT_GDEP',),
   INCREMENT=_F(LIST_INST=DEFLIST, INST_FIN=tfin, ),
   NEWTON=_F(MATRICE='TANGENTE', REAC_ITER=1,),
   CONVERGENCE=_F(RESI_GLOB_RELA=1.0E-6, ITER_GLOB_MAXI=21,),
   SOLVEUR=_F(METHODE='LDLT', ),
)

DEPX=RECU_FONCTION(
   RESULTAT=RESO,
   TOUT_ORDRE='OUI',
   NOM_CHAM='DEPL',
   NOM_CMP='DX',
   GROUP_NO='PC',)

DEPZ=RECU_FONCTION(
   RESULTAT=RESO,
   TOUT_ORDRE='OUI',
   NOM_CHAM='DEPL',
   NOM_CMP='DZ',
   GROUP_NO='PC',)

T_INST=RECU_TABLE(CO=RESO,NOM_PARA='INST',)
COPIE=FORMULE(NOM_PARA=('INST',),VALE='INST',)
T_INST=CALC_TABLE(reuse=T_INST,
   TABLE=T_INST,
   ACTION=(_F(OPERATION='OPER', FORMULE=COPIE, NOM_PARA='ETA_PILOTAGE'),),)

F_INST=RECU_FONCTION(TABLE=T_INST,PARA_X='INST',PARA_Y='ETA_PILOTAGE',)

# ---------------------- IMPRESSION --------------------------------
IMPR_FONCTION(
   FORMAT='XMGRACE',
   UNITE=29,
   COURBE=(
      _F(FONC_X=DEPX,FONC_Y=F_INST,LEGENDE='DX',MARQUEUR=0,),
      _F(FONC_X=DEPZ,FONC_Y=F_INST,LEGENDE='DZ',MARQUEUR=0,), ),
   TITRE='Reponse force-deplacement',
   BORNE_X=(0.0,170.0,),
   BORNE_Y=(0.0,5.0,),
   ECHELLE_X='LIN', ECHELLE_Y='LIN',
   GRILLE_X=20, GRILLE_Y=1,
   LEGENDE_X='Deplacement (mm)',
   LEGENDE_Y='Force (N)',
)

# ------------------ TESTS DE NON REGRESSION -----------------------
IMPR_FONCTION(
   FORMAT='TABLEAU',
   COURBE=(
      _F(FONCTION=DEPX,),
      _F(FONCTION=DEPZ,),),)

# TESTS DE NON REGRESSION SUR DEPX et DEPZ
ResuRefe = [
# PARA       DX                    DZ               TOLEMAx   TOLEMAz
[ 1.0 , 1.9557493475982E-01 , 8.1849504440533E-01 , 1.0E-06 , 1.0E-06 ,],
[ 1.2 , 2.2003159601578E+01 , 4.5085834614729E+01 , 2.0E-05 , 1.5E-05 ,],
[ 1.4 , 5.1370721240215E+01 , 5.9127605459760E+01 , 4.0E-06 , 6.5E-06 ,],
[ 1.6 , 7.2307885049332E+01 , 6.1008538728158E+01 , 8.5E-06 , 1.7E-05 ,],
[ 2.0 , 9.9971229134146E+01 , 5.5985405418044E+01 , 3.5E-06 , 1.5E-05 ,],
[ 3.0 , 1.3415730437940E+02 , 3.9186532467172E+01 , 1.0E-06 , 1.3E-05 ,],
[ 4.0 , 1.5070726872507E+02 , 2.7659987903886E+01 , 4.0E-06 , 1.2E-05 ,],
[ 5.0 , 1.6092765462757E+02 , 2.0199252710901E+01 , 1.0E-06 , 1.1E-05 ,],
]
# Par defaut TOLE_MACHINE=1.0E-06, pour passer sur toutes les machines 2.0E-05
# Les TOLE_MACHINE sont ajustes pour clap0f0q (32bits) car la solution est
# numeriquement sensible (instabilite)
motclef = {}
motclef['VALEUR']= []
for vpara, vdx,vdz, tolemax, tolemaz in ResuRefe:
   motclef['VALEUR'].append( _F(VALE_PARA=vpara, FONCTION=DEPX, VALE_CALC=vdx,
                                TOLE_MACHINE=tolemax, CRITERE='RELATIF',) )
   motclef['VALEUR'].append( _F(VALE_PARA=vpara, FONCTION=DEPZ, VALE_CALC=vdz,
                                TOLE_MACHINE=tolemaz, CRITERE='RELATIF',) )

TEST_FONCTION(**motclef)

FIN()
